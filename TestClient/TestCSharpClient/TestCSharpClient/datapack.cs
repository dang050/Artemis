﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestCSharpClient
{
    class Pack
    {
    }

    class ReaderPack
    {
        public static T Read<T>(byte[] infos) where T : Pack, new()
        {
            T pack = new T();
            MemoryStream input = new MemoryStream(infos);
            BinaryReader reader = new BinaryReader(input);

            FieldInfo[] fields = pack.GetType().GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType.IsArray)
                {
                    // 数组
                    field.SetValue(pack, ReadArray(field.FieldType, ref reader));
                }
                else if (field.FieldType.IsClass)
                {
                    // Class
                    field.SetValue(pack, ReadClass(field.FieldType, ref reader));
                }
                else
                {
                    // 基础类型
                    field.SetValue(pack, ReadBaseType(field.FieldType, ref reader));
                }
            }
            reader.Close();
            input.Close();
            return pack;
        }

        private static object ReadClass(Type fieldType, ref BinaryReader reader)
        {
            object result = fieldType.InvokeMember("Set", BindingFlags.CreateInstance, null, null, new object[] { });
            FieldInfo[] fields = fieldType.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType.IsArray)
                {
                    // 数组
                    field.SetValue(result, ReadArray(field.FieldType, ref reader));
                }
                else if (field.FieldType.IsClass)
                {
                    // Class
                    field.SetValue(result, ReadClass(field.FieldType, ref reader));
                }
                else
                {
                    // 基础类型
                    field.SetValue(result, ReadBaseType(field.FieldType, ref reader));
                }
            }
            return result;
        }

        private static object ReadArray(Type fieldType, ref BinaryReader reader)
        {
            // 读入两字节长度
            UInt16 len = reader.ReadUInt16();
            if (len <= 0)
            {
                return null;
            }
       
            Array result = (Array)fieldType.InvokeMember("Set", BindingFlags.CreateInstance, null, null, new object[] { len });
            Type type = result.GetType().GetElementType();
            for (int i = 0; i < len; ++i)
            {
                if (type.IsArray)
                {
                    result.SetValue(ReadArray(type, ref reader), i);
                }
                else if (type.IsClass)
                {
                    result.SetValue(ReadClass(type, ref reader), i);
                }
                else
                {
                    result.SetValue(ReadBaseType(type, ref reader), i);
                }
            }
            return result;
        }

        private static object ReadBaseType(Type fieldType, ref BinaryReader reader)
        {
            object result = null;
            switch (fieldType.Name)
            {
                case "Boolean":
                    {
                        result = reader.ReadBoolean();
                        break;
                    }
                case "Byte":
                    {
                        result = reader.ReadByte();
                        break;
                    }
                case "Byte[]":
                    {
                        // 前面两个字节存放长度
                        UInt16 len = reader.ReadUInt16();
                        result = reader.ReadBytes(len);
                        break;
                    }
                case "Char":
                    {
                        result = reader.ReadChar();
                        break;
                    }
                case "Char[]":
                    {
                        // 前面两个字节存放长度
                        UInt16 len = reader.ReadUInt16();
                        result = reader.ReadChars(len);
                        break;
                    }
                case "Decimal":
                    {
                        result = reader.ReadDecimal();
                        break;
                    }
                case "Double":
                    {
                        result = reader.ReadDouble();
                        break;
                    }
                case "Int16":
                    {
                        result = reader.ReadInt16();
                        break;
                    }
                case "Int32":
                    {
                        result = reader.ReadInt32();
                        break;
                    }
                case "Int64":
                    {
                        result = reader.ReadInt64();
                        break;
                    }
                case "SByte":
                    {
                        result = reader.ReadSByte();
                        break;
                    }
                case "Single":  //float
                    {
                        result = reader.ReadSingle();
                        break;
                    }
                case "String":  //string
                    {
                        result = reader.ReadString();
                        break;
                    }
                case "UInt16":
                    {
                        result = reader.ReadUInt16();
                        break;
                    }
                case "UInt32":
                    {
                        result = reader.ReadUInt32();
                        break;
                    }
                case "UInt64":
                    {
                        result = reader.ReadUInt64();
                        break;
                    }
                default:
                    break;
            }
            return result;
        }
    }
    
    class WriterPack
    {
        MemoryStream output;
        BinaryWriter writer;
        int _len;
        public int Len
        {
            get
            {
                return _len;
            }
        }

        public WriterPack()
        {
            output = new MemoryStream();
            writer = new BinaryWriter(output);
            writer.Write((UInt32)0);        // 用来记录长度
        }

        ~WriterPack()
        {
            writer.Close();
            output.Close();
        }
 
        public void Write<T>(T val) where T : Pack
        {
            if (val == null)
            {
                return;
            }
            FieldInfo[] fields = val.GetType().GetFields();
            Type type = writer.GetType();
            foreach (FieldInfo field in fields)
            {
                object value = field.GetValue(val);
                if (field.FieldType.IsArray)
                {
                    // 数组
                    WriteArray((Array)value);
                }
                else if (field.FieldType.IsClass)
                {
                    // Class
                    Write((Pack)value);
                }
                else
                {
                    // 基础类型
                    WriteBaseType(field.FieldType, value);
                }
            }
        }

        private void WriteArray(Array value)
        {
            if (value == null || value.Length == 0)
            {
                writer.Write((UInt16)0);
                return;
            }
            // 写入两字节长度
            writer.Write((UInt16)value.Length);
            Type type = value.GetType().GetElementType();
            for (int i = 0; i != value.Length; ++i)
            {
                if (type.IsArray)
                {
                    WriteArray((Array)value.GetValue(i));
                }
                else if (type.IsClass)
                {
                    Write((Pack)value.GetValue(i));
                }
                else
                {
                    WriteBaseType(type, value.GetValue(i));
                }
            }

        }

        private void WriteBaseType(Type fieldType, object value)
        {
            Type type = writer.GetType();
            MethodInfo mi = type.GetMethod("Write", new Type[] { fieldType });
            if (fieldType.Name == "Byte[]")
            {
                // 写入两字节的长度
                writer.Write((UInt16)((byte[])value).Length);
            }
            else if (fieldType.Name == "Char[]")
            {
                // 写入两字节的长度
                writer.Write((UInt16)((char[])value).Length);
            }
            mi.Invoke(writer, new object[] { value });
        }

        public byte[] ToBytes()
        {
            writer.Flush();     // 确保已经写入完成
            _len = output.ToArray().Length;
            writer.Seek(0, SeekOrigin.Begin);
            writer.Write((UInt32)_len);
            writer.Flush();
            writer.Seek(0, SeekOrigin.End);
            return output.ToArray();
        }

        public void Clear()
        {
            output.SetLength(sizeof(UInt32));
            output.Position = sizeof(UInt32);
        }
    }
}
