﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using TestCSharpClient;

namespace ConsoleApplication1
{
    class C2SLogoutPack : Pack
    {
        public UInt32 systemId = 1;
        public UInt32 cmd = 5;
    }

    class C2SGetUserPack : Pack
    {
        public UInt32 systemId = 1;
        public UInt32 cmd = 4;
        public byte[] name;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            client.ConnectionServer();


            C2SLogoutPack tPack = new C2SLogoutPack();
            WriterPack pack = new WriterPack();
            pack.Write(tPack);
            client.SendMessage(pack);
            pack.Clear();

            while (true)
            {
                string str = Console.ReadLine();

                if (str == "stop")
                {
                    break;
                }
                C2SGetUserPack cPack = new C2SGetUserPack();
                cPack.name = System.Text.Encoding.Default.GetBytes(str);

                pack.Write(cPack);
                client.SendMessage(pack);
                pack.Clear();
            }
            client.Disconnect();
     

        }
    }



    class Client
    {
        class RecievePacket
        {
            public UInt32     len = 0;
            public List<byte> data = new List<byte>();

            public void Clear()
            {
                len = 0;
                data.Clear();
            }
        }
        RecievePacket recvPack = new RecievePacket();
        byte[] recvBuffer = new byte[1024];


        int serverPort = 12345;
        string serverIP = "192.168.1.103";
        Socket client;

        public int Port
        {
            get
            {
                return serverPort;
            }
            set
            {
                serverPort = value;
            }
        }

        public string IP
        {
            get
            {
                return serverIP;
            }
            set
            {
                serverIP = value;
            }
        }

        public void ConnectionServer()
        {
            // 连接服务器
            try
            {
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPAddress ip = IPAddress.Parse(serverIP);
                IPEndPoint iep = new IPEndPoint(ip, serverPort);
                client.BeginConnect(iep, new AsyncCallback(ConnectCallBack), null);
            }
            catch { }
        }

        private void ConnectCallBack(IAsyncResult asy)
        {
            try
            {
                client.EndConnect(asy);
                client.BeginReceive(recvBuffer, 0, recvBuffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);
            }
            catch { }
        }

        private void RecieveCallBack(IAsyncResult asy)
        {
            try
            {
                // 解决粘包问题
                int recvLen = client.EndReceive(asy);
                var binaryReader = new BinaryReader(new MemoryStream(recvBuffer));

                while (recvLen > 0)
                {
                    UInt32 packLen = 0;
                    if (recvPack.len == 0)
                    {
                        packLen = binaryReader.ReadUInt32();
                        recvPack.len = packLen;
                    }
                    else
                    {
                        packLen = recvPack.len;
                    }
                    UInt32 len = Math.Min((UInt32)recvLen, packLen - (UInt32)recvPack.data.Count);    // 剩余长度
                    recvPack.data.AddRange(binaryReader.ReadBytes((int)len));

                    if (recvPack.data.Count == packLen)
                    {
                        // 一个完整的包接收完毕
                        RecieveDone(recvPack.data.ToArray());
                        recvPack.Clear();
                    }

                    recvLen -= (int)len;
                }
                binaryReader.Close();

                // 继续监听下一次的数据接收
                client.BeginReceive(recvBuffer, 0, recvBuffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);
            }
            catch { }
        }

        public void SendMessage(WriterPack pack)
        {
            try
            {
                byte[] data = pack.ToBytes();
                client.BeginSend(data, 0, pack.Len, SocketFlags.None, new AsyncCallback(SendCallBack), null);
            }
            catch { }
        }

        private void SendCallBack(IAsyncResult asy)
        {
            try
            {
                int bytesSent = client.EndSend(asy);
                Console.WriteLine("Send {0} bytes", bytesSent);
            }
            catch { }
        }

        public void Disconnect()
        {
            try
            {
                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
            catch { }
        }

        // 数据接收完成
        private void RecieveDone(byte[] data)
        {
      //      TestPack pack = ReaderPack.Read<TestPack>(data);
       //     Console.WriteLine("接收到的数据包长度：" + pack.data.Length + ", msg: " + System.Text.Encoding.Default.GetString(pack.data));
        }
    }
}
