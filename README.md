# Artemis
Artemis 是一款开源的游戏，包含服务器和客户端，服务器主要由C++11/Lua编写，客户端用Unity3D编写

## Server
服务器环境配置
  - Windows操作系统
  - 编译工具VS2013、VS2015

服务器介绍
  - 网络模型采用Windows完成端口
  - 开发语言使用C++11、Lua
  - 数据库采用Mysql
  - 自己打造日志模块、通信模块、协议模块等，尽量避免使用第三方库
  
## Client
客户端介绍
  - Unity3D引擎，脚本用C#编写
  - 使用UGUI，后面也可能会用NGUI，看需求
  - 资源暂时是用的网上LOL相关的资源，功能开发完成后可能会换

### 移动
![move](https://gitee.com/1050676515/Artemis/raw/master/ScreenShots/move.gif)

