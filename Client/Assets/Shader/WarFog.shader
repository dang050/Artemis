﻿Shader "Custom/WarFog" {
	Properties {
		_MainTex ("_MainTex", 2D) = "white" {}
		_MaskTex ("_MaskTex", 2D) = "white" {}
	}
	SubShader{
		Pass{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform sampler2D _MaskTex;

			fixed4 frag(v2f_img i) :COLOR{
				fixed4 mainColor = tex2D(_MainTex, i.uv);
				fixed4 maskColor = tex2D(_MaskTex, i.uv);
				fixed4 result;
				if (maskColor.r < .3) {
					result = maskColor.rgba;
				}
				else {
					result = mainColor.rgba;
				}
				return result;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
