﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WarningManager : MonoBehaviour {
    public WarningPanel warningPanel;
    public static List<WarningModel> errors = new List<WarningModel>();
    
    void Update()
    {
        if (errors.Count == 0 || warningPanel.gameObject.activeSelf)
        {
            return;
        }
        WarningModel model = errors[0];
        errors.RemoveAt(0);
        warningPanel.error(model);
    } 
}
