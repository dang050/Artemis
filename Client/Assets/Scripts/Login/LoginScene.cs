﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Protocol;
using System;

public class LoginScene : MonoBehaviour {

    #region 登录面板部分
    [SerializeField]
    private InputField accountInput;
    [SerializeField]
    private InputField passwdInput;
    [SerializeField]
    private Button loginBtn;
    #endregion

    #region 注册面板部分
    [SerializeField]
    private InputField regAccountInput;
    [SerializeField]
    private InputField regPasswdInput1;
    [SerializeField]
    private InputField regPasswdInput2;
    [SerializeField]
    private Button regRegisterBtn;
    #endregion

    [SerializeField]
    private GameObject regPanel;

    public void OnLoginClick()
    {
        if (accountInput.text.Length == 0 || accountInput.text.Length > 10)
        {
            WarningManager.errors.Add(new WarningModel("帐号不合法"));
            return;
        }
        if (passwdInput.text.Length == 0 || accountInput.text.Length > 10)
        {
            WarningManager.errors.Add(new WarningModel("帐号不合法"));
            return;
        }
        loginBtn.interactable = false;
        C2SLoginReq loginReq = new C2SLoginReq();
        loginReq.system = (UInt32)SystemId.aeLoginSystem;
        loginReq.cmd = (UInt32)tagLoginSystemCmd.cLogin;
        loginReq.account = System.Text.Encoding.Default.GetBytes(accountInput.text);
        loginReq.passwd = System.Text.Encoding.Default.GetBytes(passwdInput.text);

        WriterPack pack = new WriterPack();
        pack.Encode(loginReq);
        Network.Net.SendMessage(pack);
        return;
    }

    public void OnRegClick()
    {
        regPanel.SetActive(true);
    }

    public void OnRegPanelCloseClick()
    {
        regPanel.SetActive(false);
    }

    public void OnRegPanelRegClick()
    {
        if (regAccountInput.text.Length == 0 || regAccountInput.text.Length > 10)
        {
            WarningManager.errors.Add(new WarningModel("帐号不合法"));
            return;
        }
        if (regPasswdInput1.text.Length == 0 || regPasswdInput1.text.Length > 10)
        {
            WarningManager.errors.Add(new WarningModel("帐号不合法"));
            return;
        }
        if (regPasswdInput2.text.Length == 0 || regPasswdInput2.text.Length > 10 || regPasswdInput2.text != regPasswdInput1.text)
        {
            WarningManager.errors.Add(new WarningModel("帐号不合法"));
            return;
        }
        regRegisterBtn.interactable = false;

        C2SLoginReq loginReq = new C2SLoginReq();
        loginReq.system = (UInt32)SystemId.aeLoginSystem;
        loginReq.cmd = (UInt32)tagLoginSystemCmd.cRegister;
        loginReq.account = System.Text.Encoding.Default.GetBytes(regAccountInput.text);
        loginReq.passwd = System.Text.Encoding.Default.GetBytes(regPasswdInput1.text);

        WriterPack pack = new WriterPack();
        pack.Encode(loginReq);
        Network.Net.SendMessage(pack);
    }

    void SetLoginBtnState(bool state)
    {
        loginBtn.interactable = state;
    }

    void SetRegisterBtnState(bool state)
    {
        regRegisterBtn.interactable = state;
    }
}
