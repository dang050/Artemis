﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using GameData;
using Protocol;

public class LoginHandler : MonoBehaviour, IHandler {

    private ProcessNetMessageDelegate[] handler;

    void Awake()
    {
        handler = new ProcessNetMessageDelegate[]
        {
            ErrorHandler,
            ProcessLoginRsp,
            ProcessRegisterRsp,
            ProcessGetUserDataRsp,
            ProcessCreateUserRsp,
        };
    }

    public void ProcessNetMessage(UInt32 cmd, Pack pack)
    {
        handler[cmd](pack);
    }

    private void ErrorHandler(Pack pack)
    {
        WarningManager.errors.Add(new WarningModel("未知命令"));
    }

    private void ProcessLoginRsp(Pack pack)
    {
        Protocol.S2CLoginRsp data = pack as Protocol.S2CLoginRsp;
        string msg;
        switch (data.result)
        {
            case 1:
                 msg = "登录成功";
                SceneManager.LoadScene("Main");
                break;
            case 2:
                msg = "登录失败";
                break;
            default:
                msg = "未知返回值";
                break;
        }
        SendMessage("SetLoginBtnState", true);
        WarningManager.errors.Add(new WarningModel(msg));
    }

    private void ProcessRegisterRsp(Pack pack)
    {
        Protocol.S2CLoginRsp data = pack as Protocol.S2CLoginRsp;
        string msg;
        switch (data.result)
        {
            case 1:
                msg = "注册成功";
                break;
            default:
                msg = "注册失败";
                break;
        }
        WarningManager.errors.Add(new WarningModel(msg, delegate
        {
            SendMessage("SetRegisterBtnState", true);
        }));
    }

    private void ProcessGetUserDataRsp(Pack pack)
    {
        Protocol.S2CGetUserDataRsp data = pack as Protocol.S2CGetUserDataRsp;
        if (data.user == null || data.user.Length == 0)
        {
            // 弹出创建角色面板
            SendMessage("SetCreatePanelState", true);
        }
        else
        {
            // 给角色信息赋值
            UserData.user = new User(data.user[0].id, System.Text.Encoding.Default.GetString(data.user[0].name), data.user[0].level, data.user[0].model, data.user[0].scene, data.user[0].x, data.user[0].z);
            WarningManager.errors.Add(new WarningModel("角色信息获取成功"));
            SendMessage("LoadUserData");
        }
    }

    private void ProcessCreateUserRsp(Pack pack)
    {
        Protocol.S2CCreateUserRsp data = pack as Protocol.S2CCreateUserRsp;
        switch (data.result)
        {
            case 1:
                // 成功
                WarningManager.errors.Add(new WarningModel("角色创建成功", delegate
                {
                    // 请求玩家数据
                    C2SGetUserDataReq req = new C2SGetUserDataReq();
                    req.system = (UInt32)SystemId.aeLoginSystem;
                    req.cmd = (UInt32)tagLoginSystemCmd.cGetUserData;
                    WriterPack wpack = new WriterPack();
                    wpack.Encode(req);
                    Network.Net.SendMessage(wpack);
                }));
                break;
            default:
                // 失败
                WarningManager.errors.Add(new WarningModel("角色创建失败"));
                break;
        }
    }
    public static Pack DecodeLoginSystem(UInt32 cmd, byte[] data)
    {
        Pack pack;
        switch (cmd)
        {
            case (UInt32)tagLoginSystemCmd.sLogin:
                pack = ReaderPack.Decode<Protocol.S2CLoginRsp>(data);
                break;
            case (UInt32)tagLoginSystemCmd.sRegister:
                pack = ReaderPack.Decode<Protocol.S2CLoginRsp>(data);
                break;
            case (UInt32)tagLoginSystemCmd.sGetUserData:
                pack = ReaderPack.Decode<Protocol.S2CGetUserDataRsp>(data);
                break;
            case (UInt32)tagLoginSystemCmd.sCreateUser:
                pack = ReaderPack.Decode<Protocol.S2CCreateUserRsp>(data);
                break;
            default:
                pack = null;
                break;
        }
        return pack;
    }
}
