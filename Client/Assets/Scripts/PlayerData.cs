﻿using UnityEngine;
using System.Collections;
using System;
using GameData;

// 用来存储其它玩家的角色数据，自身的数据通过GameData.UserData来获取，不在这里
public class PlayerData : MonoBehaviour {
    private User data;
    [SerializeField]
    private PlayerTitle title;

    public void InitPlayerData(User data)
    {
        this.data = data;
        title.Init(data);
    }

    public void SetPosition(float x, float z)
    {
        data.position.x = x;
        data.position.z = z;
        // 如果是主角自己，还要修改UserData里面的数据
        if (data.id == UserData.user.id)
        {
            UserData.user.position.x = x;
            UserData.user.position.z = z;
        }
    }

    public Vector3 GetPosition()
    {
        return data.position;
    }
}
