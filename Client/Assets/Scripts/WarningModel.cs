﻿using UnityEngine;
using System.Collections;

public class WarningModel{

    public string text;
    public WarningResult function;
    public WarningModel(string s, WarningResult e = null)
    {
        text = s;
        function = e;
    }
}
