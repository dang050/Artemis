﻿using UnityEngine;
using System.Collections;
using GameData;
using Protocol;
using System;
using UnityEngine.UI;
using System.Text;
using UnityEngine.SceneManagement;

public class MainScene : MonoBehaviour {
    [SerializeField]
    private GameObject mask;    // 用户遮罩面板，防止误操作
    [SerializeField]
    private GameObject createPanel;
    [SerializeField]
    private InputField nameInput;
    [SerializeField]
    private Button createBtn;
    [SerializeField]
    private Text userInfoText;

	void Start () {
	    if (UserData.user == null)
        {
            mask.SetActive(true);
            // 向服务器请求玩家数据
            C2SGetUserDataReq req = new C2SGetUserDataReq();
            req.system = (UInt32)SystemId.aeLoginSystem;
            req.cmd = (UInt32)tagLoginSystemCmd.cGetUserData;
            WriterPack pack = new WriterPack();
            pack.Encode(req);
            Network.Net.SendMessage(pack);
        }
	}
	
    public void OnStartBtnClick()
    {
        C2SEnterSceneReq req = new C2SEnterSceneReq();
        req.system = (UInt32)SystemId.aeSceneSystem;
        req.cmd = (UInt32)tagSceneSystemCmd.cEnterScene;
        req.scene = UserData.user.scene;
        WriterPack pack = new WriterPack();
        pack.Encode(req);
        Network.Net.SendMessage(pack);
    }

    public void OnCreateBtnClick()
    {
        if (nameInput.text.Length == 0 || nameInput.text.Length > 10)
        {
            WarningManager.errors.Add(new WarningModel("角色名不合法"));
            return;
        }
        C2SCreateUserReq req = new C2SCreateUserReq();
        req.system = (UInt32)SystemId.aeLoginSystem;
        req.cmd = (UInt32)tagLoginSystemCmd.cCreateUser;
        req.name = System.Text.Encoding.Default.GetBytes(nameInput.text);
        // todo 这里暂时写死模型为1
        req.model = 1;         
        WriterPack pack = new WriterPack();
        pack.Encode(req);
        Network.Net.SendMessage(pack);
        return;
    }

    void SetCreatePanelState(bool state)
    {
        mask.SetActive(false);
        createPanel.SetActive(state);
    }

    void LoadUserData()
    {
        mask.SetActive(false);
        createPanel.SetActive(false);
        userInfoText.text = UserData.user.name + " 等级:" + UserData.user.level;
    }
}
