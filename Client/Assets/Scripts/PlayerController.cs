﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public enum AnimatorState : int
    {
        IDLE = 0,
        RUN = 1,
        ATTACK = 2,
        SKILL = 3,
        DEAD = 4,
    };
    [SerializeField]
    private Animator anim;

    // 记录人物当前状态
    private AnimatorState state = AnimatorState.IDLE;
    private CharacterController controller;

    // todo 这里可以改成用个队列来存储目标点，每次移动到目的地后
    // todo 从队列里面取出下一个目标点，一直在目标点为空的时候，则停止移动
    // todo 这里就要控制好客户端和服务器之间的移动消息间隔时间，不能太短
    // todo 消息间隔略小于角色移动到一个目标点所花的时间就可以了
    // todo 避免频繁移动消息，导致客户端和服务器之间角色的坐标误差太大
    private Vector3 dstPoint;           // 目的地坐标
    private float moveSpeed = 0.1f;     // 移动速度
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }

    /// <summary>
    /// 处理移动
    /// </summary>
    /// <param name="dst"></param>
    /// <param name="speed"></param>
    public void Move(Vector3 dst, float speed)
    {
        dst.y = transform.position.y;
        dstPoint = dst;
        moveSpeed = speed;
        transform.LookAt(dstPoint);
        state = AnimatorState.RUN;
        anim.SetInteger("state", (int)state);
    }

    /// <summary>
    /// 处理普通攻击
    /// </summary>
    public void Attack()
    {

    }

    void Start()
    {
        anim.SetInteger("state", (int)state);
        controller = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        switch (state)
        {
            case AnimatorState.RUN:
                RealMove();
                break;
            default:
                break;
        }
    }


    void RealMove(){
        // 判断有没有到达目的地
        if (Mathf.Abs(Vector3.Distance(dstPoint, transform.position)) >= 0.1f)
        {
            Vector3 v = Vector3.ClampMagnitude(dstPoint - transform.position, moveSpeed / 10f);
            // todo 动画播放速度和移动速度相关10倍
            anim.speed = moveSpeed;
            controller.Move(v);
        }
        else
        {
            state = AnimatorState.IDLE;
            // 播放idle动画
            anim.SetInteger("state", (int)state);   
        }
	}
}
