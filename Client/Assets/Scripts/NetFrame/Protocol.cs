﻿using System;

namespace Protocol
{
    enum SystemId : uint
    {
        aeLoginSystem = 1,
        aeSceneSystem = 2,
    }

    enum tagLoginSystemCmd : uint
    {
        cLogin = 1,
	    cRegister = 2,
        cGetUserData = 3,
        cCreateUser = 4,

	    cMaxCodeId,

	    sLogin = 1,
	    sRegister = 2,
        sGetUserData = 3,
        sCreateUser = 4,
    }

    enum tagSceneSystemCmd : uint
    {
        cMove = 1,
        cEnterScene = 2,
        cLeaveScene = 3,
        cEnterSceneFinish = 4,

        cMaxCodeId,

        sMove = 1,
        sEnterScene = 2,
        sLeaveScene = 3,
        sEnterSceneBro = 4,     // 其它玩家进入场景的广播
        sMoveBro = 5,           // 其它玩家移动的广播
        sLeaveSceneBro = 6,     // 其它玩家离开的广播
    }

    public class C2SLoginReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
        public byte[] account;
        public byte[] passwd;
    }

    public class S2CLoginRsp : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
        public byte result;     //1成功，2失败
    }

    public class C2SGetUserDataReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
    }

    public class S2CGetUserDataRsp : Pack
    {
        public class data : Pack
        {
            public UInt32   id;      // 唯一ID
            public byte[]   name;    // 名称
            public UInt16   level;   // 等级
            public UInt32   model;   // 模型id
            public UInt32   scene;   // 场景id
            public Int32    x;
            public Int32    z;
        }
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
        public data[] user;
    }

    public class C2SCreateUserReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
        public byte[] name;
        public UInt32 model;
    }

    public class S2CCreateUserRsp : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeLoginSystem;
        public UInt32 cmd;
        public byte result;
    }

    public class C2SMoveReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public int nCurrX;     // 当前坐标
        public int nCurrZ;
        // 移动方向 0(0,1) 1(1,1) 2(-1,1) 3(1,0) 4(-1,0) 5(1,-1) 6(-1,-1) 7(0,-1) 总共8个方向
        public int dir;        
    }
    public class S2CMoveRsp : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public UInt32 speed;   // 移动速度
        public int x;      // 移动后的坐标
        public int z;
    }

    public class C2SEnterSceneReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public UInt32 scene;
    }

    public class C2SEnterSceneFinishReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
    }

    public class S2CEnterSceneRsp : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public byte result;
    }

    public class C2SLeaveSceneReq : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
    }

    public class S2CLeaveSceneRsp : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public byte result;
    }

    public class S2CEnterSceneBro : Pack
    {
        public class role : Pack
        {
            public UInt32 id;      // 唯一ID
            public byte[] name;    // 名称
            public UInt16 level;   // 等级
            public UInt32 model;   // 模型id
            public Int32 x;
            public Int32 z;
        }
        public class monster : Pack
        {

        }

        public class goods : Pack
        {

        }

        public class skill : Pack
        {

        }
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public monster[]    mons;
        public role[]       user;
        public goods[]      goodsInfo;
        public skill[]      skills;
    }

    public class S2CMoveBro : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public UInt32 id;       // 玩家id
        public UInt32 speed;   // 移动速度
        public int x;      // 移动后的坐标
        public int z;
        public byte entityType;// 实体的类型，1怪物|2玩家|3物品|4技能
    }

    public class S2CLeaveSceneBro : Pack
    {
        public UInt32 system = (UInt32)SystemId.aeSceneSystem;
        public UInt32 cmd;
        public class data : Pack
        {
            public UInt32 id;       // 唯一ID
            public byte entityType; // 实体的类型，1怪物|2玩家|3物品|4技能
        }
        public data[] entitys;
    }
}