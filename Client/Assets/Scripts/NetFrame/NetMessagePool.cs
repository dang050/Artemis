﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class NetMessagePool
{
    /// <summary>
    /// 单例模式
    /// </summary>
    private static NetMessagePool instance;
    public static NetMessagePool Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new NetMessagePool();
            }
            return instance;
        }
    }

    private delegate Pack DecodeNetMessageDelegate(UInt32 cmd, byte[] data);
    private DecodeNetMessageDelegate[] decodeHandler;
    private NetMessagePool()
    {
        decodeHandler = new DecodeNetMessageDelegate[]
        {
            ErrorDecodeSystem,
            LoginHandler.DecodeLoginSystem,
            SceneHandler.DecodeLoginSystem,
        };
    }

    // 消息容器
    Queue<NetMessage> messages = new Queue<NetMessage>();

    /// <summary>
    /// push一个网络消息，根据system进行反序列化
    /// </summary>
    /// <param name="data"></param>
    public void push(byte[] data)
    {
        UInt32 system = (UInt32)((data[3] << 24) | (data[2] << 16) | (data[1] << 8) | data[0]);
        UInt32 cmd = (UInt32)((data[7] << 24) | (data[6] << 16) | (data[5] << 8) | data[4]);
        NetMessage msg = new NetMessage();
        msg.system = system;
        msg.cmd = cmd;
        // 根据不同的子系统，调用对应的反序列化操作
        msg.pack = decodeHandler[system](cmd, data);
        messages.Enqueue(msg);
    }

    /// <summary>
    /// pop一个网络消息
    /// </summary>
    /// <returns></returns>
    public NetMessage pop()
    {
        if (messages.Count > 0)
        {
            return messages.Dequeue();
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// 消息列队是否为空
    /// </summary>
    /// <returns></returns>
    public bool Empty()
    {
        if (messages.Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private Pack ErrorDecodeSystem(UInt32 cmd, byte[] data)
    {
        return null;
    }
}

