﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// 声明一个处理具体网络数据包的委托
/// </summary>
/// <param name="pack"></param>
public delegate void ProcessNetMessageDelegate(Pack pack);

public class NetMessageUtil : MonoBehaviour
{
    IHandler loginHandler;
    IHandler sceneHandler;
    // Use this for initialization
    void Start()
    {
        loginHandler = GetComponent<LoginHandler>();
        sceneHandler = GetComponent<SceneHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        while (!NetMessagePool.Instance.Empty())
        {
            NetMessage msg = NetMessagePool.Instance.pop();
            StartCoroutine("ProcessNetMessage", msg);
        }
    }

    void ProcessNetMessage(NetMessage msg)
    {
        switch (msg.system)
        {
            case (uint)Protocol.SystemId.aeLoginSystem:
                loginHandler.ProcessNetMessage(msg.cmd, msg.pack);
                break;
            case (uint)Protocol.SystemId.aeSceneSystem:
                sceneHandler.ProcessNetMessage(msg.cmd, msg.pack);
                break;
            default:
                break;
        }
    }
}
