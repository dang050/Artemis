﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System;
using System.IO;
using Protocol;
using System.Collections.Generic;

public class Network
{
    int serverPort = 12345;
    string serverIP = "127.0.0.1";

    /// <summary>
    /// 网络模块，单例模式
    /// </summary>
    private static Network net;
    public static Network Net
    {
        get
        {
            if (net == null)
            {
                net = new Network();
            }
            return net;
        }
    }

    ReceivePacket recvPack = new ReceivePacket();
    byte[] recvBuffer = new byte[1024];

    Socket client;


    private Network()
    {
        ConnectionServer();
    }

    ~Network()
    {
        if (Connected())
        {
            Disconnect();
        }
    }
    public int Port
    {
        get
        {
            return serverPort;
        }
        set
        {
            serverPort = value;
        }
    }

    public string IP
    {
        get
        {
            return serverIP;
        }
        set
        {
            serverIP = value;
        }
    }

    public bool Connected()
    {
        if (client != null)
            return client.Connected;
        else
            return false;
    }
    public void ConnectionServer()
    {
        // 连接服务器
        try
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // 使用阻塞模式连接
            client.Connect(serverIP, serverPort);
            client.BeginReceive(recvBuffer, 0, 1024, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);

//            IPAddress ip = IPAddress.Parse(serverIP);
//            IPEndPoint iep = new IPEndPoint(ip, serverPort);
//            client.BeginConnect(iep, new AsyncCallback(ConnectCallBack), null);
        }
        catch { }
    }

    private void ConnectCallBack(IAsyncResult asy)
    {
        try
        {
            client.EndConnect(asy);
            client.BeginReceive(recvBuffer, 0, recvBuffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);
        }
        catch
        {
            client.Close();
            throw new Exception("Connect Server Failed!");
        }
    }

    private void RecieveCallBack(IAsyncResult asy)
    {
        try
        {
            // 解决粘包问题
            int recvLen = client.EndReceive(asy);
            var binaryReader = new BinaryReader(new MemoryStream(recvBuffer));

            while (recvLen > 0)
            {
                UInt32 packLen = 0;
                if (recvPack.len == 0)
                {
                    packLen = binaryReader.ReadUInt32() - sizeof(UInt32);     // 这里要减去4字节包的长度
                    recvPack.len = packLen;
                    recvLen -= 4;
                }
                else
                {
                    packLen = recvPack.len;
                }
                UInt32 len = Math.Min((UInt32)recvLen, packLen - (UInt32)recvPack.data.Count);    // 剩余长度
                recvPack.data.AddRange(binaryReader.ReadBytes((int)len));

                if (recvPack.data.Count == packLen)
                {
                    // 一个完整的包接收完毕
                    NetMessagePool.Instance.push(recvPack.data.ToArray());
                    recvPack.Clear();
                }

                recvLen -= (int)len;
            }
            binaryReader.Close();

            // 继续监听下一次的数据接收
            client.BeginReceive(recvBuffer, 0, recvBuffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);
        }
        catch
        {
            // 远程服务器断开连接
            client.Close();
        }
    }

    public void SendMessage(WriterPack pack)
    {
        try
        {
            byte[] data = pack.ToBytes();
            client.BeginSend(data, 0, pack.Len, SocketFlags.None, new AsyncCallback(SendCallBack), null);
        }
        catch { }
    }

    private void SendCallBack(IAsyncResult asy)
    {
        try
        {
            client.EndSend(asy);
        }
        catch { }
    }

    public void Disconnect()
    {
        try
        {
            client.Shutdown(SocketShutdown.Both);
            client.Close();
        }
        catch { }
    }
}