﻿using UnityEngine;
using System.Collections;

public class PlayerTitle: MonoBehaviour {
    [SerializeField]
    private Transform front;
    [SerializeField]
    private TextMesh nameText;
    private float hp;

    void LateUpdate()
    {
        if (transform.rotation != Camera.main.transform.rotation)
            transform.rotation = Camera.main.transform.rotation;
    }

    public void Init(GameData.User user)
    {
        nameText.text = user.name;
        SetHp(0.8f);
    }

    private void SetHp(float value)
    {
        hp = value;
        front.localScale = new Vector3(hp, 1);
        front.localPosition = new Vector3((1 - hp) * -0.8f, 0);
    }
}
