﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace GameData
{
    enum EntityType : byte
    {
        MONSTER = 1,    // TYPE需要大于0
        PLAYER = 2,
        GOODS = 3,
        SKILL = 4,
    }

    public class User
    {
        public UInt32   model;      // 模型ID    
        public UInt32   id;         // 唯一ID
        public string   name;       // 名称
        public UInt16   level;      // 等级
        public UInt32   scene;      // 声明ID
        public Vector3 position;    // 位置
        
        public User(UInt32 id, string name, UInt16 level, UInt32 model, UInt32 scene, Int32 x, Int32 z)
        {
            this.id = id;
            this.name = name;
            this.level = level;
            this.model = model;
            this.scene = scene;
            // 这里暂时写死60
            this.position = new Vector3(x, 60, z);
        }
    }

    // 主角的数据
    public class UserData
    {
        public static User user = null;
    }
}


