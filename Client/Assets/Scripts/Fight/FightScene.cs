﻿using UnityEngine;
using System.Collections.Generic;
using GameData;
using UnityStandardAssets.Utility;
using Protocol;
using System;

public class FightScene : MonoBehaviour {
    // 场景玩家数据字典
    private Dictionary<uint, GameObject>    players = new Dictionary<uint,GameObject>();

    public static FightScene instance;

    void Awake()
    {
        instance = this;

        // 创建玩家角色
        GameObject player = CreatePlayer(UserData.user);
        // 将相机SmoothFollow的target设为自身
        Camera.main.GetComponent<SmoothFollow>().target = player.transform;
        GetComponent<SceneHandler>().player = player;
    }

	void Start () {
        // 发送cEnterSceneFinish消息
        C2SEnterSceneFinishReq req = new C2SEnterSceneFinishReq();
        req.system = (UInt32)SystemId.aeSceneSystem;
        req.cmd = (UInt32)tagSceneSystemCmd.cEnterSceneFinish;
        WriterPack pack = new WriterPack();
        pack.Encode(req);
        Network.Net.SendMessage(pack);
    }


    void Destroy()
    {
        foreach (GameObject obj in players.Values)
        {
            Destroy(obj);
        }
    }

    /// <summary>
    /// 实例化一个角色模型到场景中
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public GameObject CreatePlayer(User user)
    {
        GameObject playerPrefab = (GameObject)Resources.Load("Player/" + user.model);
        GameObject player = (GameObject)Instantiate(playerPrefab, new Vector3(user.position.x * 1.0f, user.position.y * 1.0f, user.position.z * 1.0f), playerPrefab.transform.rotation);
        player.GetComponent<PlayerData>().InitPlayerData(user);

        players.Add(user.id, player);
        return player;
    }

    /// <summary>
    /// 从场景中销毁一个角色
    /// </summary>
    /// <param name="id"></param>
    public void DestroyPlayer(uint id)
    {
        if (players.ContainsKey(id))
        {
            Destroy(players[id]);
            players.Remove(id);
        }
    }

    /// <summary>
    /// 根据玩家id，获取场景中的gameobject
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public GameObject GetPlayerById(uint id)
    {
        if (players.ContainsKey(id))
        {
            return players[id];
        }
        return null;
    }
}
