﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SceneEvent : MonoBehaviour, IPointerClickHandler
{
    private SceneHandler sceneHandler;
    void Start()
    {
        sceneHandler = FightScene.instance.GetComponent<SceneHandler>();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        switch (eventData.pointerId)
        {
            case PointerInputModule.kMouseRightId:
                RightClick(eventData.position);
                break;
            default:
                break;
        }
    }

    void RightClick(Vector2 pos)
    {
        Ray ray = Camera.main.ScreenPointToRay(pos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            sceneHandler.Move(hit.point);
        }

        // 先碰撞的在后面
        //RaycastHit[] hits = Physics.RaycastAll(ray, 200);
        //for (int i = hits.Length - 1; i >= 0; i--)
        //{
        //    RaycastHit item = hits[i];
        //    //如果是敌方单位 则进行普通攻击
        //    //己方单位无视
        //    //如果是地板层 则开始寻路
        //    if (item.transform.gameObject.layer == LayerMask.NameToLayer("Water"))
        //    {
                
        //        return;
        //    }
        //}
    }
}
