﻿using UnityEngine;
using System.Collections;
using System;
using Protocol;

public class FightHandler : MonoBehaviour,IHandler {

    private ProcessNetMessageDelegate[] handler;

    void Awake()
    {
        handler = new ProcessNetMessageDelegate[]
        {
            ErrorHandler,
        };
    }

    private void ErrorHandler(Pack pack)
    {
        WarningManager.errors.Add(new WarningModel("未知命令"));
    }

    public void ProcessNetMessage(uint cmd, Pack pack)
    {
        handler[cmd](pack);
    }


    public static Pack DecodeLoginSystem(UInt32 cmd, byte[] data)
    {
        Pack pack;
        switch (cmd)
        {
            case (UInt32)tagSceneSystemCmd.sMove:
                pack = ReaderPack.Decode<S2CMoveRsp>(data);
                break;
            default:
                pack = null;
                break;
        }
        return pack;
    }
}
