﻿using GameData;
using Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneHandler : MonoBehaviour, IHandler
{
    [HideInInspector]
    public GameObject player;      // 主角角色
    private float moveTime;                         // 上次移动的时间
    private float moveIntervalTime = 0.1f;     // 移动操作的间隔时间
    private Vector3 dstPoint;       // 目的点坐标

    WriterPack pack = new WriterPack();

    private ProcessNetMessageDelegate[] handler;

    /// <summary>
    /// 玩家移动操作 由外部调用
    /// </summary>
    /// <param name="dst"></param>
    public void Move(Vector3 dst)
    {
        // 加上移动操作的间隔
        if (Time.time - moveTime >= moveIntervalTime)
        {
            // todo 这里让y轴暂时不变
            dst.y = UserData.user.position.x;
            dst.x = Mathf.Round(dst.x);         // 不要小数
            dst.z = Mathf.Round(dst.z);         // 不要小数
            if (Mathf.Abs(Vector3.Distance(dstPoint, dst)) >= 1.0f)     // 判断是否同一个目标点
            {
                dstPoint = dst;
                int dir = CalcDir(UserData.user.position, dst);
                if (dir == -1)
                {
                    return;
                }
                // 发送移动消息
                C2SMoveReq req = new C2SMoveReq();
                req.system = (UInt32)SystemId.aeSceneSystem;
                req.cmd = (UInt32)tagSceneSystemCmd.cMove;
                req.nCurrX = Mathf.RoundToInt(UserData.user.position.x);
                req.nCurrZ = Mathf.RoundToInt(UserData.user.position.z);
                req.dir = dir;

                pack.Encode(req);
                Network.Net.SendMessage(pack);
                moveTime = Time.time;
                pack.Clear();
            }
        }
    }

    /// <summary>
    /// 计算移动方向
    /// </summary>
    /// <param name="src"></param>
    /// <param name="dst"></param>
    /// <returns></returns>
    private int CalcDir(Vector3 src, Vector3 dst)
    {
        Vector3 v = dst - src;

        int x = 0;
        int z = 0;
        if (v.x < 0)
        {
            x = (int)(v.x - 0.5);
        }
        else
        {
            x = (int)(v.x + 0.5);
        }
        if (x != 0)
        {
            x = x / Mathf.Abs(x);
        }
        if (v.z < 0)
        {
            z = (int)(v.z - 0.5);
        }
        else
        {
            z = (int)(v.z + 0.5);
        }
        if (z != 0)
        {
            z = z / Mathf.Abs(z);
        }

        if (x == 0 && z == 1)
        {
            return 0;
        }
        else if (x == 1 && z == 1)
        {
            return 1;
        }
        else if (x == -1 && z == 1)
        {
            return 2;
        }
        else if (x == 1 && z == 0)
        {
            return 3;
        }
        else if (x == -1 && z == 0)
        {
            return 4;
        }
        else if (x == 1 && z == -1)
        {
            return 5;
        }
        else if (x == -1 && z == -1)
        {
            return 6;
        }
        else if (x == 0 && z == -1)
        {
            return 7;
        }
        else
        {
            return -1;
        }
    }


    void Awake()
    {
        handler = new ProcessNetMessageDelegate[]
        {
            ErrorHandler,
            ProcessMoveRsp,
            ProcessEnterSceneRsp,
            ErrorHandler,       // sLeaveScene
            ProcessEnterSceneBro,
            ProcessMoveBro,       // sMoveBro
            ProcessLeaveSceneBro,
        };
    }


    private void ErrorHandler(Pack pack)
    {
        WarningManager.errors.Add(new WarningModel("未知命令"));
    }

    public void ProcessNetMessage(uint cmd, Pack pack)
    {
        handler[cmd](pack);
    }

    private void ProcessMoveBro(Pack packet)
    {
        S2CMoveBro data = packet as S2CMoveBro;
        switch (data.entityType)
        {
            case (byte)EntityType.PLAYER:
                GameObject role = FightScene.instance.GetPlayerById(data.id);
                if (role == null) return;
                PlayerData role_data = role.GetComponent<PlayerData>();
                // 修改其它玩家坐标
                role_data.SetPosition(data.x, data.z);
                // 处理移动
                role.GetComponent<PlayerController>().Move(role_data.GetPosition(), data.speed);
                break;
            case (byte)EntityType.MONSTER:
                break;
            case (byte)EntityType.GOODS:
                break;
            case (byte)EntityType.SKILL:
                break;
        }
    }

    private void ProcessEnterSceneBro(Pack packet)
    {
        S2CEnterSceneBro data = packet as S2CEnterSceneBro;
        // todo 根据不同的实体类型创建不同的实体
        if (data.user == null || data.user.Length == 0)
        {
            return;
        }
        for (int i = 0; i < data.user.Length; ++i)
        {
            // 创建场景里的角色
            FightScene.instance.CreatePlayer(new User(
                data.user[i].id,
                System.Text.Encoding.Default.GetString(data.user[i].name),
                data.user[i].level,
                data.user[i].model,
                UserData.user.scene,
                data.user[i].x,
                data.user[i].z
                ));
        }
    }

    private void ProcessLeaveSceneBro(Pack packet)
    {
        S2CLeaveSceneBro data = packet as S2CLeaveSceneBro;
        if (data.entitys == null || data.entitys.Length == 0)
        {
            return;
        }
        for (int i = 0; i < data.entitys.Length; ++i)
        {
            // todo 根据不同的实体类型销毁不同的实体
            switch (data.entitys[i].entityType)
            {
                case (byte)EntityType.MONSTER:
                    break;
                case (byte)EntityType.PLAYER:
                    FightScene.instance.DestroyPlayer(data.entitys[i].id);
                    break;
                case (byte)EntityType.GOODS:
                    break;
                case (byte)EntityType.SKILL:
                    break;
                default:
                    break;
            }
        }
    }
    private void ProcessEnterSceneRsp(Pack packet)
    {
        S2CEnterSceneRsp data = packet as S2CEnterSceneRsp;
        if (data.result == 1)
        {
            SceneManager.LoadScene("Fight" + UserData.user.scene);
        }
        else
        {
            WarningManager.errors.Add(new WarningModel("进入场景失败"));
        }
    }
    private void ProcessMoveRsp(Pack packet)
    {
        S2CMoveRsp data = packet as S2CMoveRsp;
        // 修改当前坐标
        player.GetComponent<PlayerData>().SetPosition(data.x, data.z);
        // 处理移动
        player.GetComponent<PlayerController>().Move(UserData.user.position, data.speed);
        
        // 判断是否已到达目标点
        if (Mathf.Abs(Vector3.Distance(dstPoint, UserData.user.position)) >= 0.5f)
        {
            // 继续发送移动消息
            C2SMoveReq req = new C2SMoveReq();
            req.system = (UInt32)SystemId.aeSceneSystem;
            req.cmd = (UInt32)tagSceneSystemCmd.cMove;
            req.nCurrX = data.x;
            req.nCurrZ = data.z;
            int dir = CalcDir(UserData.user.position, dstPoint);
            req.dir = dir;
            
            pack.Encode(req);
            Network.Net.SendMessage(pack);
            pack.Clear();
        }
    }

    public static Pack DecodeLoginSystem(UInt32 cmd, byte[] data)
    {
        Pack pack;
        switch (cmd)
        {
            case (UInt32)tagSceneSystemCmd.sMove:
                pack = ReaderPack.Decode<S2CMoveRsp>(data);
                break;
            case (UInt32)tagSceneSystemCmd.sEnterScene:
                pack = ReaderPack.Decode<S2CEnterSceneRsp>(data);
                break;
            case (UInt32)tagSceneSystemCmd.sEnterSceneBro:
                pack = ReaderPack.Decode<S2CEnterSceneBro>(data);
                break;
            case (UInt32)tagSceneSystemCmd.sLeaveSceneBro:
                pack = ReaderPack.Decode<S2CLeaveSceneBro>(data);
                break;
            case (UInt32)tagSceneSystemCmd.sMoveBro:
                pack = ReaderPack.Decode<S2CMoveBro>(data);
                break;
            default:
                pack = null;
                break;
        }
        return pack;
    }
}

