﻿using UnityEngine;
using System.Collections;

public class WarFogTest : MonoBehaviour {

    [SerializeField]
    private RenderTexture mask;
    [SerializeField]
    private Material mat;

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        mat.SetTexture("_MaskTex", mask);
        Graphics.Blit(src, dest, mat);
    }

}
