﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public delegate void WarningResult();
public class WarningPanel : MonoBehaviour {
    WarningResult err = null;
    public Text text;

    public void error(WarningModel model)
    {
        text.text = model.text;
        err = model.function;
        gameObject.SetActive(true);
    }

    public void close()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        if (err != null)
        {
            err();
        }
    }
	
}
