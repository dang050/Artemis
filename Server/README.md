#Server

服务器环境依赖
  - Mysql数据库
  - 使用VS2013/VS2015编译

服务器路径设置
  - 3rdparty文件夹用于存放第三方库
  - 配置属性=》VC++目录=》引用目录=》..\3rdparty
  - 配置属性=》VC++目录=》库目录=》..\3rdparty
  - 配置属性=》C/C++=》常规=》附加包含目录=》..\3rdparty\lua\include;..\3rdparty\tolua++\include;(64位版本配置对应的64位路径)
  - 配置属性=》链接器=》常规=》附加库目录=》..\3rdparty\tolua++;..\3rdparty\lua;(64位版本配置对应的64位路径)
  - 配置属性=》链接器=》输入=》lua5.1.lib;tolua++_$(PlatformName).lib;
  - 给pkg文件自定义生成命令：..\3rdparty\tolua++\tolua++.exe -n ServerInterface -o server_interface.cpp server_interface.pkg
  
服务器配置文件说明
  - 服务器配置文件位于Config Filter里面
  - database.sql 用于建库建表相关的SQL语句，每次数据库结构有更新，会将对应的SQL语句放在文件里
  - mysql_config.json 用于配置数据库的连接信息
  - scenes.json 用于配置场景的总体信息
  - scene_x.json 用于配置具体场景的具体信息，x为场景的唯一id


