#include "sql_helper.h"
#include <sstream>
#include "mysql_conn_manager.h"
#include "logger.h"

namespace SqlHelper
{
	int SelectIdFromUser(const std::string& name, const std::string& passwd)
	{
		std::ostringstream sql;
		sql << "select `id` from `user` where `name` = '" << name << "' and `passwd` = '" << passwd << "'";
		try
		{
			auto res = MysqlConnManager::GetInstance()->Query(sql.str().c_str());
			if (res->GetCurrentNumRows() <= 0)
			{
				return -1;
			}
			else
			{
				auto result = res->GetCurrentRow();
				return std::stoi(result[0]);
			}
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
		return -1;
	}

	bool CheckIdIsExistInUser(const std::string& name)
	{
		std::ostringstream sql;
		sql << "select `id` from `user` where `name` = '" << name << "'";
		try
		{
			auto res = MysqlConnManager::GetInstance()->Query(sql.str().c_str());
			if (res->GetCurrentNumRows() <= 0)
			{
				return false;
			}
			return true;
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
		return false;
	}

	void InsertIntoUser(const std::string& name, const std::string& passwd)
	{
		std::ostringstream sql;
		sql << "insert into `user`(`name`, `passwd`) values( '" << name << "','" << passwd << "')";
		try
		{
			MysqlConnManager::GetInstance()->Exec(sql.str().c_str());
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
	}

	bool CreatePlayerData(unsigned int id, const std::string& name, unsigned int modelId, int x, int y, unsigned int sceneId, unsigned int subSceneId)
	{
		std::ostringstream sql;
		sql << "select `id` from `player_data` where `name` = '" << name << "'";
		try
		{
			// �жϽ�ɫ���Ƿ����
			auto res = MysqlConnManager::GetInstance()->Query(sql.str().c_str());
			if (res->GetCurrentNumRows() <= 0)
			{
				sql.str("");
				sql << "insert into `player_data`(`id`, `name`, `level`, `model`, `scene`, `x`, `y`, `sub_scene`) values( " << id << ", '" << name << "', 0, " << modelId << ", " << sceneId << ", " << x << ", " << y << ", " << subSceneId << ")";
				return MysqlConnManager::GetInstance()->Exec(sql.str().c_str());
			}
			return false;
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
		return false;
	}

	std::shared_ptr<MysqlResult> GetUserDataFromPlayerData(unsigned int id)
	{
		std::ostringstream sql;
		sql << "select `model`, `name`, `level`, `scene`, `x`, `y`, `sub_scene` from `player_data` where `id` = " << id;
		try
		{
			return MysqlConnManager::GetInstance()->Query(sql.str().c_str());
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
		return nullptr;
	}

	void UpdatePlayerPosition(unsigned int id, int x, int y, unsigned int sceneId, unsigned int subSceneId)
	{
		std::ostringstream sql;
		sql << "update `player_data` set `x` = " << x << ", `y` = " << y;
		if (sceneId != 0)
		{
			sql << ", `scene` = " << sceneId;
		}
		if (subSceneId != 0)
		{
			sql << ", `sub_scene` = " << subSceneId;
		}
		sql << " where `id` = " << id;
		try
		{
			MysqlConnManager::GetInstance()->Exec(sql.str().c_str());
		}
		catch (...)
		{
			error_log("%s", ("SQL Error: " + sql.str()).c_str());
		}
	}
}