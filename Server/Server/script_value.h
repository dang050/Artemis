#ifndef __SCRIPT_VALUE_H_
#define __SCRIPT_VALUE_H_

#include "script.h"
#include <windows.h>
#include <cstdio>
#include <type_traits>

class ScriptValue final
{
public:
	// 定义脚本值的数据类型
	enum ScriptValueType : unsigned char
	{
		vNil = LUA_TNIL,		// nil值
		vBool = LUA_TBOOLEAN,	// bool值
		vLightData = LUA_TLIGHTUSERDATA,	// 纯指针值
		vNumber = LUA_TNUMBER,	// number值
		vString = LUA_TSTRING,	// string值
		vCFunction = LUA_TFUNCTION,	// C函数指针值
		vUserData = LUA_TUSERDATA,	// userdata值
	};

	// 定义脚本值的数据结构
	union ScriptValueData
	{
		bool	bVal;		// bool值
		void*	pLightPtr;	// 指针值
		double	numVal;		// Number值
		struct  
		{
			char*	str;
			SIZE_T  len;
		}strVal;			// 字符串值
		lua_CFunction	cfnVal;		// C函数值
		struct {
			void*	ptr;
			SIZE_T	len;
		}userVal;			// 用户数据值
	};

public:
	ScriptValue();
	~ScriptValue();
	ScriptValue(const ScriptValue&);
	ScriptValue& operator= (const ScriptValue&);
	ScriptValue(ScriptValue&&);
	ScriptValue& operator= (ScriptValue&&);

	template
	<
		typename T,
		typename = typename std::enable_if<
			std::is_assignable
			<
				ScriptValue,
				typename std::decay<T>::type
			>::value
		>::type
	>
	ScriptValue(T&& val)
	{
		operator= (std::forward<T>(val));
	}

public:
	// 赋值运算
	ScriptValue& operator= (const int val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = val;
		return *this;
	}

	ScriptValue& operator= (const unsigned int val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = val;
		return *this;
	}

	ScriptValue& operator= (const INT64 val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = static_cast<double>(val);
		return *this;
	}

	ScriptValue& operator= (const UINT64 val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = static_cast<double>(val);
		return *this;
	}

	ScriptValue& operator= (const double val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = val;
		return *this;
	}

	ScriptValue& operator= (const float val)
	{
		clear();
		m_type = vNumber;
		m_data.numVal = val;
		return *this;
	}

	ScriptValue& operator= (const bool val)
	{
		clear();
		m_type = vBool;
		m_data.bVal = val;
		return *this;
	}

	ScriptValue& operator= (const char* val)
	{
		clear();
		m_type = vString;
		m_data.strVal.len = strlen(val);
		m_data.strVal.str = (char *)malloc(m_data.strVal.len + 1);
		memcpy(m_data.strVal.str, val, m_data.strVal.len);
		m_data.strVal.str[m_data.strVal.len] = 0;		// 加上结束符
		return *this;
	}

	ScriptValue& operator= (char* val)
	{
		return operator= (static_cast<const char*>(val));
	}

	ScriptValue& operator= (void* val)
	{
		clear();
		m_type = vLightData;
		m_data.pLightPtr = val;
		return *this;
	}

	ScriptValue& operator= (lua_CFunction val)
	{
		clear();
		m_type = vCFunction;
		m_data.cfnVal = val;
		return *this;
	}

	ScriptValue& SetUserData(void* data, SIZE_T len);
public:
	// 类型转换
	explicit operator int() const
	{
		if (m_type == vNumber)
			return static_cast<int>(m_data.numVal);
		else if (m_type == vString)
		{
			int result = 0;
			sscanf_s(m_data.strVal.str, "%d", &result);
			return result;
		}
		return 0;
	}

	explicit operator INT64() const
	{
		if (m_type == vNumber)
			return static_cast<INT64>(m_data.numVal);
		else if (m_type == vString)
		{
			INT64 result = 0;
			sscanf_s(m_data.strVal.str, "%i64d", &result);
			return result;
		}
		return 0;
	}

	explicit operator double() const
	{
		if (m_type == vNumber)
			return m_data.numVal;
		else if (m_type == vString)
		{
			double result = 0;
			sscanf_s(m_data.strVal.str, "%lf", &result);
			return result;
		}
		return 0;
	}

	explicit operator float() const
	{
		if (m_type == vNumber)
			return static_cast<float>(m_data.numVal);
		else if (m_type == vString)
		{
			float result = 0;
			sscanf_s(m_data.strVal.str, "%f", &result);
			return result;
		}
		return 0;
	}

	explicit operator bool() const
	{
		switch (m_type)
		{
		case ScriptValue::vBool:
			return m_data.bVal;
		case ScriptValue::vLightData:
			return m_data.pLightPtr != nullptr;
		case ScriptValue::vNumber:
			return m_data.numVal != 0;
		case ScriptValue::vString:
			return m_data.strVal.str && m_data.strVal.len > 0 && strcmp("true", m_data.strVal.str) == 0;
		case ScriptValue::vCFunction:
			return m_data.cfnVal != nullptr;
		default:
			return false;;
		}
	}

	explicit operator const char*() const
	{
		if (m_type == vString)
			return m_data.strVal.str;
		return nullptr;
	}

	explicit operator const lua_CFunction() const
	{
		if (m_type == vCFunction)
			return m_data.cfnVal;
		return nullptr;
	}

	explicit operator void*() const
	{
		if (m_type == vLightData)
			return m_data.pLightPtr;
		return nullptr;
	}

public:
	ScriptValueType GetType() const
	{
		return m_type;
	}
private:
	void clear();

private:
	ScriptValueType		m_type;
	ScriptValueData		m_data;
};
#endif