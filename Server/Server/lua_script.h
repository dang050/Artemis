#ifndef __LUA_SCRIPT_H_
#define __LUA_SCRIPT_H_

#include "script.h"
#include "lua_variant_wrapper.h"
#include "script_value_list.h"

class LuaScript final
{
public:
	LuaScript();
	~LuaScript();
	LuaScript(const LuaScript&) = delete;
	LuaScript& operator= (const LuaScript&) = delete;

public:
	void Init();
	void Load(const char* fileName);
	void Call(const char* funName, ScriptValueList& args, ScriptValueList& results, int nResultCount);
	bool FunctionExists(const char* funName);

public:
	static int HandlerScriptCallError(lua_State* L);
private:
	lua_State*	m_pLua;
};
#endif