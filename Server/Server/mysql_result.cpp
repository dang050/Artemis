#include "mysql_result.h"

MysqlResult::MysqlResult(MYSQL_RES* res)
:_res(res)
{

}

MysqlResult::~MysqlResult()
{
	if (_res != nullptr)
	{
		mysql_free_result(_res);
		_res = nullptr;
	}
}

MysqlResult::MysqlResult(MysqlResult&& obj)
{
	if (this != &obj)
	{
		_res = obj._res;
		obj._res = nullptr;
	}
}

MysqlResult& MysqlResult::operator= (MysqlResult&& obj)
{
	if (this != &obj)
	{
		if (_res != nullptr)
		{
			mysql_free_result(_res);
		}
		_res = obj._res;
		obj._res = nullptr;
	}
	return *this;
}
