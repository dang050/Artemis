//////////////////////////////////////////////////////////
//		                   .-' _..`.					//
//		                  /  .'_.'.'					//
//		                 | .' (.)`.						//
//		                 ;'   ,_   `.					//
//		 .--.__________.'    ;  `.;-'					//
//		|  ./               /							//
//		|  |               /							//
//		`..'`-._  _____, ..'							//
//		     / | |     | |\ \							//
//		    / /| |     | | \ \							//
//														//
//		~~~~~~~~~~						~~~~~~~~~~~		//
//		 放我出去							MDZZ		//
//////////////////////////////////////////////////////////
#include <iostream>
#include "server.h"
#include "error_handler.h"

int main()
{
	// 设置处理Unhandled Exception的回调函数  
	//   
	SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)iocp::ApplicationCrashHandler);

	std::cout << "===============================================================" << std::endl;
	std::cout << "|         *****        ***         ****       ****            |" << std::endl;
	std::cout << "|           *         *   *       *   *        *  *           |" << std::endl;
	std::cout << "|           *         *   *       *            *  *           |" << std::endl;
	std::cout << "|           *         *   *       *            ***            |" << std::endl;
	std::cout << "|           *         *   *       *            *              |" << std::endl;
	std::cout << "|           *         *   *       *            *              |" << std::endl;
	std::cout << "|           *         *   *       *   *        *              |" << std::endl;
	std::cout << "|         *****        ***         ***        ***             |" << std::endl;
	std::cout << "===============================================================" << std::endl;

	Server::GetInstance()->Start();
	std::string	cmd;

	while (std::getline(std::cin, cmd))
	{
		if (cmd == "dmp")
		{
			// 主动生成dump文件
			DebugBreak();
		}
		else if (cmd == "stop")
		{
			Server::GetInstance()->Stop();
			break;
		}
		else
		{
			continue;
		}
	}

	getchar();
	return 0;
}