#include "entity.h"

Entity::Entity(EntityType type, unsigned int id, unsigned int model, unsigned int sceneId, unsigned int subSceneId, int x, int y, std::string name)
:_type(type), _model(model), _sceneId(sceneId), _subSceneId(subSceneId), _x(x), _y(y), _id(id), _name(name)
{
}

Entity::~Entity()
{}

Entity::Entity(const Entity& obj)
{
	_type = obj._type;
	_x = obj._x;
	_y = obj._y;
	_id = obj._id;
	_name = obj._name;
	_model = obj._model;
	_sceneId = obj._sceneId;
	_subSceneId = obj._subSceneId;
}

Entity& Entity::operator=(const Entity& obj)
{
	if (this == &obj)
	{
		return *this;
	}
	_type = obj._type;
	_x = obj._x;
	_y = obj._y;
	_id = obj._id;
	_name = obj._name;
	_model = obj._model;
	_sceneId = obj._sceneId;
	_subSceneId = obj._subSceneId;
	return *this;
}

void Entity::SetPos(int x, int y, unsigned int sceneId, unsigned int subSceneId)
{
	_x = x;
	_y = y;
	if (sceneId != 0 || _sceneId != sceneId)
	{
		_sceneId = sceneId;
		_subSceneId = subSceneId;
	}
}

std::pair<int, int> Entity::GetPos() const
{
	return std::make_pair(_x, _y);
}

/*
* 注册定时器
* name ：定时器名字，实体内唯一
* nDelay ：延时执行，单位毫秒
* nInterval ：间隔时间，单位毫秒
* func ：定时器执行的函数
*/
void Entity::RegisterTimer(const std::string& name, unsigned long nDelay, unsigned long nInterval, unsigned int count, const std::function<void(void)>& func)
{
	m_hTimerComponentMgr.RegTimerComponent(name, nDelay, nInterval, count, func);
}

void Entity::UnRegisterTimer(const std::string& name)
{
	m_hTimerComponentMgr.UnRegTimerComponent(name);
}

void Entity::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
	m_hTimerComponentMgr.LogicRun(nCurrTick);
}