#ifndef __IOCP_H_
#define __IOCP_H_

#include <winsock2.h>
#include <MSWSock.h>
#include <vector>
#include <thread>
#include <string>
#include <condition_variable>
#include "socket_io_context.h"

#define WORKER_THREADS_PER_PROCESSOR 2
#define DEFAULT_IP	"127.0.0.1"
#define DEFAULT_PORT 12345
#define EXIT_CODE	NULL		// 传递给Worker线程的退出信号

class CIOCP final
{
public:
	CIOCP(std::function<void(std::shared_ptr<PER_SOCKET_CONTEXT>&)> connectCallback)
		:m_bStart(false),
		m_connectCallback(connectCallback)
	{
		LoadSocketLib();
		GetLocalIP();
	}
	~CIOCP()
	{
		Stop();
		WSACleanup();
	}
	CIOCP& operator= (const CIOCP&) = delete;
	CIOCP(const CIOCP&) = delete;

public:
	bool Start(u_short port = DEFAULT_PORT);
	void Stop();
private:
	bool InitializeIOCP();
	bool InitializeListenSocket(u_short);
	bool LoadSocketLib();
	void GetLocalIP();
	void DeInitialize();		// 释放所有资源

private:
	bool PostAccept(PPER_IO_CONTEXT);
	bool PostRecv(PPER_IO_CONTEXT);
	//	bool PostSend(PPER_IO_CONTEXT);
	bool AfterAccept(PPER_SOCKET_CONTEXT, PPER_IO_CONTEXT, DWORD);
	bool AfterRecv(PPER_SOCKET_CONTEXT, PPER_IO_CONTEXT, DWORD);
	bool AfterSend(PPER_SOCKET_CONTEXT, PPER_IO_CONTEXT, DWORD);
private:
	bool HandleIOCPError(PPER_SOCKET_CONTEXT, PPER_IO_CONTEXT, DWORD);	// 处理完成端口上的错误

public:
	inline bool IsSocketAlive(SOCKET sock)
	{
		int nBytes = send(sock, "", 0, 0);
		if (nBytes == -1)
			return false;
		else
			return true;
	}
	static int GetNumOfProcessor()
	{
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		return si.dwNumberOfProcessors;
	}
	// 线程函数
	void WorkerThread(int no);

private:
	std::string			m_strIP;	// 服务器端的IP地址
private:
	std::function<void(std::shared_ptr<PER_SOCKET_CONTEXT>&)>		m_connectCallback;		// 连接建立后的回调函数，需要在这个回调函数里设置好套接字的四个回调
	HANDLE									m_hShutdownEvent;			// 用来通知工作线程退出的事件，保证线程安全退出
	HANDLE									m_hIOCompletionPort;		// 完成端口的句柄
	std::vector<std::thread>				m_hWorkerThreads;			// 工作线程容器
	std::unique_ptr<PER_SOCKET_CONTEXT>		m_pListenContext;			// 监听Socket的Context信息

	LPFN_ACCEPTEX							m_lpfnAcceptEx;
	LPFN_GETACCEPTEXSOCKADDRS				m_lpfnGetAcceptExSockAddrs;
	std::mutex								m_hMutex;
	std::mutex								m_hOutputMutex;
	std::condition_variable					m_hCondVar;					// 用于控制线程同步
	BOOL									m_bReadyFlag;
	BOOL									m_bStart;					// 服务器运行标识
};
#endif