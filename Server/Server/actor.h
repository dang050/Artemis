#ifndef __ACTOR_H_
#define __ACTOR_H_
#include <queue>
#include <thread>
#include "network_msg_handler.h"
#include "object_pool.h"
#include <memory>
#include "login_system.h"
#include "scene_system.h"
#include "entity.h"
#include "ctimer.h"

class Actor : public Entity, public std::enable_shared_from_this<Actor>
{
public:
	Actor(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext);
	~Actor();
	Actor(const Actor&) = delete;
	Actor& operator=(const Actor&) = delete;

public:
	inline std::shared_ptr<NetWorkMsgHandler>& GetNetWorkMsgHandler()
	{
		return m_pNetWorkMsgHandler;
	}
	inline bool CanGC() const
	{
		return m_bGC;
	}

	std::shared_ptr<Actor>	GetSharedFromSelf()
	{
		return shared_from_this();
	}

	std::shared_ptr<Entity>	GetSharedBase() override
	{
		return shared_from_this();
	}

	inline bool IsLogin() const
	{
		return m_bIsLogin;
	}
	inline unsigned short GetLevel() const
	{
		return _level;
	}
public:
	void LogicRun(const std::chrono::system_clock::duration&) override;
	void Send(CDataPacket& pack);
	void Destroy();	// 销毁角色

private:
	void Logout();					// 玩家登出
	void Login(unsigned int);		// 玩家登陆
	bool CheckAndLoadUserData();
	bool LoadUserDataFromDB();		// 从数据库加载玩家信息

public:
	/* 四个回调函数 */
	void connectCallback();
	void disconnectCallback();
	void recvCallback(CDataPacket&);
	void sendCallback();

private:
	void ProcessNetworkMsg();
	void ProcessNetworkMsg(CDataPacket& pack);

private:
	std::shared_ptr<NetWorkMsgHandler>	m_pNetWorkMsgHandler;
	std::queue<std::shared_ptr<CDataPacket>>		m_packs;
	std::mutex										m_hMutex;

private:
	bool	m_bGC;						// 角色的状态，是否能被GC，true能|false不能
	static	std::mutex					m_hPoolMutex;
	static  ObjectPool<CDataPacket>		m_packPool;		// 这里要用静态的
	bool				m_bIsLogin;		// 是否登录

	CTimer<50>			m_t50ms;		// 50ms定时器

// 子系统
	friend class LoginSystem;
private:
	LoginSystem			m_subLoginSystem;
	SceneSystem			m_subSceneSystem;
// 玩家的属性
private:
	unsigned short		_level;		// 等级
};
#endif