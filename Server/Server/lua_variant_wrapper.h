#ifndef __LUA_VARIANT_WRAPPER_H_
#define __LUA_VARIANT_WRAPPER_H_

#include "script.h"
#include "lua_variant.h"

int WrapperGet(lua_State* L);
int WrapperSet(lua_State* L);

class LuaVariantWrapper final
{
	friend int WrapperGet(lua_State* L);
	friend int WrapperSet(lua_State* L);
public:
	LuaVariantWrapper();
	~LuaVariantWrapper();
	LuaVariantWrapper(LuaVariantWrapper&&);
	LuaVariantWrapper(const LuaVariantWrapper&) = delete;
	LuaVariantWrapper& operator= (LuaVariantWrapper&&);
	LuaVariantWrapper& operator= (class LuaVariantWrapper&) = delete;

public:
	static void Init(lua_State* L);
	static int Wrapper(lua_State* L, LuaVariant* val);
	void GetValue(lua_State* L, const char* name);

private:
	LuaVariant*	_data;
};
#endif