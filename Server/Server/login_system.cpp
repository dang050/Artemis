#include "login_system.h"
#include "SystemId.h"
#include "actor.h"
#include "logger.h"
#include <sstream>
#include "actor_manager.h"
#include "sql_helper.h"
#include "scene_manager.h"

const LoginSystem::InheritClass::OnHandleNetPack LoginSystem::InheritClass::m_hHandlers[] = 
{
	&LoginSystem::Error,
	&LoginSystem::Login,
	&LoginSystem::Register,
	&LoginSystem::GetUserData,
	&LoginSystem::CreateUser,
};

LoginSystem::LoginSystem(Actor* actor)
:SubSystem(actor)
{

}

void LoginSystem::ProcessNetwork(unsigned int system, unsigned int cmd, CDataPacket& pack)
{
	if (cmd < static_cast<unsigned int>(tagLoginSystemCmd::cMaxCodeId))
		(this->*m_hHandlers[cmd])(pack);
	else
	{
		pack << cmd;
		(this->*m_hHandlers[0])(pack);
	}
}

void LoginSystem::Error(CDataPacket& pack)
{
	unsigned int cmd = 0;
	pack >> cmd;
	error_log("LoginSystem 接收到了错误的命令：%d", cmd);
}

// 1成功，2失败
void LoginSystem::CreateUser(CDataPacket& pack)
{
	if (m_pEntity->IsLogin())
	{
		std::string name;
		unsigned int modelId;
		pack >> name;
		pack >> modelId;
		// 名字不能为空，和已有角色不能再创建
		if (!name.empty() && m_pEntity->_name.empty() && modelId > 0)
		{
			CDataPacket netPack;
			netPack << static_cast<unsigned int>(aeLoginSystem) << static_cast<unsigned int>(tagLoginSystemCmd::sCreateUser);
			unsigned int sceneId = 0;
			int x = 0;
			int y = 0;
			std::tie(sceneId, x, y) = SceneManager::GetInstance()->GetInitScenePos();
			if (SqlHelper::CreatePlayerData(m_pEntity->_id, name, modelId, x, y, sceneId))
			{
				netPack << (byte)1;
				// 创建成功后，把玩家角色数据加载进内存
				m_pEntity->LoadUserDataFromDB();
			}
			else
			{
				netPack << (byte)2;
			}
			m_pEntity->Send(netPack);
		}
	}
}

void LoginSystem::GetUserData(CDataPacket& pack)
{
	if (m_pEntity->IsLogin())
	{
		CDataPacket netPack;
		netPack << static_cast<unsigned int>(aeLoginSystem) << static_cast<unsigned int>(tagLoginSystemCmd::sGetUserData);
		if (m_pEntity->CheckAndLoadUserData())
		{
			netPack << (unsigned short)1;
			netPack << (unsigned int)m_pEntity->_id;
			netPack << m_pEntity->_name.c_str();
			netPack << m_pEntity->_level;
			netPack << m_pEntity->_model;
			netPack << m_pEntity->_sceneId;
			netPack << m_pEntity->_x;
			netPack << m_pEntity->_y;
		}
		else
		{
			netPack << (unsigned short)0;
		}
		m_pEntity->Send(netPack);
	}
	// 用户未登录不处理
}

void LoginSystem::Login(CDataPacket& pack)
{
	if (!m_pEntity->IsLogin())
	{
		std::string name;
		std::string passwd;
		pack >> name >> passwd;

		int actorId = SqlHelper::SelectIdFromUser(name, passwd);
		if (actorId < 0)
		{
			// 登录失败
			LoginFail(2);
		}
		else
		{
			// 这里需要判断是否帐号已登录
			if (ActorManager::GetInstance()->CheckOnline(actorId))
			{
				// 用户已登录
				LoginFail(3);
			}
			else
			{
				// 登录成功
				LoginSuccess(actorId);
			}
		}
	}
	// 重复登录不处理
}

void LoginSystem::Register(CDataPacket& pack)
{
	if (!m_pEntity->IsLogin())
	{
		std::string name;
		std::string passwd;
		pack >> name >> passwd;

		if (!SqlHelper::CheckIdIsExistInUser(name))
		{
			SqlHelper::InsertIntoUser(name, passwd);
			RegisterResult(1);		// 注册成功
		}
		else
		{
			// 注册失败，帐号已存在
			RegisterResult(2);
		}
	}
}

void LoginSystem::LoginSuccess(unsigned int actorId)
{
	m_pEntity->Login(actorId);

	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeLoginSystem) << static_cast<unsigned int>(tagLoginSystemCmd::sLogin);
	netPack << (byte)1;
	m_pEntity->Send(netPack);
}

// 1成功，2帐号密码错误，3用户已登录
void LoginSystem::LoginFail(unsigned int result)
{
	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeLoginSystem) << static_cast<unsigned int>(tagLoginSystemCmd::sLogin);
	netPack << (byte)2;		// 登录失败
	m_pEntity->Send(netPack);
}

// 1成功，2失败
void LoginSystem::RegisterResult(unsigned char result)
{
	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeLoginSystem) << static_cast<unsigned int>(tagLoginSystemCmd::sRegister);
	netPack << result;
	m_pEntity->Send(netPack);
}