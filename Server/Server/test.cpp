#include "test.h"
#include "lua_variant_wrapper.h"
#include <iostream>
#include "logger.h"
LuaVariant  var;

int test(lua_State* L)
{
	return LuaVariantWrapper::Wrapper(L, &var);
}
void* test2(lua_State* L)
{
	void * t = new int(2);
	info_log("%0xd", t);
	return t;
}

void test3(void* t)
{
	info_log("%0xd : %d", t, *(int*)t);
	delete t;
}