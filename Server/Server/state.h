/* ״̬���� */
#ifndef __STATE_H_
#define __STATE_H_

enum class FSMStateId :unsigned int
{
	MOVE = 1,
};

template<typename T>
class State
{
public:
	State(FSMStateId stateId, T* pEntity)
		:m_nStateId(stateId),
		m_pEntity(pEntity)
	{}
	virtual ~State(){}
	State(const State<T>&) = delete;
	State<T>& operator= (const State<T>&) = delete;

public:
	FSMStateId	GetStateId()
	{
		return m_nStateId;
	}

	virtual void handle() = 0;

protected:
	T*				m_pEntity;
	FSMStateId		m_nStateId;
};
#endif