#include "lua_script.h"
#include "lua_variant_wrapper.h"
#include <string>
#include <iostream>
#include <sstream>
#include "logger.h"

extern  int  luaopen_ServerInterface(lua_State* tolua_S);

LuaScript::LuaScript()
:m_pLua(nullptr)
{
}

LuaScript::~LuaScript()
{
	if (m_pLua)
		lua_close(m_pLua);
}

void LuaScript::Init()
{
	m_pLua = luaL_newstate();
	luaopen_base(m_pLua);
	luaopen_ServerInterface(m_pLua);
	LuaVariantWrapper::Init(m_pLua);
}

void LuaScript::Load(const char* fileName)
{
	if (!m_pLua)
		return;
	debug_log("加载脚本：%s", fileName);
	int ret = luaL_dofile(m_pLua, fileName);
	if (ret != 0)
	{
		std::string err = lua_tostring(m_pLua, -1);
		// 将错误信息出栈
		lua_pop(m_pLua, 1);
		// 输出错误信息
		error_log("load file: %s error: %s", fileName, err.c_str());
	}
}

bool LuaScript::FunctionExists(const char* sFnName)
{
	if (!m_pLua || !sFnName)
		return false;

	lua_getglobal(m_pLua, sFnName);
	bool result = lua_isfunction(m_pLua, -1) != 0;
	lua_pop(m_pLua, 1);
	return result;
}

int LuaScript::HandlerScriptCallError(lua_State* L)
{
	lua_Debug debug;
	std::stringstream msg;
	// 0是HandlerScriptCallError自己, 1是error函数, 2是真正出错的函数
	int level = 0;
	while (lua_getstack(L, level, &debug))
	{
		lua_getinfo(L, "Sln", &debug);

		msg << debug.short_src << ":line " << debug.currentline;
		if (debug.name != 0) {
			msg << "(" << debug.namewhat << " " << debug.name << ")" << std::endl;
		}
		level++;
	}
	lua_pushstring(L, msg.str().c_str());
	return 1;
}

void LuaScript::Call(const char* funName, ScriptValueList& args, ScriptValueList& results, int nResultCount)
{
	//////////////////////////////////////////////////////////////////////////
	/*todo 这里后续需要注意一个问题
	 *如果在脚本中分配了内存，要注意处理回收，因为脚本可能随时崩溃掉
	 */

	if (!m_pLua)
	{
		// 错误信息
		error_log("脚本未初始化");
		return;
	}

	// 获取当前栈顶
	int nTop = lua_gettop(m_pLua);

	// 压入错误处理函数
	lua_pushcfunction(m_pLua, HandlerScriptCallError);
	lua_getglobal(m_pLua, funName);
	if (!lua_isfunction(m_pLua, -1))
	{
		// 错误信息
		error_log("Lua Function %s does not exists", funName);
		return;
	}
	int nArgCount = static_cast<int>(args.Count());
	if (nArgCount > 0)
		args.PushArgs(m_pLua, nArgCount);
	// -nArgCount-2是错误处理函数在栈中的索引
	int nErr = lua_pcall(m_pLua, nArgCount, nResultCount, -nArgCount - 2);
	if (nErr != 0)
	{
		// 获取错误信息
		const char* err = lua_tostring(m_pLua, -1);
		// 输出错误信息
		error_log("调用函数：%s 失败，Error: %s", funName, err);
		// 从栈中弹出错误信息
		lua_pop(m_pLua, 1);
	}
	else
	{
		if (&args == &results)
			args.clear();
		// 如果期望的返回值是不限个，计算本次调用的返回值数量
		if (nResultCount == LUA_MULTRET)
			nResultCount = lua_gettop(m_pLua) - nTop;
		if (nResultCount > 0)
		{
			results.GetResults(m_pLua, nResultCount);
		}
	}

	// 恢复堆栈
	lua_settop(m_pLua, nTop);
}
