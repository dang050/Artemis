#ifndef __LOGIN_SYSTEM_H_
#define __LOGIN_SYSTEM_H_
#include "sub_system.h"
class Actor;

class LoginSystem :public SubSystem<LoginSystem, Actor>
{
public:
	LoginSystem(Actor* actor);
	~LoginSystem() {}
	LoginSystem(const LoginSystem&) = delete;
	LoginSystem& operator= (const LoginSystem&) = delete;

public:
	void ProcessNetwork(unsigned int system, unsigned int cmd, CDataPacket& pack);

	void Error(CDataPacket& pack);
	void Login(CDataPacket& pack);
	void Register(CDataPacket& pack);
	void GetUserData(CDataPacket& pack);
	void CreateUser(CDataPacket& pack);

private:
	void LoginSuccess(unsigned int);
	void LoginFail(unsigned int);
	void RegisterResult(unsigned char);
};
#endif