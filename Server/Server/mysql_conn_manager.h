#ifndef __MYSQL_CONN_MANAGER_H_
#define __MYSQL_CONN_MANAGER_H_

#include "singleton.h"
#include "mysql_connection.h"
#include <vector>
#include <memory>
#include <mutex>
#include <atomic>
#include <thread>
#include <map>

class MysqlConnManager final:public Singleton<MysqlConnManager>
{
public:
	MysqlConnManager();
	~MysqlConnManager(){};
	MysqlConnManager(const MysqlConnManager&) = delete;
	MysqlConnManager& operator= (const MysqlConnManager&) = delete;

public:
	void Start(size_t size = 10);
	void Stop();

public:
	bool Exec(const char* sql);
	bool Exec(const char* sql, unsigned long len);
	std::shared_ptr<MysqlResult> Query(const char* sql);
	std::shared_ptr<MysqlResult> Query(const char* sql, unsigned long len);

	template<typename Fn, typename... Args>
	inline auto Transaction(Fn&& func, Args&&... args) -> typename std::result_of<Fn(Args...)>::type
	{
		size_t pos = getPos();
		auto pair = _conn_pool[pos];
		std::thread::id id = std::this_thread::get_id();
		std::unique_lock<std::mutex>	lck(_transaction_mutex);
		_transaction_conns[id] = pos;
		lck.unlock();

		std::lock_guard<std::recursive_mutex>		lock(*(pair.first));		// 给conn上递归锁
		std::shared_ptr<MysqlConnection> conn(pair.second.get(), [this, id](MysqlConnection* p){
			std::lock_guard<std::mutex> trans_lock(_transaction_mutex);
			_transaction_conns.erase(id);
		});
		return conn->Transaction(std::forward<Fn>(func), std::forward<Args>(args)...);
	}

private:
	inline size_t getPos()
	{
		assert(_pool_size != 0);
		std::unique_lock<std::mutex>	lck(_transaction_mutex);
		if (_transaction_conns.find(std::this_thread::get_id()) != _transaction_conns.end())
		{
			return _transaction_conns[std::this_thread::get_id()];
		}
		lck.unlock();

		size_t pos = _counter.load();
		if (pos >= _pool_size)
		{
			pos = 0;
			_counter.store(pos);
		}
		else
		{
			_counter += 1;
		}

		return pos;
	}


private:
	size_t																	_pool_size;			// mysql 连接的数量
	std::vector<std::pair<std::shared_ptr<std::recursive_mutex>, std::shared_ptr<MysqlConnection>>>	_conn_pool;
	std::atomic<size_t>					_counter;

	std::mutex							_transaction_mutex;
	std::map<std::thread::id, size_t>	_transaction_conns;		// 用来存放事务用到的conn
};

#endif