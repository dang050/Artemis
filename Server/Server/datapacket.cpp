#include "datapacket.h"

CDataPacket::CDataPacket()
:
_buffer(nullptr),
_all_size(0),
_used_size(0),
_read(nullptr)
{
	_all_size = kDefaultSize;
	_buffer = malloc(_all_size);
	if (_buffer == nullptr)
	{
		std::cerr << "malloc failed" << std::endl;
		getchar();
		exit(0);
		return;
	}
	_read = _buffer;
}

CDataPacket::~CDataPacket()
{
	if (_buffer)
	{
		free(_buffer);
		_buffer = nullptr;
		_read = nullptr;
	}
	_all_size = 0;
	_used_size = 0;
}

CDataPacket::CDataPacket(const CDataPacket& obj)
{
	// 深拷贝
	_used_size = obj._used_size;
	_all_size = obj._all_size;
	_buffer = malloc(_all_size);
	if (_buffer == nullptr)
	{
		std::cerr << "malloc failed" << std::endl;
		getchar();
		exit(0);
		return;
	}
	_read = static_cast<void*>(static_cast<char*>(_buffer)+(static_cast<char*>(obj._read) - static_cast<char*>(obj._buffer)));
	memcpy(_buffer, obj._buffer, _all_size);
}

CDataPacket::CDataPacket(CDataPacket&& obj)
{
	// 浅拷贝
	_all_size = obj._all_size;
	_used_size = obj._used_size;
	_buffer = obj._buffer;
	_read = obj._read;
	obj._all_size = obj._used_size = 0;
	obj._buffer = nullptr;
	obj._read = nullptr;
}

CDataPacket& CDataPacket::operator= (const CDataPacket& obj)
{
	// 处理自赋值
	if (this != &obj)
	{
		if (_buffer)
		{
			free(_buffer);
		}
		// 深拷贝
		_all_size = obj._all_size;
		_used_size = obj._used_size;
		_buffer = malloc(_all_size);
		if (_buffer == nullptr)
		{
			std::cerr << "malloc failed" << std::endl;
			getchar();
			exit(0);
			return *this;
		}
		_read = static_cast<void*>(static_cast<char*>(_buffer)+(static_cast<char*>(obj._read) - static_cast<char*>(obj._buffer)));
		memcpy(_buffer, obj._buffer, _used_size);
	}
	return *this;
}

CDataPacket& CDataPacket::operator= (CDataPacket&& obj)
{
	if (this != &obj)
	{
		if (_buffer)
		{
			free(_buffer);
		}
		// 浅拷贝
		_all_size = obj._all_size;
		_used_size = obj._used_size;
		_buffer = obj._buffer;
		_read = obj._read;
		obj._all_size = obj._used_size = 0;
		obj._buffer = nullptr;
		obj._read = nullptr;
	}
	return *this;
}

void CDataPacket::chk_alloc_size(size_t size)
{
	size_t last_size = _all_size - _used_size;
	if (last_size < size)
	{
		realloc_size(size);
	}
}

void CDataPacket::realloc_size(size_t size)
{
	// 由于重分配的时候这里会清除里面的垃圾数据，也就是_buffer到_read之间的数据直接丢掉
	size_t num = (size / kIncreaseSize) + 1;
	size_t all_size = num * kIncreaseSize + _all_size;
	void* buff = malloc(all_size);
	if (buff == nullptr)
	{
		std::cerr << "malloc failed" << std::endl;
		getchar();
		exit(0);
		return;
	}
	_used_size = (static_cast<char*>(_buffer)+_used_size) - static_cast<char*>(_read);
	memcpy(buff, _read, _used_size);

	free(_buffer);
	_buffer = buff;
	_read = _buffer;
	_all_size = all_size;
}

bool CDataPacket::check_value_size(size_t size) const
{
	size_t used_size = (static_cast<char*>(_buffer)+_used_size) - static_cast<char*>(_read);
	if (used_size >= size)
	{
		return true;
	}
	else
	{
		return false;
	}
}

CDataPacket::operator size_t() const
{
	unsigned int size = sizeof(unsigned int);
	if (check_value_size(size))
	{
		return *static_cast<unsigned int*>(_read);
	}
	return 0;
}

size_t CDataPacket::size() const
{
	return (static_cast<char*>(_buffer)+_used_size) - static_cast<char*>(_read);
}

void CDataPacket::clear()
{
	_read = _buffer;
	_used_size = 0;
}

CDataPacket& CDataPacket::ReadBuffer(void* const buf, size_t size)
{
	if (check_value_size(size))
	{
		memcpy(buf, _read, size);
		_read = static_cast<void*>(static_cast<char*>(_read)+size);
	}
	return *this;
}

CDataPacket& CDataPacket::WriteBuffer(const void* const buf, size_t size)
{
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), buf, size);
	_used_size += size;

	return *this;
}

CDataPacket& CDataPacket::operator >> (std::string& val)
{
	unsigned short size = 0;
	operator >> (size);			// 获取字符串长度
	if (check_value_size(size))
	{
		val += std::string(static_cast<const char*>(_read), size);
		_read = static_cast<void*>(static_cast<char*>(_read) + size);
	}
	return *this;
}
