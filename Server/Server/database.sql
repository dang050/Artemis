CREATE DATABASE IF NOT EXISTS `db_name` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `user`(
	`id` bigint(4) NOT NULL AUTO_INCREMENT,
	`name` varchar(20) DEFAULT NULL,
	`passwd` varchar(20) DEFAULT NULL,
	PRIMARY KEY(`id`)
) ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `player_data`(
	`id` bigint(4) NOT NULL,
	`name` varchar(20) DEFAULT NULL,
	`level` int(10) DEFAULT NULL,
	PRIMARY KEY(`id`)
) ENGINE = InnoDB  DEFAULT CHARSET = utf8;

ALTER TABLE `player_data` ADD COLUMN `model` int;
ALTER TABLE `player_data` ADD COLUMN `scene` int;
ALTER TABLE `player_data` ADD COLUMN `x` int;
ALTER TABLE `player_data` ADD COLUMN `y` int;
ALTER TABLE `player_data` ADD COLUMN `sub_scene` int;

