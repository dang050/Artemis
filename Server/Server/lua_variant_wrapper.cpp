#include "lua_variant_wrapper.h"
#include <string>

void WrapperSetKey(lua_State* L, LuaVariant& var);

int WrapperGet(lua_State* L)
{
	int nType = lua_type(L, -1);
	luaL_checktype(L, -2, LUA_TUSERDATA);
	LuaVariantWrapper* wrap = static_cast<LuaVariantWrapper*>(lua_touserdata(L, -2));

	switch (nType)
	{
	case LUA_TNUMBER:
		if (!wrap->_data)
		{
			lua_pushnil(L);
		}
		else
		{
			int num = static_cast<int>(lua_tonumber(L, -1));
			wrap->GetValue(L, std::to_string(num).c_str());
		}
		break;
	case LUA_TSTRING:
		if (!wrap->_data)
		{
			lua_pushnil(L);
		}
		else
		{
			wrap->GetValue(L, lua_tostring(L, -1));
		}
		break;
	default:
		lua_pushnil(L);
		break;
	}
	// 返回值用于提示该C函数的返回值数量，即压入栈中的返回值数量
	return 1;
}

void WrapperSetValue(lua_State* L, LuaVariant& var)
{
	int nValueType = lua_type(L, -1);
	// valuetype 应该也只有这五种情况
	switch (nValueType)
	{
	case LUA_TBOOLEAN:
		var = lua_toboolean(L, -1);
		break;
	case LUA_TNUMBER:
		var = lua_tonumber(L, -1);
		break;
	case LUA_TSTRING:
		var = lua_tostring(L, -1);
		break;
	case LUA_TNIL:
		// 清空旧的值
		var.clear();
		break;
	case LUA_TTABLE:
//		var.SetType(LuaVariant::VARTYPE::Array);
		// 清空旧的值
		var.clear();
		lua_pushnil(L);
		while (lua_next(L, -2))
		{
			/* 处理相应数据。此时栈上 -1 处为 value, -2 处为 key */
			WrapperSetKey(L, var);
			//这时值在-1（栈顶）处，key在-2处。  
			lua_pop(L, 1);//把栈顶的值移出栈，让key成为栈顶以便继续遍历 
		}
		break;
	}
}

void WrapperSetKey(lua_State* L, LuaVariant& var)
{
	int nKeyType = lua_type(L, -2);
	// keytype 只有两种情况
	switch (nKeyType)
	{
		case LUA_TNUMBER:
		{
			int key = static_cast<int>(lua_tonumber(L, -2));
			LuaVariant& tempVar = var.Set(std::to_string(key).c_str());
			WrapperSetValue(L, tempVar);
			break;
		}
		case LUA_TSTRING:
		{
			LuaVariant& tempVar = var.Set(lua_tostring(L, -2));
			WrapperSetValue(L, tempVar);
			break;
		}
	default:
		break;
	}
}

int WrapperSet(lua_State* L)
{
	luaL_checktype(L, -3, LUA_TUSERDATA);
	LuaVariantWrapper* wrap = static_cast<LuaVariantWrapper*>(lua_touserdata(L, -3));
	WrapperSetKey(L, *wrap->_data);
	return 0;
}

LuaVariantWrapper::LuaVariantWrapper()
:_data(nullptr)
{
}
LuaVariantWrapper::~LuaVariantWrapper()
{
	_data = nullptr;
}

LuaVariantWrapper::LuaVariantWrapper(LuaVariantWrapper&& val)
{
	_data = val._data;
}

LuaVariantWrapper& LuaVariantWrapper::operator= (LuaVariantWrapper&& val)
{
	if (&val == this)
		return *this;
	_data = val._data;
	val._data = nullptr;
	return *this;
}

void LuaVariantWrapper::Init(lua_State* L)
{
	// 创建metatable
	luaL_newmetatable(L, "LuaVariantWrapperMetatable");
	lua_pushstring(L, "__index");

	lua_pushcfunction(L, WrapperGet);
	lua_rawset(L, -3);

	lua_pushstring(L, "__newindex");
	lua_pushcfunction(L, WrapperSet);
	lua_rawset(L, -3);
}

int LuaVariantWrapper::Wrapper(lua_State* L, LuaVariant* val)
{
	LuaVariantWrapper* wrapper = (LuaVariantWrapper*)lua_newuserdata(L, sizeof(LuaVariantWrapper));
	wrapper->_data = val;
	// 获取metatable
	luaL_getmetatable(L, "LuaVariantWrapperMetatable");
	// 设置metatable
	lua_setmetatable(L, -2);
	return 1;
}

void LuaVariantWrapper::GetValue(lua_State* L, const char* name)
{
	LuaVariant* value = _data->Get(name);
	if (value == nullptr)
	{
		lua_pushnil(L);
		return;
	}
	switch (value->Type())
	{
	case LuaVariant::VARTYPE::vNumber:
		lua_pushnumber(L, static_cast<double>(*value));
		break;
	case LuaVariant::VARTYPE::vNil:
		lua_pushnil(L);
		break;
	case LuaVariant::VARTYPE::vString:
		lua_pushstring(L, static_cast<const char*>(*value));
		break;
	case LuaVariant::VARTYPE::vArray:
		Wrapper(L, value);
		break;
	default:
		lua_pushnil(L);
		break;
	}
}