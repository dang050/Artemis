#ifndef __TIMER_COMPONENT_MANAGER_H_
#define __TIMER_COMPONENT_MANAGER_H_

#include <unordered_map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include "timer_component.h"
class TimerComponentManager final
{
public:
	TimerComponentManager();
	~TimerComponentManager();
	TimerComponentManager(const TimerComponentManager&) = delete;
	TimerComponentManager& operator= (const TimerComponentManager&) = delete;

public:
	void LogicRun(const std::chrono::system_clock::duration&);
	void RegTimerComponent(const std::string&, unsigned long, unsigned long, unsigned int, const std::function<void(void)>&);
	void UnRegTimerComponent(const std::string&);

private:
	std::mutex			m_hMutex;
	std::unordered_map<std::string, std::shared_ptr<TimerComponent>>						m_mapTimerComponents;
	std::vector<std::unordered_map<std::string, std::shared_ptr<TimerComponent>>::iterator>	m_arrayInvalidIter;	// 存放无效的迭代器，用于删除
};
#endif