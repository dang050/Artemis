/*
** Lua binding: ServerInterface
** Generated automatically by tolua++-1.0.92 on 09/22/16 18:05:17.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua++.h"

/* Exported function */
extern "C" __declspec(dllexport) int  tolua_ServerInterface_open (lua_State* tolua_S);

#include "test.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
}

/* function: test */
#ifndef TOLUA_DISABLE_tolua_ServerInterface_test00
static int tolua_ServerInterface_test00(lua_State* tolua_S)
{
	return test(tolua_S);
}
#endif //#ifndef TOLUA_DISABLE

/* function: test2 */
#ifndef TOLUA_DISABLE_tolua_ServerInterface_test200
static int tolua_ServerInterface_test200(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isnoobj(tolua_S,1,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  lua_State* tolua_var_2 =  tolua_S;
  {
   void* tolua_ret = (void*)  test2(tolua_var_2);
   tolua_pushuserdata(tolua_S,(void*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'test2'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* function: test3 */
#ifndef TOLUA_DISABLE_tolua_ServerInterface_test300
static int tolua_ServerInterface_test300(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isuserdata(tolua_S,1,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  void* tolua_var_3 = ((void*)  tolua_touserdata(tolua_S,1,0));
  {
   test3(tolua_var_3);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'test3'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
int tolua_ServerInterface_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
  tolua_function(tolua_S,"test",tolua_ServerInterface_test00);
  tolua_function(tolua_S,"test2",tolua_ServerInterface_test200);
  tolua_function(tolua_S,"test3",tolua_ServerInterface_test300);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 int luaopen_ServerInterface (lua_State* tolua_S) {
 return tolua_ServerInterface_open(tolua_S);
};
#endif

