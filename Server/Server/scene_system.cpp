#include "scene_system.h"
#include "SystemId.h"
#include "logger.h"
#include "actor.h"
#include "scene_manager.h"
#include "sql_helper.h"

const SceneSystem::InheritClass::OnHandleNetPack SceneSystem::InheritClass::m_hHandlers[] =
{
	&SceneSystem::Error,
	&SceneSystem::Move,
	&SceneSystem::EnterScene,
	&SceneSystem::LeaveScene,
	&SceneSystem::EnterSceneFinish,
};

SceneSystem::SceneSystem(Actor *actor)
:SubSystem(actor), m_readyScene(0), m_readySubScene(0)
{

}

void SceneSystem::ProcessNetwork(unsigned int system, unsigned int cmd, CDataPacket& pack)
{
	if (cmd < static_cast<unsigned int>(tagSceneSystemCmd::cMaxCodeId))
	{
		(this->*m_hHandlers[cmd])(pack);
	}
	else
	{
		pack << cmd;
		(this->*m_hHandlers[0])(pack);
	}
}

void SceneSystem::Move(CDataPacket& pack)
{
	int nCurrX = 0;
	int nCurrY = 0;
	int dir = 0;
	pack >> nCurrX >> nCurrY >> dir;
	if (m_hScene)
	{
		m_hMoveDeque.emplace_back(nCurrX, nCurrY, dir);
	}
}

void SceneSystem::EnterSceneFinish(CDataPacket& pack)
{
	if (m_readyScene != 0)
	{
		if (SceneManager::GetInstance()->CheckEnterScene(m_pEntity->GetSharedBase(), m_readyScene, m_readySubScene))
		{
			// 先将玩家从当前场景踢出去
			if (m_hScene != nullptr)
			{
				LeaveScene();
			}

			m_hScene = SceneManager::GetInstance()->EnterScene(m_pEntity->GetSharedBase(), m_readyScene, m_readySubScene);

			// 修改玩家的场景、坐标等相关信息，入库
			SqlHelper::UpdatePlayerPosition(m_pEntity->GetId(), m_pEntity->GetX(), m_pEntity->GetY(), m_pEntity->GetSceneId(), m_pEntity->GetSubSceneId());
		}
		m_readyScene = 0;
		m_readySubScene = 0;
	}
}

void SceneSystem::EnterScene(CDataPacket& pack)
{
	unsigned int scene = 0;
	pack >> scene;
	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sEnterScene);
	if (SceneManager::GetInstance()->CheckEnterScene(m_pEntity->GetSharedBase(), scene))
	{
		m_readyScene = scene;
		m_readySubScene = 0;
		// 成功
		netPack << static_cast<BYTE>(1);

		// 将移动消息队列清空
		m_hMoveDeque.clear();
	}
	else
	{
		// 失败
		netPack << static_cast<BYTE>(2);
	}

	m_pEntity->Send(netPack);
}

void SceneSystem::Error(CDataPacket& pack)
{
	unsigned int cmd = 0;
	pack >> cmd;
	error_log("MoveSystem 接收到了错误的命令: %d", cmd);
}

// todo 这个消息不会用到，后面可以去掉
void SceneSystem::LeaveScene(CDataPacket& pack)
{
	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sLeaveScene);
	if (LeaveScene())
	{
		// 发送离开场景成功
		netPack << (BYTE)1;
	}
	else
	{
		// 发送离开场景失败
		netPack << (BYTE)0;
	}
	m_pEntity->Send(netPack);
}


bool SceneSystem::LeaveScene()
{
	if (m_hScene == nullptr)
	{
		return false;
	}
	else
	{
		if (m_hScene->Leave(m_pEntity->GetSharedBase()))
		{
			m_hScene = nullptr;

			// 离开场景时，玩家的坐标信息不入库
	//		SqlHelper::UpdatePlayerPosition(m_pEntity->GetId(), m_pEntity->GetX(), m_pEntity->GetY(), m_pEntity->GetSceneId(), m_pEntity->GetSubSceneId());
			// 将移动消息队列清空
			m_hMoveDeque.clear();
			return true;
		}
		else
		{
			return false;
		}
	}
}

void SceneSystem::LogicRun()
{
	if (!m_hMoveDeque.empty())
	{
		int x = 0, y = 0, dir = 0;
		std::tie(x, y, dir) = m_hMoveDeque.back();
		if (m_hScene->Move(m_pEntity->GetSharedBase(), x, y, dir))
		{
			// 修改玩家的坐标，入库
			SqlHelper::UpdatePlayerPosition(m_pEntity->GetId(), m_pEntity->GetX(), m_pEntity->GetY());
		}
		// 只有最后一条移动消息生效
		m_hMoveDeque.clear();
	}
}
