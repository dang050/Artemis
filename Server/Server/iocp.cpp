#include "iocp.h"
#include <iostream>
#pragma comment(lib, "ws2_32.lib")
#include "logger.h"

bool CIOCP::InitializeIOCP()
{
	m_hIOCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (m_hIOCompletionPort == NULL)
	{
		error_log("建立完成端口失败");
		return false;
	}

	// 本机处理器数量
	int nThreads = WORKER_THREADS_PER_PROCESSOR * GetNumOfProcessor();
	m_hWorkerThreads.reserve(nThreads);

	std::unique_lock<std::mutex> lck(m_hOutputMutex);
	for (int i = 0; i < nThreads; ++i)
	{
		m_hWorkerThreads.push_back(std::thread(&CIOCP::WorkerThread, this, i + 1));

		m_hCondVar.wait(lck, [this]{return this->m_bReadyFlag; });
		m_bReadyFlag = false;
	}

	debug_log("建立 WorkerThread %d 个.", m_hWorkerThreads.size());
	return true;
}

bool CIOCP::InitializeListenSocket(u_short port)
{
	struct sockaddr_in ServerAddress;
	// 生成监听的Socket信息
	m_pListenContext = std::make_unique<PER_SOCKET_CONTEXT>();
	m_pListenContext->m_Socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (INVALID_SOCKET == m_pListenContext->m_Socket)
	{
		error_log("初始化监听Socket失败，错误代码：%d", WSAGetLastError());
		return false;
	}
	else
	{
		debug_log("WSASocket 调用完成.");
	}

	// 将监听Socket绑定至完成端口中
	if (CreateIoCompletionPort(reinterpret_cast<HANDLE>(m_pListenContext->m_Socket), m_hIOCompletionPort, reinterpret_cast<ULONG_PTR>(m_pListenContext.get()), 0) == NULL)
	{
		error_log("绑定 Listen Socket 至完成端口失败！错误代码：%d", GetLastError());
		RELEASE_SOCKET(m_pListenContext->m_Socket);
		return false;
	}
	else
	{
		debug_log("Listen Socket 绑定完成端口成功.");
	}

	// 填充地址信息
	ZeroMemory((char*)&ServerAddress, sizeof(ServerAddress));
	ServerAddress.sin_family = AF_INET;
	ServerAddress.sin_addr.s_addr = inet_addr(m_strIP.c_str());
	ServerAddress.sin_port = htons(port);

	// 绑定地址和端口
	if (SOCKET_ERROR == bind(m_pListenContext->m_Socket, (struct sockaddr *)&ServerAddress, sizeof(ServerAddress)))
	{
		error_log("bind()函数执行错误！错误代码：%d", WSAGetLastError());
		return false;
	}
	else
	{
		debug_log("bind 调用完成.");
	}

	// 开始进行监听
	if (listen(m_pListenContext->m_Socket, SOMAXCONN) == SOCKET_ERROR)
	{
		error_log("Listen()函数执行错误！错误代码：%d", WSAGetLastError());
		return false;
	}
	else
	{
		debug_log("Listen 调用完成.");
	}

	// AcceptEx 和 GetAcceptExSockaddrs 的GUID，用于导出函数地址
	GUID GuidAcceptEx = WSAID_ACCEPTEX;
	GUID GuidGetAcceptExSockAddrs = WSAID_GETACCEPTEXSOCKADDRS;

	// 获取AcceptEx函数指针
	DWORD dwBytes = 0;
	if (SOCKET_ERROR == WSAIoctl(
		m_pListenContext->m_Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidAcceptEx,
		sizeof(GuidAcceptEx),
		&m_lpfnAcceptEx,
		sizeof(m_lpfnAcceptEx),
		&dwBytes,
		NULL,
		NULL))
	{
		error_log("WSAIoctl 获取AcceptEx函数指针失败！错误代码：%d", WSAGetLastError());
		return false;
	}

	// 获取GetAcceptExSockAddr函数指针
	if (SOCKET_ERROR == WSAIoctl(
		m_pListenContext->m_Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidGetAcceptExSockAddrs,
		sizeof(GuidGetAcceptExSockAddrs),
		&m_lpfnGetAcceptExSockAddrs,
		sizeof(m_lpfnGetAcceptExSockAddrs),
		&dwBytes,
		NULL,
		NULL))
	{
		error_log("WSAIoctl 获取GetAcceptExSockAddr函数指针失败！错误代码：%d", WSAGetLastError());
		return false;
	}

	for (size_t i = 0; i < m_hWorkerThreads.size(); ++i)
	{
		// 新建一个IO_CONTEXT
		std::shared_ptr<PER_IO_CONTEXT> pAcceptIoContext = m_pListenContext->GetNewIoContext();
		if (PostAccept(pAcceptIoContext.get()) == false)
		{
			m_pListenContext->RemoveContext(pAcceptIoContext);
			return false;
		}
	}

	debug_log("投递 %d 个AcceptEx请求完毕.", m_hWorkerThreads.size());
	return true;
}

// 投递Accept
bool CIOCP::PostAccept(PPER_IO_CONTEXT pAcceptIoContext)
{
	assert(m_pListenContext->m_Socket != INVALID_SOCKET);

	DWORD dwBytes = 0;
	pAcceptIoContext->m_opType = ACCEPT_POSTED;
	WSABUF *p_wsaBuf = &pAcceptIoContext->m_wsaBuf;
	OVERLAPPED *p_ol = &pAcceptIoContext->m_Overlapped;
	pAcceptIoContext->ResetBuffer();

	// 为以后新连入的客户端准备好Socket
	pAcceptIoContext->m_sockAccept = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (pAcceptIoContext->m_sockAccept == INVALID_SOCKET)
	{
		error_log("创建用于Accept的Socket失败！错误代码：%d", WSAGetLastError());
		return false;
	}

	// 投递AcceptEx
	if (m_lpfnAcceptEx(m_pListenContext->m_Socket, pAcceptIoContext->m_sockAccept, p_wsaBuf->buf, p_wsaBuf->len - ((sizeof(SOCKADDR_IN)+16) * 2),
		sizeof(SOCKADDR_IN)+16, sizeof(SOCKADDR_IN)+16, &dwBytes, p_ol) == false)
	{
		if (WSA_IO_PENDING != WSAGetLastError())
		{
			error_log("投递 AcceptEx 请求失败！错误代码：%d", WSAGetLastError());
			return false;
		}
	}
	return true;
}

// Accept完成
bool CIOCP::AfterAccept(PPER_SOCKET_CONTEXT pSocketContext, PPER_IO_CONTEXT pIoContext, DWORD dwBytesTransfered)
{
	SOCKADDR_IN* pClientAddr = nullptr;
	SOCKADDR_IN* pLocalAddr = nullptr;
	int remoteLen = sizeof(SOCKADDR_IN), localLen = sizeof(SOCKADDR_IN);

	///////////////////////////////////////////////////////////////
	// 1、取得连入客户端的地址信息，以及客户端发送过来的第一组数据
	m_lpfnGetAcceptExSockAddrs(pIoContext->m_wsaBuf.buf, pIoContext->m_wsaBuf.len - ((sizeof(SOCKADDR_IN)+16) * 2),
		sizeof(SOCKADDR_IN)+16, sizeof(SOCKADDR_IN)+16, (LPSOCKADDR*)&pLocalAddr, &localLen, (LPSOCKADDR*)&pClientAddr, &remoteLen);

	///////////////////////////////////////////////////////////////
	// 2、新建一个SocketContext
	std::shared_ptr<PER_SOCKET_CONTEXT> pNewSocketContext = std::make_shared<PER_SOCKET_CONTEXT>();
	pNewSocketContext->m_Socket = pIoContext->m_sockAccept;
	memcpy(&(pNewSocketContext->m_ClientAddr), pClientAddr, sizeof(SOCKADDR_IN));

	// 参数设置完毕，将这个Socket和完成端口绑定
	if (CreateIoCompletionPort(reinterpret_cast<HANDLE>(pNewSocketContext->m_Socket), m_hIOCompletionPort, reinterpret_cast<ULONG_PTR>(pNewSocketContext.get()), 0) == NULL)
	{
		error_log("执行CreateIoCompletionPort()出现错误！错误代码：%d", GetLastError());
		pNewSocketContext.reset();
		return false;
	}

	//////////////////////////////////////////////////////////////
	// 3、建立其下的IoContext，用于投递IO请求
	std::shared_ptr<PER_IO_CONTEXT> pNewIoContext = pNewSocketContext->GetNewIoContext();
	pNewIoContext->m_sockAccept = pNewSocketContext->m_Socket;
	pIoContext->m_wsaBuf.len = dwBytesTransfered;		// 设置实际接收到的字节数
	/////////////////////////////////////////////////////////////
	// 4、连接完成回调
	m_connectCallback(pNewSocketContext);
	// 上线回调函数
	pNewSocketContext->m_connectCallback();

	// 调用recvCallback函数
	pNewSocketContext->m_recvCallback(&(pIoContext->m_wsaBuf));

	////////////////////////////////////////////////////////////
	// 5、投递WSARecv请求
	if (PostRecv(pNewIoContext.get()) == false)
	{
		// 投递失败
		// 调用客户端下线回调函数
		pNewSocketContext->m_disconnectCallback();
		pNewSocketContext.reset();
	}

	/////////////////////////////////////////////////////////////
	// 6、使用完毕之后，把Listen Socket的那个IoContext重新投递AcceptEx请求
	return PostAccept(pIoContext);
}

bool CIOCP::PostRecv(PPER_IO_CONTEXT pIoContext)
{
	DWORD dwFlags = 0;
	DWORD dwBytes = 0;
	WSABUF *pBuf = &pIoContext->m_wsaBuf;
	OVERLAPPED *pOL = &pIoContext->m_Overlapped;

	pIoContext->ResetBuffer();
	pIoContext->m_opType = RECV_POSTED;

	// 投递WSARecv请求
	int nBytesRecv = WSARecv(pIoContext->m_sockAccept, pBuf, 1, &dwBytes, &dwFlags, pOL, NULL);
	// 如果返回值错误，并且错误代码并非是Pending的话，说明这个重叠请求失败了
	if ((SOCKET_ERROR == nBytesRecv) && (WSA_IO_PENDING != WSAGetLastError()))
	{
		error_log("投递WSARecv失败！错误代码：%d", WSAGetLastError());
		return false;
	}
	return true;
}

/*
bool CIOCP::PostSend(PPER_IO_CONTEXT pIoContext)
{
DWORD dwBytesSend = 0;
pIoContext->m_opType = SEND_POSTED;

int nBytesSend = WSASend(pIoContext->m_sockAccept, &pIoContext->m_wsaBuf, 1, &dwBytesSend, 0, &pIoContext->m_Overlapped, NULL);
if ((SOCKET_ERROR == nBytesSend) && (WSA_IO_PENDING != WSAGetLastError()))
{
error_log("投递WSASend失败！错误代码：%d", WSAGetLastError());
return false;
}
return true;
}
*/

bool CIOCP::AfterRecv(PPER_SOCKET_CONTEXT pSocketContext, PPER_IO_CONTEXT pIoContext, DWORD dwBytesTransfered)
{
	// 设置实际接收到的字节数
	pIoContext->m_wsaBuf.len = dwBytesTransfered;
	// 调用recv回调函数
	pSocketContext->m_recvCallback(&(pIoContext->m_wsaBuf));

	// 投递下一下WSARecv请求
	return PostRecv(pIoContext);
}

bool CIOCP::AfterSend(PPER_SOCKET_CONTEXT pSocketContext, PPER_IO_CONTEXT pIoContext, DWORD dwBytesTransfered)
{
	pSocketContext->m_sendCallback(pIoContext);

	return true;
}
bool CIOCP::LoadSocketLib()
{
	WSADATA wsaData;
	int nResult;
	nResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (NO_ERROR != nResult)
	{
		error_log("初始化WinSock 2.2失败");
		return false;
	}
	return true;
}

void CIOCP::GetLocalIP()
{
#ifdef _DEBUG
	m_strIP = DEFAULT_IP;
	return;
#endif
	char hostname[MAX_PATH] = { 0 };
	gethostname(hostname, MAX_PATH);
	struct hostent FAR* lpHostEnt = gethostbyname(hostname);
	if (lpHostEnt == nullptr)
	{
		m_strIP = DEFAULT_IP;
		return;
	}

	// 取得IP地址列表中的第一个为返回的IP
	LPSTR lpAddr = lpHostEnt->h_addr_list[0];

	// 将IP地址转化成字符串形式
	struct in_addr inAddr;
	memmove(&inAddr, lpAddr, 4);
	m_strIP = inet_ntoa(inAddr);
}

// 释放所有资源
void CIOCP::DeInitialize()
{
	// 等待工作线程退出
	for (auto& t : m_hWorkerThreads)
	{
		t.join();
	}

	// 释放退出事件
	RELEASE_HANDLE(m_hShutdownEvent);

	// 释放完成端口
	RELEASE_HANDLE(m_hIOCompletionPort);

	// 关闭监听套接字
	m_pListenContext.reset();
}

// 工作者线程
void CIOCP::WorkerThread(int threadNO)
{
	{
		debug_log("工作者线程 %d 号启动.", threadNO);
		m_bReadyFlag = true;
		m_hCondVar.notify_one();
	}

	DWORD					dwBytesTransfered = 0;
	PPER_SOCKET_CONTEXT		pSocketContext = nullptr;
	OVERLAPPED*				pOverlapped = nullptr;

	while (WaitForSingleObject(m_hShutdownEvent, 0) != WAIT_OBJECT_0)
	{
		dwBytesTransfered = 0;
		BOOL bReturn = GetQueuedCompletionStatus(
			m_hIOCompletionPort,
			&dwBytesTransfered,
			(PULONG_PTR)&pSocketContext,
			&pOverlapped,
			INFINITE);

		// 如果收到的是退出标志，则直接退出
		if (reinterpret_cast<ULONG_PTR>(pSocketContext) == EXIT_CODE)
		{
			break;
		}

		// 读取传入的参数
		PPER_IO_CONTEXT pIoContext = CONTAINING_RECORD(pOverlapped, PER_IO_CONTEXT, m_Overlapped);

		// 判断是否出现了错误
		if (!bReturn)
		{
			DWORD dwErr = GetLastError();
			if (HandleIOCPError(pSocketContext, pIoContext, dwErr) == false)
			{
				break;
			}
		}
		else
		{
			// 判断是否有客户端断开了(客户端收到0字节表示套接字关闭连接)
			if ((dwBytesTransfered == 0) && (pIoContext->m_opType == RECV_POSTED || pIoContext->m_opType == SEND_POSTED))
			{
				if (pSocketContext == m_pListenContext.get())
				{
					PostAccept(pIoContext);
				}
				else
				{
					// 客户端下线
					pSocketContext->m_disconnectCallback();
				}
				continue;
			}
			else
			{
				switch (pIoContext->m_opType)
				{
				case ACCEPT_POSTED:
				{
					AfterAccept(pSocketContext, pIoContext, dwBytesTransfered);
				}
					break;
				case RECV_POSTED:
				{
					AfterRecv(pSocketContext, pIoContext, dwBytesTransfered);
				}
					break;
				case SEND_POSTED:
				{
					AfterSend(pSocketContext, pIoContext, dwBytesTransfered);
				}
					break;
				default:
					error_log("WorkThread 中的 pIoContext->m_opType 参数异常！");
					break;
				}
			}
		}
	}

	{
		std::lock_guard<std::mutex> lck(m_hOutputMutex);
		debug_log("工作者线程 %d 号退出.", threadNO);
	}

	return;
}

/*
* 要注意，如果pSocketContext是m_pListenContext，则要继续投递Accept请求到完成端口上
*/
bool CIOCP::HandleIOCPError(PPER_SOCKET_CONTEXT pSocketContext, PPER_IO_CONTEXT pIoContext, DWORD dwErr)
{
	// 超时
	if (WAIT_TIMEOUT == dwErr)
	{
		// 确认客户端是否还活着
		if (!IsSocketAlive(pSocketContext->m_Socket))
		{
			debug_log("检测到客户端异常退出1！");
			if (pSocketContext == m_pListenContext.get())
			{
				PostAccept(pIoContext);
			}
			else
			{
				pSocketContext->m_disconnectCallback();
			}
			return true;
		}
		else
		{
			debug_log("网络操作超时！(忽略)");	// 这里会不会有问题？todo
			if (pSocketContext == m_pListenContext.get())
			{
				PostAccept(pIoContext);
			}
			else
			{
				pSocketContext->m_disconnectCallback();
			}
			return true;
		}
	}
	else if (ERROR_NETNAME_DELETED == dwErr)	// 客户端异常退出
	{
		debug_log("检测到客户端异常退出2！");
		if (pSocketContext == m_pListenContext.get())
		{
			PostAccept(pIoContext);
		}
		else
		{
			pSocketContext->m_disconnectCallback();
		}
		return true;
	}
	else
	{
		error_log("完成端口操作出现错误，线程退出！错误代码：%d", dwErr);
		return false;
	}
}

void CIOCP::Stop()
{
	if (m_pListenContext && m_pListenContext->m_Socket != INVALID_SOCKET && m_bStart)
	{
		// 激活关闭事件
		SetEvent(m_hShutdownEvent);

		// 通知所有工作线程退出
		for (std::vector<std::thread>::size_type i = 0; i < m_hWorkerThreads.size(); ++i)
		{
			PostQueuedCompletionStatus(m_hIOCompletionPort, 0, (ULONG_PTR)EXIT_CODE, NULL);
		}

		// 释放资源
		DeInitialize();
		m_bStart = false;

		debug_log("停止监听.");
	}
}

bool CIOCP::Start(u_short port)
{
	if (!m_bStart)
	{

		m_hShutdownEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

		if (InitializeIOCP() == false)
		{
			error_log("初始化IOCP失败.");
			return false;
		}
		else
		{
			debug_log("IOCP初始化完成.");
		}

		// 初始化套接字，开始监听
		if (InitializeListenSocket(port) == false)
		{
			// 激活关闭事件
			SetEvent(m_hShutdownEvent);

			// 通知所有工作线程退出
			for (std::vector<std::thread>::size_type i = 0; i < m_hWorkerThreads.size(); ++i)
			{
				PostQueuedCompletionStatus(m_hIOCompletionPort, 0, (ULONG_PTR)EXIT_CODE, NULL);
			}

			// 释放资源
			DeInitialize();

			error_log("Listen Socket初始化失败.");
			return false;
		}
		else
		{
			//		debug_log("Listen Socket初始化完毕.");
		}
		debug_log("系统准备就绪，等待连接...");
		m_bStart = true;
		return true;
	}
	else
	{
		error_log("IOCP正在运行中");
		return false;
	}
}