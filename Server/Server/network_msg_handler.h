#ifndef __NETWORK_MSG_HANDLER_H_
#define __NETWORK_MSG_HANDLER_H_
#include "socket_io_context.h"
#include "datapacket.h"
#include <queue>
#include <atomic>

using ConnectCallback = std::function<void()>;
using DisconnectCallback = std::function<void()>;
using RecvCallback = std::function<void(CDataPacket&)>;
using SendCallback = std::function<void()>;

class NetWorkMsgHandler final
{
public:
	NetWorkMsgHandler(std::shared_ptr<PER_SOCKET_CONTEXT>&, ConnectCallback const &, DisconnectCallback const &, RecvCallback const &, SendCallback const &);
	~NetWorkMsgHandler();
	NetWorkMsgHandler(const NetWorkMsgHandler&) = delete;
	NetWorkMsgHandler& operator= (const NetWorkMsgHandler&) = delete;

public:
	/* 四个回调函数 */
	void connectCallback();
	void disconnectCallback();
	void recvCallback(WSABUF*);
	void sendCallback(PPER_IO_CONTEXT);

public:
	template<typename T, typename = typename std::enable_if<
		std::is_same<typename std::decay<T>::type, CDataPacket>::value>::type>
		inline void Send(T&&);
	void LogicRun(const std::chrono::system_clock::duration& nCurrTick);
	void Destroy();		// 关闭套接字

private:
	void RealSend();		// 实际的WSASend调用

private:
	std::shared_ptr<PER_SOCKET_CONTEXT> m_pContext;

	std::function<void()>	m_connectCallback;
	std::function<void()>	m_disconnectCallback;
	std::function<void(CDataPacket&)>	m_recvCallback;
	std::function<void()>	m_sendCallback;

private:
	std::mutex				m_hMutex;
	CDataPacket				m_recvPack;

	bool					m_bFirst;		// 用于标识是否是第一个包(第一个包需要添加整个包的长度)
	std::mutex				m_hQueueMutex;
	std::queue<CDataPacket>	m_sendPackQueue;
	std::atomic<bool>		m_bAtom;		// true 表示正在发送状态，false 表示未发送状态，用来保证WSASend同时只在一个线程中调用
};


template<typename T, typename>
inline void NetWorkMsgHandler::Send(T&& pack)
{
	std::lock_guard<std::mutex>		lck(m_hQueueMutex);
	// 添加进队列 这里只添加进消息队列，并不进行实际的WSASend调用
	m_sendPackQueue.push(std::forward<T>(pack));
}
#endif