#pragma once
#include <memory>
#include <stdexcept>
#include <list>

// 线程不安全
template<typename T>
class ObjectPool final
{
	static const uint32_t kDefaultSize = 0x00000080;  //128
	static const uint32_t kIncreaseSize = 0x00000400; //1024
public:
	explicit ObjectPool(uint32_t chunk_size = kDefaultSize, uint32_t increase_size = kIncreaseSize)
		:_chunk_size(chunk_size),
		_increase_size(increase_size)
	{
		if (_chunk_size < 0 || _increase_size < 0)
		{
			throw std::invalid_argument("invalid argument");
		}

		allocate(_chunk_size);
	}
	~ObjectPool();
	ObjectPool(const ObjectPool<T>&) = delete;
	ObjectPool<T>& operator= (const ObjectPool<T>&) = delete;

public:
	template<typename...Args, typename = typename std::enable_if<std::is_constructible<T, Args...>::value>::type>
	T* acquire(Args&&... args)
	{
		T* obj = get_free();
		alloc.construct(obj, std::forward<Args>(args)...);		// 调用构造函数
		return obj;
	}
	void release(T* obj)
	{
		deallocate(obj);
	}
private:
	T* get_free();
	void allocate(size_t size);
	void deallocate(T *obj);

private:
	std::list<T*>	_free_list;
	uint32_t		_chunk_size;
	uint32_t		_increase_size;
	static std::allocator<T> alloc;
};
template<typename T>
std::allocator<T> ObjectPool<T>::alloc;

template<typename T>
T* ObjectPool<T>::get_free()
{
	if (_free_list.empty())
	{
		allocate(_increase_size);
	}
	T* result = _free_list.back();
	_free_list.pop_back();

	return result;
}

template<typename T>
void ObjectPool<T>::allocate(size_t size)
{
	for (size_t i = 0; i != size; ++i)
	{
		T *current_obj = alloc.allocate(1);	// 分配内存
		_free_list.push_back(current_obj);
	}
}

template<typename T>
void ObjectPool<T>::deallocate(T *obj)
{
	alloc.destroy(obj);		// 调用析构函数
	_free_list.push_back(obj);
}

template<typename T>
ObjectPool<T>::~ObjectPool()
{
	for (auto& i : _free_list)
	{
		alloc.deallocate(i, 1);
	}
}
