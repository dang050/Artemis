#ifndef __SCENE_AOI_H_
#define __SCENE_AOI_H_
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <vector>
#include "entity.h"

class SceneAOI final
{
public:
	SceneAOI(unsigned int x = 30, unsigned int y = 30);
	~SceneAOI();
	SceneAOI(const SceneAOI&) = delete;
	SceneAOI& operator=(const SceneAOI&) = delete;

public:
	void SetAOIRegion(const unsigned int x, const unsigned int y)
	{
		m_nAOIX = x;
		m_nAOIY = y;
	}

public:
	// ����һ��Ψһkey
	static std::string MakeKey(const std::shared_ptr<Entity>& obj)
	{
		unsigned int type = static_cast<unsigned int>(obj->GetType());
		unsigned int id = obj->GetId();
		return std::to_string(type) + "_" + std::to_string(id);
	}

public:
	void GetAOINodes(const std::list<std::shared_ptr<Entity>>::iterator& iter, std::vector<std::shared_ptr<Entity>>& nodes);
	void GetAOINodes(const std::list<std::shared_ptr<Entity>>::iterator& iter, std::unordered_set<std::shared_ptr<Entity>>& nodes);

public:
	void Enter(const std::shared_ptr<Entity>& obj, std::vector<std::shared_ptr<Entity>>& nodes);
	void Move(std::shared_ptr<Entity>& obj, const std::pair<int, int>& newPos, std::vector<std::shared_ptr<Entity>>& leaveNodes, std::vector<std::shared_ptr<Entity>>& updateNodes, std::vector<std::shared_ptr<Entity>>& enterNodes);
	void Leave(const std::shared_ptr<Entity>& obj, std::vector<std::shared_ptr<Entity>>& nodes);

private:
	std::list<std::shared_ptr<Entity>>::iterator Insert(const std::shared_ptr<Entity>& obj);
	template
	<
		typename T, 
		typename = typename std::enable_if
		<
			std::is_same<T, std::vector<std::shared_ptr<Entity>>>::value ||
			std::is_same<T, std::unordered_set<std::shared_ptr<Entity>>>::value
		>::type
	>
	inline void Erase(const std::string& key, T& nodes)
	{
		auto it = m_nIndexs.find(key);
		if (it != m_nIndexs.end())
		{
			auto nodeIter = it->second;

			GetAOINodes(nodeIter, nodes);
			m_pNodes.erase(nodeIter);
			m_nIndexs.erase(it);
		}
	}

private:
	unsigned int m_nAOIX;		// AOI����x�᷶Χ
	unsigned int m_nAOIY;		// AOI����y�᷶Χ

private:
	std::list<std::shared_ptr<Entity>>	m_pNodes;	// list���������ɾ���������ᵼ�µ�����ʧЧ
	std::unordered_map<std::string, std::list<std::shared_ptr<Entity>>::iterator>	m_nIndexs;		// ����
};
#endif