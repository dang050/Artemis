#ifndef __TIMER_COMPONENT_H_
#define __TIMER_COMPONENT_H_

#include <chrono>
#include <functional>
class TimerComponentManager;
class TimerComponent final
{
	friend class TimerComponentManager;
	using millsecond_type = std::chrono::duration<unsigned long, std::ratio<1, 1000>>;
public:
	TimerComponent(const millsecond_type&, const millsecond_type&, unsigned int, const std::function<void(void)>&);
	~TimerComponent();
	TimerComponent(const TimerComponent&);
	TimerComponent& operator= (const TimerComponent&);

public:
	void Run(std::chrono::system_clock::duration);

	inline bool IsValid() const
	{
		return m_nCount > 0 ? true : false;
	}

private:
	std::chrono::system_clock::duration	m_nextTick;		// 下次执行时间
	millsecond_type						m_nInterval;	// 间隔时间
	unsigned int						m_nCount;		// 执行次数
	std::function<void(void)>			m_pFunc;		// 处理函数
};

#endif