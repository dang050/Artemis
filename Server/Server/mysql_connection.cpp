#include "mysql_connection.h"
#include <iostream>
#include "parse_json.h"

MysqlConnection::MysqlConnection()
:_conn(nullptr)
{
}

MysqlConnection::~MysqlConnection()
{
	if (_conn != nullptr)
	{
		mysql_close(_conn);
		_conn = nullptr;
	}
}

MysqlConnection::MysqlConnection(MysqlConnection&& obj)
{
	if (this != &obj)
	{
		_conn = obj._conn;
		obj._conn = nullptr;
	}
}

MysqlConnection& MysqlConnection::operator=(MysqlConnection&& obj)
{
	if (this != &obj)
	{
		if (_conn != nullptr)
		{
			mysql_close(_conn);
		}
		_conn = obj._conn;
		obj._conn = nullptr;
	}
	return *this;
}

bool MysqlConnection::Connect(const char* db, const char* user, const char* passwd, const char* host, unsigned int port, const char* unix_socket, unsigned long clientflag)
{
	if (_conn != nullptr)
	{
		mysql_close(_conn);
		_conn = nullptr;
	}
	_conn = mysql_init(nullptr);
	if (!(mysql_real_connect(_conn, host, user, passwd, db, port, unix_socket, clientflag)))
	{
		mysql_close(_conn);
		_conn = nullptr;
		return false;
	}
	else
	{
		// 设置编码格式
		return Exec("set names gbk");
	}
}

bool MysqlConnection::Connect()
{
	std::ifstream ifs;
	ifs.open("mysql_config.json", std::ios::binary);
	if (!ifs)
	{
		return false;
	}

	Json::Reader	reader;
	Json::Value		root;
	if (reader.parse(ifs, root))
	{
		std::string username = root["username"].asString();
		std::string passwd = root["passwd"].asString();
		std::string hostname = root["host"].asString();
		unsigned int port = root["port"].asUInt();
		std::string dbname = root["database"].asString();
		unsigned int maxConn = root["max_conn"].asUInt();
		ifs.close();
		if (Connect(dbname.c_str(), username.c_str(), passwd.c_str(), hostname.c_str(), port))
		{
			// 设置mysql的最大连接数
			return Exec(("set GLOBAL max_connections=" + std::to_string(maxConn)).c_str());
		}
		else
		{
			return false;
		}
	}
	else
	{
		ifs.close();
		return false;
	}
}

bool MysqlConnection::Exec(const char* sql)
{
	assert(_conn != nullptr);
	// 执行成功返回0，失败返回非0
	if (mysql_query(_conn, sql))
	{
		throw mysql_error(_conn);
		return false;
	}
	else
	{
		return true;
	}
}

bool MysqlConnection::Exec(const char* sql, unsigned long len)
{
	assert(_conn != nullptr);
	if (mysql_real_query(_conn, sql, len))
	{
		throw mysql_error(_conn);
		return false;
	}
	else
	{
		return true;
	}
}

std::shared_ptr<MysqlResult> MysqlConnection::Query(const char* sql)
{
	assert(_conn != nullptr);
	if (mysql_query(_conn, sql))
	{
		throw mysql_error(_conn);
		return NULL;
	}
	else
	{
		auto result = std::make_shared<MysqlResult>(mysql_store_result(_conn));
		return result;
	}
}

std::shared_ptr<MysqlResult> MysqlConnection::Query(const char* sql, unsigned long len)
{
	assert(_conn != nullptr);
	if (mysql_real_query(_conn, sql, len))
	{
		throw mysql_error(_conn);
		return NULL;
	}
	else
	{
		auto result = std::make_shared<MysqlResult>(mysql_store_result(_conn));
		return result;
	}
}

/****************************************************
 *使用例子
 *
char * db = "CREATE DATABASE IF NOT EXISTS `db_name` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;";
char * create = "CREATE TABLE IF NOT EXISTS `test`(`id` bigint(4) NOT NULL AUTO_INCREMENT,`name` varchar(20) DEFAULT NULL,`passwd` int(10) DEFAULT NULL,PRIMARY KEY(`id`)) ENGINE = InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET = latin1; ";

MysqlConnection conn;
conn.Connect("test", "root", "123456");
conn.Exec("drop table IF EXISTS `test`;");
conn.Exec(create);
conn.Exec("insert into `test` values(10, 'aq', 123456)");
auto res = MysqlConnManager::GetInstance()->Query("select * from test where id = 11");
if (res->GetCurrentNumRows() > 0)
{
	MYSQL_ROW row;
	unsigned fields_num = res.GetCurrentNumFields();
	while ((row = res.GetCurrentRow()))
	{
		for (unsigned int j = 0; j < fields_num; j++)
		{
			std::cout << row[j] << " ";
		}
		std::cout << std::endl;
	}
}
******************************************************/
