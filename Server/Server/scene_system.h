#ifndef __SCENE_SYSTEM_H_
#define __SCENE_SYSTEM_H_
#include "sub_system.h"
#include "scene.h"
#include <deque>

class Actor;

class SceneSystem :public SubSystem<SceneSystem, Actor>
{
public:
	SceneSystem(Actor* actor);
	~SceneSystem(){}
	SceneSystem(const SceneSystem&) = delete;
	SceneSystem& operator= (const SceneSystem&) = delete;

public:
	void ProcessNetwork(unsigned int system, unsigned int cmd, CDataPacket& pack);

	void Error(CDataPacket& pack);
	void Move(CDataPacket& pack);
	void EnterScene(CDataPacket& pack);
	void LeaveScene(CDataPacket& pack);
	void EnterSceneFinish(CDataPacket& pack);

public:
	bool LeaveScene();
	void LogicRun();
private:
	std::shared_ptr<Scene>		m_hScene;
	unsigned int				m_readyScene;
	unsigned int				m_readySubScene;
	std::deque<std::tuple<int, int, int>>		m_hMoveDeque;	// �ƶ���Ϣջ
};
#endif