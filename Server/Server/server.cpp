#include "server.h"
#include "actor_manager.h"
#include "logger.h"
#include "tool.h"
#include "scene_manager.h"
#include "mysql_conn_manager.h"

Server::Server(std::chrono::milliseconds const& interval)
:m_iocp(std::bind(&Server::connected, this, std::placeholders::_1)),
m_bState(false),
m_logicLoopInterval(interval)
{
}

Server::~Server()
{
	if (m_bState)
	{
		Stop();
	}
}

bool Server::Start()
{
	debug_log("服务器启动中...");

	// 加载脚本
	m_hScript.Init();
	m_hScript.Load("test.lua");

	ScriptValueList	param;
	m_hScript.Call("func", param, param, 0);

	// 启动数据库管理
	MysqlConnManager::GetInstance()->Start();
	// 启动场景
	SceneManager::GetInstance()->Start();
	// 启动用户管理器
	ActorManager::GetInstance()->Start();
	// 启动逻辑处理线程
	m_bState = true;
	m_pThread = std::make_unique<std::thread>(&Server::LogicRun, this);

	// 启动iocp服务
	return m_iocp.Start();
}

void Server::Stop()
{
	m_bState = false;
	// 关闭逻辑处理线程
	m_pThread->join();
	m_pThread.reset();

	// 需要先关闭iocp，再进行其它的处理
	// 关闭iocp服务
	m_iocp.Stop();

	// 关闭用户管理器
	ActorManager::GetInstance()->Stop();
	// 关闭场景
	SceneManager::GetInstance()->Stop();
	// 关闭数据库连接池
	MysqlConnManager::GetInstance()->Stop();
	debug_log("服务器关闭.");
}

// 需要在这个回调函数里设置好套接字的四个回调
void Server::connected(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext)
{
	ActorManager::GetInstance()->AddActor(pSocketContext);
}

// 服务器逻辑
void Server::LogicRun()
{
	while (m_bState)
	{
		std::chrono::system_clock::duration	nCurrTick = iocp::GetCurrTickCount();
		// 全局定时器逻辑处理
		m_hTimerComponentMgr.LogicRun(nCurrTick);

		// 角色管理逻辑处理
		ActorManager::GetInstance()->LogicRun(nCurrTick);


		std::this_thread::sleep_for(m_logicLoopInterval);
	}
}

/*
* 注册定时器
* name ：定时器名字，实体内唯一
* nDelay ：延时执行，单位毫秒
* nInterval ：间隔时间，单位毫秒
* func ：定时器执行的函数
*/
void Server::RegisterTimer(const std::string& name, unsigned long nDelay, unsigned long nInterval, unsigned int count, const std::function<void(void)>& func)
{
	m_hTimerComponentMgr.RegTimerComponent(name, nDelay, nInterval, count, func);
}

void Server::UnRegisterTimer(const std::string& name)
{
	m_hTimerComponentMgr.UnRegTimerComponent(name);
}