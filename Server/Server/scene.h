#ifndef __SCENE_H_
#define __SCENE_H_
#include "scene_aoi.h"
#include <mutex>

class Scene final
{
public:
	Scene(unsigned int sceneId, unsigned int subId);
	~Scene();
	Scene(const Scene&) = delete;
	Scene& operator= (const Scene&) = delete;

private:
	void LoadSceneDataFromConfig();

public:
	bool Move(std::shared_ptr<Entity>& obj, int x, int y, int dir);
	void Enter(std::shared_ptr<Entity>& obj, int x = 0, int y = 0);
	bool Leave(std::shared_ptr<Entity>& obj);

private:
	void PlayerMove(std::shared_ptr<Entity>& obj, const std::pair<int, int>& pos);
	void PlayerEnter(std::shared_ptr<Entity>& obj, const std::vector<std::shared_ptr<Entity>>& nodes);
	void PlayerLeave(std::shared_ptr<Entity>& obj, const std::vector<std::shared_ptr<Entity>>& nodes, bool broadcast = false);
public:
	void LogicRun(const std::chrono::system_clock::duration&);
private:
	unsigned int	m_nSceneId;		//场景id
	unsigned int	m_nSubId;		//子id
	int				m_nDefaultX;	//默认x坐标
	int				m_nDefaultY;	//默认y坐标
	SceneAOI		m_hAOI;			//场景AOI
	std::mutex		m_hAOIMutex;	//AOI mutex
};
#endif