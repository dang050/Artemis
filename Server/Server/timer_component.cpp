#include "timer_component.h"
#include "tool.h"
TimerComponent::TimerComponent(const millsecond_type& nDelay, const millsecond_type& nInterval, unsigned int nCount, const std::function<void(void)>& func)
:m_nInterval(nInterval), m_nCount(nCount), m_pFunc(func)
{
	m_nextTick = iocp::GetCurrTickCount() + nDelay;
}

TimerComponent::~TimerComponent()
{}

TimerComponent::TimerComponent(const TimerComponent& obj)
:m_nextTick(obj.m_nextTick),
m_nInterval(obj.m_nInterval),
m_nCount(obj.m_nCount),
m_pFunc(obj.m_pFunc)
{
}

TimerComponent& TimerComponent::operator= (const TimerComponent& obj)
{
	if (this == &obj)
	{
		return *this;
	}
	m_nextTick = obj.m_nextTick;
	m_nInterval = obj.m_nInterval;
	m_nCount = obj.m_nCount;
	m_pFunc = obj.m_pFunc;
	return *this;
}

void  TimerComponent::Run(std::chrono::system_clock::duration nCurrTick)
{
	if (nCurrTick >= m_nextTick)
	{
		m_pFunc();
		m_nextTick += m_nInterval;
		--m_nCount;
	}
}