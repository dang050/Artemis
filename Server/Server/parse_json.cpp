#include "parse_json.h"

#ifdef _WIN64
#ifdef _DEBUG
#if _MSC_VER >= 1900	// vs2015及以上版本
#pragma comment(lib, "Jsoncpp\\x64\\Debug\\lib_json_2015.lib")
#else
#pragma comment(lib, "Jsoncpp\\x64\\Debug\\lib_json_2013.lib")
#endif
#else
#if _MSC_VER >= 1900	// vs2015及以上版本
#pragma comment(lib, "Jsoncpp\\x64\\Release\\lib_json_2015.lib")
#else
#pragma comment(lib, "Jsoncpp\\x64\\Release\\lib_json_2013.lib")
#endif
#endif
#else
#ifdef _DEBUG
#if _MSC_VER >= 1900	// vs2015及以上版本
#pragma comment(lib, "Jsoncpp\\Debug\\lib_json_2015.lib")
#else
#pragma comment(lib, "Jsoncpp\\Debug\\lib_json_2013.lib")
#endif
#else
#if _MSC_VER >= 1900	// vs2015及以上版本
#pragma comment(lib, "Jsoncpp\\Release\\lib_json_2015.lib")
#else
#pragma comment(lib, "Jsoncpp\\Release\\lib_json_2013.lib")
#endif
#endif
#endif

void test_json()
{
	std::ifstream ifs;
	ifs.open("test.json", std::ios::binary);
	if (!ifs)
	{
		std::cout << "打开失败" << std::endl;
	}

	Json::Reader reader;
	Json::Value	root;

	if (reader.parse(ifs, root))
	{
		//读取根节点信息
		std::string name = root["name"].asString();
		int age = root["age"].asInt();
		bool sex_is_male = root["sex_is_male"].asBool();

		std::cout << "My name is " << name << std::endl;
		std::cout << "I'm " << age << " years old" << std::endl;
		std::cout << "I'm a " << (sex_is_male ? "man" : "woman") << std::endl;

		//读取子节点信息
		std::string partner_name = root["partner"]["partner_name"].asString();
		int partner_age = root["partner"]["partner_age"].asInt();
		bool partner_sex_is_male = root["partner"]["partner_sex_is_male"].asBool();

		std::cout << "My partner's name is " << partner_name << std::endl;
		std::cout << (partner_sex_is_male ? "he" : "she") << " is "
			<< partner_age << " years old" << std::endl;

		//读取数组信息
		std::cout << "Here's my achievements:" << std::endl;
		for (unsigned int i = 0; i < root["achievement"].size(); i++)
		{
			std::string ach = root["achievement"][i].asString();
			std::cout << ach << '\t';
		}
		std::cout << std::endl;

		// 读取AQ
		std::cout << "Here's mine test:" << std::endl;
		for (unsigned int i = 0; i < root["AQ"].size(); i++)
		{
			Json::Value node = root["AQ"][i];
			int id = node["id"].asInt();
			std::string value = node["value"].asString();
			std::cout << id << ":" << value << std::endl;
		}
		std::cout << std::endl;

		std::cout << "Reading Complete!" << std::endl;
	}

	ifs.close();
}