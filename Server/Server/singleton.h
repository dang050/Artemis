#ifndef __SINGLETON_H_
#define __SINGLETON_H_

template<typename T>
class Singleton
{
protected:
	Singleton()
	{
		Garbo;			// 为了防止编译器把Garbo变量优化掉了
	}
	virtual ~Singleton()
	{
	}
	Singleton(const Singleton<T>&) = delete;
	Singleton<T>& operator= (const Singleton<T>&) = delete;

private:
	class CGarbo
	{
	public:
		CGarbo(){
		}
		~CGarbo()
		{
			if (Singleton<T>::m_pInstance)
				delete Singleton<T>::m_pInstance;
		}
	};
	static T *m_pInstance;
	static CGarbo Garbo;		//定义一个静态成员变量，程序结束时，系统会自动调用它的析构函数

public:
	template<typename... Args>
	static T* GetInstance(Args&&... args)
	{
		if (m_pInstance == nullptr)
			m_pInstance = new T(std::forward<Args>(args)...);
		return m_pInstance;
	}

	static CGarbo get()
	{
		return Garbo;
	}
};
template<typename T>
T* Singleton<T>::m_pInstance = nullptr;

template<typename T>
typename Singleton<T>::CGarbo Singleton<T>::Garbo;


#endif