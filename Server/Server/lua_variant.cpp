#include "lua_variant.h"
#include <string>

LuaVariant::LuaVariant()
{
	_type = vNil;
	_key = 0;
	_data.s.len = 0;
	_data.n = 0;
	_data.a.len = 0;
}

LuaVariant::~LuaVariant()
{
	clear();
}

// 深拷贝
LuaVariant::LuaVariant(const LuaVariant& val)
{
	_key = val._key;
	if (val._type == vNumber)
	{
		operator=(val._data.n);
	}
	else if (val._type == vString)
	{
		// 深拷贝
		operator= (val._data.s.str);
	}
	else if (val._type == vArray)
	{
		clear();
		_type = vArray;
		_data.a.list = (LuaVariant**)malloc(val._data.a.len);
		for (unsigned int i = 0; i < val._data.a.len; ++i)
		{
			_data.a.list[i] = new LuaVariant(*val._data.a.list[i]);
		}
		_data.a.len = val._data.a.len;
	}
}

// 浅拷贝
LuaVariant::LuaVariant(LuaVariant&& val)
{
	_key = val._key;
	_type = val._type;
	_data = val._data;

	val._type = vNil;
	val._data.n = 0;
	val._data.s.len = 0;
	val._data.s.str = nullptr;
	val._data.a.len = 0;
	val._data.a.list = nullptr;
}

LuaVariant& LuaVariant::operator=(const LuaVariant& val)
{
	if (&val == this)
		return *this;
	_key = val._key;
	if (val._type == vNumber)
	{
		operator=(val._data.n);
	}
	else if (val._type == vString)
	{
		// 深拷贝
		operator= (val._data.s.str);
	}
	else if (val._type == vArray)
	{
		clear();
		_type = vArray;
		_data.a.list = (LuaVariant**)malloc(val._data.a.len);
		for (unsigned int i = 0; i < val._data.a.len; ++i)
		{
			_data.a.list[i] = new LuaVariant(*val._data.a.list[i]);
		}
		_data.a.len = val._data.a.len;
	}
	return *this;
}

LuaVariant& LuaVariant::operator=(LuaVariant&& val)
{
	if (&val == this)
		return *this;
	clear();

	_key = val._key;
	_type = val._type;
	_data = val._data;

	val._type = vNil;
	val._data.n = 0;
	val._data.s.len = 0;
	val._data.s.str = nullptr;
	val._data.a.len = 0;
	val._data.a.list = nullptr;

	return *this;
}

LuaVariant& LuaVariant::operator= (double val)
{
	if (_type != vNumber && _type != vNil)
		clear();
	_type = vNumber;
	_data.n = val;
	return *this;
}

LuaVariant& LuaVariant::operator= (const char* val)
{
	if (_type == vString && _data.s.str && !strcmp(_data.s.str, val))
		return *this;
	clear();
	_type = vString;
	_data.s.len = static_cast<unsigned int>(strlen(val));
	_data.s.str = (char*)malloc(_data.s.len + 1);
	memcpy(_data.s.str, val, _data.s.len + 1);
	return *this;
}

// 这里不可以将key重置，key只能由它的上级来指定
void LuaVariant::clear()
{
	if (_type = vString)
	{
		free(_data.s.str);
		_data.s.str = nullptr;
		_data.s.len = 0;
	}
	else if (_type = vArray)
	{
		for (unsigned int i = 0; i < _data.a.len; ++i)
		{
			delete _data.a.list[i];
		}
		free(_data.a.list);
		_data.a.list = nullptr;
		_data.a.len = 0;
	}
	else if (_type = vNumber)
	{
		_data.n = 0;
	}
	_type = vNil;
}

size_t LuaVariant::hash(const char* str) const
{
	std::hash<std::string> str_hash;
	return str_hash(std::string(str));
}

LuaVariant* LuaVariant::Get(const char* sName)
{
	if (_type != vArray || !sName)
		return nullptr;
	size_t key = hash(sName);
	return Get(key);
}

LuaVariant* LuaVariant::Get(size_t key)
{
	if (_type != vArray)
		return nullptr;
	LuaVariant* pVar;
	for (unsigned int index = 0; index < _data.a.len; ++index)
	{
		pVar = _data.a.list[index];
		if (pVar->_key == key)
		{
			return pVar;
		}
	}
	return nullptr;
}

// 创建一个Array类型的值，如果成员已经存在则不处理，否则创建一个新的LuaVariant
LuaVariant& LuaVariant::Set(const char* sName)
{
	if (_type != vArray)
		clear();
	_type = vArray;
	size_t key = hash(sName);
	LuaVariant* pVar = Get(key);
	if (!pVar)
	{
		pVar = new LuaVariant();
		pVar->_key = key;

		_data.a.list = (LuaVariant**)realloc(_data.a.list, sizeof(*_data.a.list) * (_data.a.len + 1));
		_data.a.list[_data.a.len] = pVar;
		_data.a.len++;
	}
	return *pVar;
}

const LuaVariant& LuaVariant::Set(const char* name, double val)
{
	LuaVariant& var = Set(name);
	var.operator= (val);
	return var;
}

const LuaVariant& LuaVariant::Set(const char* name, const char* val)
{
	LuaVariant& var = Set(name);
	var.operator= (val);
	return var;
}

size_t LuaVariant::GetStoreSize() const
{
	size_t size = 1;		// 类型用1字节存储
	switch (_type)
	{
	case vNil:
		break;
	case vNumber:
		size += sizeof(_key)+sizeof(_data.n);
		break;
	case vString:
		size += sizeof(_key)+sizeof(_data.s.len) + ((_data.s.len > 0) ? _data.s.len + 1 : 0);	// 加上1是字符串结束符
		break;
	case vArray:
		size += sizeof(_key)+sizeof(_data.a.len);
		if (_data.a.len > 0)
		{
			for (unsigned int index = 0; index != _data.a.len; ++index)
			{
				size += _data.a.list[index]->GetStoreSize();
			}
		}
		break;
	}
	return size;
}

size_t LuaVariant::Pack(char *dst, size_t size) const
{
	size_t reqSize = GetStoreSize();
	if (size < reqSize)
		return 0;

	size_t saveSize = 0;

	// 写入类型
	*(unsigned char*)dst = static_cast<unsigned char>(_type);
	dst += sizeof(unsigned char);
	size -= sizeof(unsigned char);
	saveSize += sizeof(unsigned char);

	switch (_type)
	{
	case vNil:
		break;
	case vNumber:
	{
				   // 写入索引
				   *(size_t*)dst = _key;
				   dst += sizeof(size_t);
				   size -= sizeof(size_t);
				   saveSize += sizeof(size_t);

				   // 写入数据
				   *(double*)dst = _data.n;
				   dst += sizeof(double);
				   size -= sizeof(double);
				   saveSize += sizeof(double);
				   break;
	}
	case vString:
	{
				   // 写入索引
				   *(size_t*)dst = _key;
				   dst += sizeof(size_t);
				   size -= sizeof(size_t);
				   saveSize += sizeof(size_t);

				   // 写入字符串长度
				   *(unsigned int*)dst = _data.s.len;
				   dst += sizeof(unsigned int);
				   size -= sizeof(unsigned int);
				   saveSize += sizeof(unsigned int);

				   // 写入字符串内容
				   if (_data.s.len > 0)
				   {
					   // 这里加1是算上结束符
					   memcpy(dst, _data.s.str, _data.s.len + 1);
					   dst += (_data.s.len + 1);
					   size -= (_data.s.len + 1);
					   saveSize += (_data.s.len + 1);
				   }
				   break;
	}
	case vArray:
	{
				  // 写入索引
				  *(size_t*)dst = _key;
				  dst += sizeof(size_t);
				  size -= sizeof(size_t);
				  saveSize += sizeof(size_t);

				  // 写入成员数量
				  *(unsigned int*)dst = _data.a.len;
				  dst += sizeof(unsigned int);
				  size -= sizeof(unsigned int);
				  saveSize += sizeof(unsigned int);

				  // 循环写入成员内容
				  if (_data.a.len > 0)
				  {
					  for (unsigned int index = 0; index != _data.a.len; ++index)
					  {
						  size_t usedSize = _data.a.list[index]->Pack(dst, size);
						  dst += usedSize;
						  size -= usedSize;
						  saveSize += usedSize;
					  }
				  }
				  break;
	}
	}
	return saveSize;
}

size_t LuaVariant::Load(const char* src, size_t size)
{
	if (size < 1)
		return 0;
	clear();

	size_t usedSize = 0;

	// 读取类型
	_type = static_cast<VARTYPE>(*(unsigned char*)src);
	src += sizeof(unsigned char);
	size -= sizeof(unsigned char);
	usedSize += sizeof(unsigned char);

	switch (_type)
	{
	case vNil:
		_key = 0;
		break;
	case vNumber:
	{
				   // 读取索引
				   _key = *(size_t *)src;
				   src += sizeof(size_t);
				   size -= sizeof(size_t);
				   usedSize += sizeof(size_t);

				   // 读取数据
				   _data.n = *(double*)src;
				   src += sizeof(double);
				   size -= sizeof(double);
				   usedSize += sizeof(double);
				   break;
	}
	case vString:
	{
				   // 读取索引
				   _key = *(size_t *)src;
				   src += sizeof(size_t);
				   size -= sizeof(size_t);
				   usedSize += sizeof(size_t);

				   // 读取长度
				   unsigned int len = *(unsigned int*)src;
				   src += sizeof(unsigned int);
				   size -= sizeof(unsigned int);
				   usedSize += sizeof(unsigned int);

				   if (len > 0)
				   {
					   operator=(src);
					   src += len + 1;
					   size -= len + 1;
					   usedSize += len + 1;
				   }
				   break;
	}
	case vArray:
	{
				  // 读取索引
				  _key = *(size_t *)src;
				  src += sizeof(size_t);
				  size -= sizeof(size_t);
				  usedSize += sizeof(size_t);

				  // 读取长度
				  unsigned int len = *(unsigned int*)src;
				  src += sizeof(unsigned int);
				  size -= sizeof(unsigned int);
				  usedSize += sizeof(unsigned int);

				  if (len > 0)
				  {
					  _data.a.list = (LuaVariant**)malloc(len * sizeof(LuaVariant*));
					  _data.a.len = len;
					  for (unsigned int index = 0; index != len; ++index)
					  {
						  _data.a.list[index] = new LuaVariant();
						  size_t n = _data.a.list[index]->Load(src, size);
						  src += n;
						  size -= n;
						  usedSize += n;
					  }
				  }
				  break;
	}
	}
	return usedSize;
}