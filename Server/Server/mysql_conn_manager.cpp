#include "mysql_conn_manager.h"
#include "logger.h"

MysqlConnManager::MysqlConnManager()
:_pool_size(0), _counter(0)
{
}

void MysqlConnManager::Start(size_t size)
{
	if (_pool_size == 0)
	{
		_pool_size = size;
		_conn_pool.reserve(_pool_size);
		for (size_t index = 0; index != size; ++index)
		{
			_conn_pool.push_back(std::make_pair(
				std::make_shared<std::recursive_mutex>(),
				std::make_shared<MysqlConnection>()
				)
				);
		}

		for (auto& it : _conn_pool)
		{
			if (!it.second->Connect())
			{
				error_log("数据库连接失败");
			}
		}
	}
	debug_log("数据库连接池启动.");
}

void MysqlConnManager::Stop()
{
	if (_pool_size != 0)
	{
		_pool_size = 0;

		// 清空事务map
		std::unique_lock<std::mutex> lck(_transaction_mutex);
		_transaction_conns.clear();
		_counter = 0;
		// 这里必须要释放，不然有可能造成死锁
		lck.unlock();

		// 这里最好用遍历的方式，而不要用clear
		for (auto& it : _conn_pool)
		{
			std::lock_guard<std::recursive_mutex>	lock(*(it.first));
			it.second.reset();
		}
	}
	debug_log("数据库连接池关闭.");
}

bool MysqlConnManager::Exec(const char* sql)
{
	size_t pos = getPos();

	auto pair = _conn_pool[pos];
	std::lock_guard<std::recursive_mutex>		lock(*(pair.first));
	return pair.second->Exec(sql);
}

bool MysqlConnManager::Exec(const char* sql, unsigned long len)
{
	size_t pos = getPos();

	auto pair = _conn_pool[pos];
	std::lock_guard<std::recursive_mutex>		lock(*(pair.first));
	return pair.second->Exec(sql, len);
}

std::shared_ptr<MysqlResult> MysqlConnManager::Query(const char* sql)
{
	size_t pos = getPos();

	auto pair = _conn_pool[pos];
	std::lock_guard<std::recursive_mutex>		lock(*(pair.first));
	return pair.second->Query(sql);
}

std::shared_ptr<MysqlResult> MysqlConnManager::Query(const char* sql, unsigned long len)
{
	size_t pos = getPos();

	auto pair = _conn_pool[pos];
	std::lock_guard<std::recursive_mutex>		lock(*(pair.first));
	return pair.second->Query(sql, len);
}