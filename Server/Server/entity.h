#ifndef __ENTITY_H_
#define __ENTITY_H_
#include <utility>
#include <memory>
#include "timer_component_manager.h"
#include "datapacket.h"

/*
* 所有继承自此类的实体，都不要直接用指针，必须用智能指针shared_ptr
*/
class Entity
{
public:
	enum class EntityType : unsigned char
	{
		MONSTER = 1,
		PLAYER,
		GOODS,
		SKILL,
	};

public:
	Entity(EntityType type, unsigned int id = 0, unsigned int model = 0, unsigned int sceneId = 0, unsigned int subSceneId = 0, int x = 0, int y = 0, std::string name = "");
	virtual ~Entity();
	Entity(Entity const&);
	Entity& operator= (Entity const&);

public:
	void SetPos(int x, int y, unsigned int sceneId = 0, unsigned int subSceneId = 0);
	std::pair<int, int> GetPos() const;
	int GetX() const
	{
		return _x;
	}
	int GetY() const
	{
		return _y;
	}
	void SetId(unsigned int id)
	{
		_id = id;
	}
	unsigned int GetId() const
	{
		return _id;
	}
	EntityType GetType() const
	{
		return _type;
	}
	unsigned int GetSceneId() const
	{
		return _sceneId;
	}
	unsigned int GetSubSceneId() const
	{
		return _subSceneId;
	}
	std::string GetName() const
	{
		return _name;
	}
	unsigned int GetModel() const
	{
		return _model;
	}
protected:
	virtual void LogicRun(const std::chrono::system_clock::duration&);		// 虚函数，处理实体的逻辑循环
	virtual std::shared_ptr<Entity> GetSharedBase() = 0;

public:
	void RegisterTimer(const std::string&, unsigned long, unsigned long, unsigned int, const std::function<void(void)>&);
	void UnRegisterTimer(const std::string&);


protected:
	unsigned int	_id;		// 实体的唯一id
	unsigned int	_model;		// 实体的模型ID
	EntityType		_type;		// 实体的类型
	unsigned int	_sceneId;	// 实体的场景ID
	unsigned int	_subSceneId;
	int				_x;			// 实体的坐标
	int				_y;
	std::string		_name;		// 实体的名字

private:
	TimerComponentManager		m_hTimerComponentMgr;
};
#endif