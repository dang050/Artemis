#pragma once

enum SystemId : unsigned int
{
	aeLoginSystem = 1,
	aeSceneSystem,
};

enum class tagLoginSystemCmd :unsigned int
{
	cLogin = 1,
	cRegister = 2,
	cGetUserData = 3,
	cCreateUser = 4,

	cMaxCodeId,

	sLogin = 1,
	sRegister = 2,
	sGetUserData = 3,
	sCreateUser = 4,
};

enum class tagSceneSystemCmd : unsigned int
{
	cMove = 1,
	cEnterScene,
	cLeaveScene,
	cEnterSceneFinish,

	cMaxCodeId,

	sMove = 1,
	sEnterScene,
	sLeaveScene,
	sEnterSceneBro,
	sMoveBro,
	sLeaveSceneBro,
};
