#ifndef __SCRIPT_VALUE_LIST_H_
#define __SCRIPT_VALUE_LIST_H_

#include "script_value.h"
#include <vector>

class ScriptValueList final
{
public:
	ScriptValueList();
	~ScriptValueList();
	ScriptValueList(const ScriptValueList&);
	ScriptValueList& operator= (const ScriptValueList&); 
	ScriptValueList(ScriptValueList&&);
	ScriptValueList& operator= (ScriptValueList&&);

public:
	// 添加一个值
	template<typename T>
	ScriptValueList& operator<< (T&& val)
	{
		m_values.emplace_back(std::forward<T>(val));
		return *this;
	}

	ScriptValueList& operator+= (const ScriptValueList& obj)
	{
		m_values.insert(m_values.end(), obj.m_values.begin(), obj.m_values.end());
	}

	ScriptValueList& operator+= (ScriptValueList&& obj)
	{
		for (auto& elem : obj.m_values)
		{
			m_values.emplace_back(std::move(elem));
		}
		obj.clear();
	}

public:
	// 将列表中的值压入栈中，列表不会被清空
	void PushArgs(lua_State* L, size_t count);
	// 从lua栈中取出返回值，函数返回取出了多少个值
	// 如果列表中已经存在值，则之前的值将不会清空
	int GetResults(lua_State* L, int count);
	// 将lua中传入的所有参数保存到值列表中，列表会被清空，函数返回读取了多少个参数
	int GetArguments(lua_State* L, int nStackIdx);

	inline size_t Count()
	{
		return m_values.size();
	}
	void clear()
	{
		m_values.clear();
	}
private:
	std::vector<ScriptValue>	m_values;
};
#endif