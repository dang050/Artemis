#ifndef __SUB_SYSTEM_H_
#define __SUB_SYSTEM_H_
#include <vector>
#include "datapacket.h"

template<typename T, typename ENTITY>
class SubSystem
{
	//	typedef void (SubSystem::*Handler)();
	using OnHandleNetPack = void(T::*)(CDataPacket&);
	using InheritClass = SubSystem<T, ENTITY>;
public:
	SubSystem(ENTITY* pEntity)
		:m_pEntity(pEntity)
	{}
	virtual ~SubSystem() {}
	SubSystem(const SubSystem<T, ENTITY>&) = delete;
	SubSystem<T, ENTITY>& operator= (const SubSystem<T, ENTITY>&) = delete;
	virtual void ProcessNetwork(unsigned int system, unsigned int cmd, CDataPacket& pack) = 0;
protected:
	ENTITY*							m_pEntity;
	const static OnHandleNetPack	m_hHandlers[];
};
#endif