#ifndef __SOCKET_IO_CONTEXT_H_
#define __SOCKET_IO_CONTEXT_H_
#include <winsock2.h>
#include <list>
#include <mutex>
#include <functional>
#include <memory>
#include <assert.h>
#include <algorithm>
#include "object_pool.h"

#define BUFFER_LENGTH	128

// 释放Socket
#define RELEASE_SOCKET(x)	do{if(x!=INVALID_SOCKET){closesocket(x); x=INVALID_SOCKET;}}while(0)
#define RELEASE(x)			do{if(x!=nullptr) {delete x; x = nullptr;}}while(0)
#define RELEASE_HANDLE(x)	do{if(x!=nullptr && x!=INVALID_HANDLE_VALUE){CloseHandle(x); x=nullptr;}}while(0)

enum OPERATION_TYPE :unsigned char
{
	NULL_POSTED,	// 用于初始化
	ACCEPT_POSTED,
	RECV_POSTED,
	SEND_POSTED,
};

/**********************************************
* 单句柄数据结构定义
***********************************************/
typedef struct _PER_IO_CONTEXT final
{
	OVERLAPPED		m_Overlapped;		// 每一个重叠网络操作的重叠结构
	SOCKET			m_sockAccept;		// 这个网络操作所使用的Socket
	WSABUF			m_wsaBuf;
	char			m_szBuffer[BUFFER_LENGTH];
	OPERATION_TYPE	m_opType;

	_PER_IO_CONTEXT();

	~_PER_IO_CONTEXT();

	void ResetBuffer();

	_PER_IO_CONTEXT& operator= (const _PER_IO_CONTEXT&) = delete;
	_PER_IO_CONTEXT(const _PER_IO_CONTEXT&) = delete;

}PER_IO_CONTEXT, *PPER_IO_CONTEXT;

typedef struct _PER_SOCKET_CONTEXT final
{
	SOCKET			m_Socket;				// 每一个客户端连接的Socket，投递Accept请求的时候这里是监听套接字
	SOCKADDR_IN		m_ClientAddr;			// 客户端的地址

	// 销毁套接字
	void Destroy()
	{
		RELEASE_SOCKET(m_Socket);
	}

	// 四个回调函数
	std::function<void()>			m_connectCallback;
	std::function<void(WSABUF*)>	m_recvCallback;
	std::function<void(PPER_IO_CONTEXT)>	m_sendCallback;
	std::function<void()>			m_disconnectCallback;

	// 对于同一个套接字，可以同时在上面投递多个IO请求
	std::mutex m_hMutex;
	_PER_SOCKET_CONTEXT();
	~_PER_SOCKET_CONTEXT();
	std::shared_ptr<PER_IO_CONTEXT> GetNewIoContext();

	void RemoveContext(std::shared_ptr<PER_IO_CONTEXT>& pContext);
	void RemoveContext(PPER_IO_CONTEXT pContext);

	void SetCallbackFunc(std::function<void()> const&, std::function<void()> const&, std::function<void(WSABUF*)> const&, std::function<void(PPER_IO_CONTEXT)> const&);
	_PER_SOCKET_CONTEXT& operator= (const _PER_SOCKET_CONTEXT&) = delete;
	_PER_SOCKET_CONTEXT(const _PER_SOCKET_CONTEXT&) = delete;

private:
	std::list<std::shared_ptr<PER_IO_CONTEXT>>	m_arrayIoContext;	// 客户端网络操作的上下文

	// 这里的ObjectPool必须声明为static
	// 原因是：客户端突然断开连接，而list中的PER_IO_CONTEXT智能指针可能还在外面存在(调用GetNewIoContext()函数的地方)
	// 导致的结果是PER_IO_CONTEXT池已经销毁了，而PER_IO_CONTEXT还未release，最终释放的时候会导致未定义行为
	static	ObjectPool<PER_IO_CONTEXT>			m_poolIoContext;	// PerIoContext对象池
	static	std::mutex		m_hPoolMutex;		// 池的互斥事件

}PER_SOCKET_CONTEXT, *PPER_SOCKET_CONTEXT;
#endif