#include "scene_aoi.h"
#include <iterator>

SceneAOI::SceneAOI(unsigned int x, unsigned int y)
:m_nAOIX(x), m_nAOIY(y)
{
}

SceneAOI::~SceneAOI()
{}


void SceneAOI::Enter(const std::shared_ptr<Entity>& obj, std::vector<std::shared_ptr<Entity>>& nodes)
{
	auto it = Insert(obj);
	GetAOINodes(it, nodes);
}

void SceneAOI::Leave(const std::shared_ptr<Entity>& obj, std::vector<std::shared_ptr<Entity>>& nodes)
{
	Erase(SceneAOI::MakeKey(obj), nodes);
}


void SceneAOI::Move(std::shared_ptr<Entity>& obj, const std::pair<int, int>& newPos, std::vector<std::shared_ptr<Entity>>& leaveNodes, std::vector<std::shared_ptr<Entity>>& updateNodes, std::vector<std::shared_ptr<Entity>>& enterNodes)
{
	std::unordered_set<std::shared_ptr<Entity>> oldNodes;
	std::unordered_set<std::shared_ptr<Entity>> newNodes;

	Erase(SceneAOI::MakeKey(obj), oldNodes);
	// 修改玩家的坐标
	obj->SetPos(newPos.first, newPos.second);
	GetAOINodes(Insert(obj), newNodes);

	for (auto& iter : oldNodes)
	{
		if (newNodes.find(iter) != newNodes.end())
		{
			// 新老nodes中都存在，则放入updateNodes
			updateNodes.push_back(iter);
			newNodes.erase(iter);
		}
		else
		{
			// 在老nodes中，而不在新nodes中，则放入leaveNodes
			leaveNodes.push_back(iter);
		}
	}

	// 只存在新nodes中，则放入enterNodes
	std::copy(newNodes.cbegin(), newNodes.cend(), std::back_inserter(enterNodes));

	return;
}

// 按x轴坐标从小到大插入
std::list<std::shared_ptr<Entity>>::iterator SceneAOI::Insert(const std::shared_ptr<Entity>& obj)
{
	std::list<std::shared_ptr<Entity>>::iterator iter = m_pNodes.begin();

	for (; iter != m_pNodes.end(); ++iter)
	{
		if ((*iter)->GetX() > obj->GetX())
		{
			break;
		}
	}
	auto it = m_pNodes.insert(iter, obj);
	m_nIndexs.insert(std::make_pair(SceneAOI::MakeKey(obj), it));
	return it;
}

void SceneAOI::GetAOINodes(const std::list<std::shared_ptr<Entity>>::iterator& iter, std::vector<std::shared_ptr<Entity>>& nodes)
{
	auto pos = (*iter)->GetPos();
	int minX = pos.first - m_nAOIX;
	int maxX = pos.first + m_nAOIX;
	int minY = pos.second - m_nAOIY;
	int maxY = pos.second + m_nAOIY;
	std::list<std::shared_ptr<Entity>>::iterator it = iter != m_pNodes.begin() ? std::prev(iter):iter;

	// 先往前遍历
	while (it != m_pNodes.begin())
	{
		auto pos = (*it)->GetPos();
		if (pos.first >= minX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.push_back(*it);
			}
		}
		else
		{
			break;
		}
		it = std::prev(it);
	}

	// 特殊处理begin()
	if (it == m_pNodes.begin() && it != iter)
	{
		auto pos = (*it)->GetPos();
		if (pos.first >= minX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.push_back(*it);
			}
		}
	}


	// 再往后遍历
	it = iter != m_pNodes.end() ? std::next(iter) :iter;
	while (it != m_pNodes.end())
	{
		auto pos = (*it)->GetPos();
		if (pos.first <= maxX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.push_back(*it);
			}
		}
		else
		{
			break;
		}
		it = std::next(it);
	}
}

void SceneAOI::GetAOINodes(const std::list<std::shared_ptr<Entity>>::iterator& iter, std::unordered_set<std::shared_ptr<Entity>>& nodes)
{
	auto pos = (*iter)->GetPos();
	int minX = pos.first - m_nAOIX;
	int maxX = pos.first + m_nAOIX;
	int minY = pos.second - m_nAOIY;
	int maxY = pos.second + m_nAOIY;
	std::list<std::shared_ptr<Entity>>::iterator it = iter != m_pNodes.begin() ? std::prev(iter) : iter;

	// 先往前遍历
	while (it != m_pNodes.begin())
	{
		auto pos = (*it)->GetPos();
		if (pos.first >= minX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.insert(*it);
			}
		}
		else
		{
			break;
		}
		it = std::prev(it);
	}

	// 特殊处理begin()
	if (it == m_pNodes.begin() && it != iter)
	{
		auto pos = (*it)->GetPos();
		if (pos.first >= minX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.insert(*it);
			}
		}
	}

	// 再往后遍历
	it = iter != m_pNodes.end() ? std::next(iter) : iter;
	while (it != m_pNodes.end())
	{
		auto pos = (*it)->GetPos();
		if (pos.first <= maxX)
		{
			if (pos.second >= minY && pos.second <= maxY)
			{
				nodes.insert(*it);
			}
		}
		else
		{
			break;
		}
		it = std::next(it);
	}
}