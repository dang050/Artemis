#ifndef __ERROR_HANDLER_H_
#define __ERROR_HANDLER_H
#pragma warning(disable:4091)		// 关闭warning C4091:没有声明变量时忽略“”的左侧
#include <windows.h>
#include <DbgHelp.h>
#include <tchar.h>
#pragma comment(lib, "Dbghelp.lib")

namespace iocp
{
	// 创建Dump文件  
	//   
	void CreateDumpFile(LPCWSTR lpstrDumpFilePathName, EXCEPTION_POINTERS *pException)
	{
		// 创建Dump文件  
		//  
		HANDLE hDumpFile = CreateFile(lpstrDumpFilePathName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		// Dump信息  
		//  
		MINIDUMP_EXCEPTION_INFORMATION dumpInfo;
		dumpInfo.ExceptionPointers = pException;
		dumpInfo.ThreadId = GetCurrentThreadId();
		dumpInfo.ClientPointers = TRUE;

		// 写入Dump文件内容  
		//  
		MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hDumpFile, MiniDumpNormal, &dumpInfo, NULL, NULL);

		CloseHandle(hDumpFile);
	}

	// 处理Unhandled Exception的回调函数  
	//  
	LONG ApplicationCrashHandler(EXCEPTION_POINTERS *pException)
	{
		// 写dmp文件
		CreateDumpFile(_T("error.dmp"), pException);
		// 这里弹出一个错误对话框并退出程序   
		FatalAppExit(0, _T("*** Unhandled Exception! ***"));
		return EXCEPTION_EXECUTE_HANDLER;
	}
}

#endif