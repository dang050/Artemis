#include <algorithm>
#include "actor_manager.h"
#include "tool.h"
#include "logger.h"

ActorManager::ActorManager()
{
}

ActorManager::~ActorManager()
{
	ClearActor();
}

void ActorManager::Start()
{
	debug_log("用户管理器启动.");
}

void ActorManager::Stop()
{
	// 强制所有用户下线
	ClearActor();
	debug_log("用户管理器关闭.");
}

void ActorManager::AddActor(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext)
{
	assert(pSocketContext);
	std::shared_ptr<Actor> actor = std::make_shared<Actor>(pSocketContext);
	std::lock_guard<std::mutex>	lck(m_hMutex);
	m_listActors.push_back(actor);
}

void ActorManager::RemoveLoginedActor(unsigned int actorId)
{
	std::lock_guard<std::mutex>		lck(m_hLoginMutex);
	m_loginedActors.erase(actorId);
}

void ActorManager::AddLoginedActor(const std::shared_ptr<Actor>& actor)
{
	unsigned int actorId = actor->GetId();
	std::lock_guard<std::mutex>		lck(m_hLoginMutex);
	m_loginedActors[actorId] = actor;
}

void ActorManager::ClearActor()
{
	{
		std::lock_guard<std::mutex>	lck(m_hMutex);
		m_loginedActors.clear();
	}
	{
		std::lock_guard<std::mutex>	lck(m_hLoginMutex);
		m_listActors.clear();
	}
}

void ActorManager::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
	// 判断是否执行ActorGC()
	if (m_gcTimer.CheckAndSet(nCurrTick))
	{
		ActorGC();
	}

	// 这里避免对m_listActor加锁
	std::for_each(m_listActors.cbegin(), m_listActors.cend(), [&nCurrTick](std::list<std::shared_ptr<Actor>>::const_reference elem) {
		elem->LogicRun(nCurrTick);
	});
}

void ActorManager::ActorGC()
{
	std::lock_guard<std::mutex>	lck(m_hMutex);
	m_listActors.remove_if([](std::list<std::shared_ptr<Actor>>::const_reference elem) {
		return elem->CanGC();
	});
}

bool ActorManager::CheckOnline(unsigned int actorId) 
{
	if (m_loginedActors.find(actorId) != m_loginedActors.end())
	{
		return true;
	}
	return false;
}