/* ����״̬�� */
#ifndef __FSM_H_
#define __FSM_H_
#include <memory>
#include <unordered_map>
#include "state.h"

template<typename T>
class fsm
{
public:
	fsm();
	virtual ~fsm();
	fsm(const fsm<T>&) = delete;
	fsm<T>& operator= (const fsm<T>&) = delete;

protected:
	void	RegisterState(const std::shared_ptr<State<T>>& state);		// ע��״̬

public:
	void	ChangeState(FSMStateId stateId);		// ״̬ת��


protected:
	FSMStateId		m_nCurrStateId;			// ��ǰ״̬id
	std::unordered_map<FSMStateId, std::shared_ptr<State<T>>>		m_mapStates;		// ����״̬
};

template<typename T>
fsm<T>::fsm()
{}

template<typename T>
fsm<T>::~fsm()
{}

template<typename T>
void fsm<T>::RegisterState(const std::shared_ptr<State<T>>& state)
{
	FSMStateId id = state->GetStateId();
	m_mapStates[id] = state;
}

template<typename T>
void fsm<T>::ChangeState(FSMStateId stateId)
{
	if (stateId == m_nCurrStateId)
	{
		return;
	}
	else
	{
		if (m_mapStates.find(stateId) != m_mapStates.end())
		{
			m_nCurrStateId = stateId;
			m_mapStates[m_nCurrStateId]->handle();
		}
		else
		{
			throw "unknow state";			// δ֪״̬
		}
	}
}
#endif