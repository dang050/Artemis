#include "script_value.h"

ScriptValue::ScriptValue()
:m_type(vNil)
{
	memset(&m_data, 0, sizeof(m_data));
}

ScriptValue::~ScriptValue()
{
	clear();
}

ScriptValue::ScriptValue(const ScriptValue& obj)
{
	switch (obj.m_type)
	{
	case vBool:
		 operator= (obj.m_data.bVal);
		 break;
	case vLightData:
		 operator= (obj.m_data.pLightPtr);
		 break;
	case vNumber:
		 operator= (obj.m_data.numVal);
		 break;
	case vString:
		 operator= (obj.m_data.strVal.str);
		 break;
	case vCFunction:
		 operator= (obj.m_data.cfnVal);
		 break;
	case vUserData:
		SetUserData(obj.m_data.userVal.ptr, obj.m_data.userVal.len);
		break;
	default:
		clear();
		break;
	}
}

ScriptValue::ScriptValue(ScriptValue&& obj)
{
	m_type = obj.m_type;
	m_data = obj.m_data;

	obj.m_type = vNil;
	memset(&obj.m_data, 0, sizeof(obj.m_data));
}

ScriptValue& ScriptValue::operator= (ScriptValue&& obj)
{
	if (this == &obj)
		return *this;
	clear();
	m_type = obj.m_type;
	m_data = obj.m_data;

	obj.m_type = vNil;
	memset(&obj.m_data, 0, sizeof(obj.m_data));
	return *this;
}

ScriptValue& ScriptValue::operator= (const ScriptValue& obj)
{
	if (this == &obj)
		return *this;

	switch (obj.m_type)
	{
	case vBool:
		return operator= (obj.m_data.bVal);
	case vLightData:
		return operator= (obj.m_data.pLightPtr);
	case vNumber:
		return operator= (obj.m_data.numVal);
	case vString:
		return operator= (obj.m_data.strVal.str);
	case vCFunction:
		return operator= (obj.m_data.cfnVal);
	case vUserData:
		return SetUserData(obj.m_data.userVal.ptr, obj.m_data.userVal.len);
	default:
		clear();
		break;
	}
	return *this;
}

void ScriptValue::clear()
{
	if (m_type == vString)
	{
		free(static_cast<void*>(m_data.strVal.str));
	}
	else if (m_type == vUserData)
	{
		free(m_data.userVal.ptr);
	}
	m_type = vNil;
	memset(&m_data, 0, sizeof(m_data));
}

ScriptValue& ScriptValue::SetUserData(void* data, SIZE_T len)
{
	clear();
	m_type = vUserData;
	m_data.userVal.len = len;
	m_data.userVal.ptr = malloc(len);
	memcpy(m_data.userVal.ptr, data, len);
	return *this;
}