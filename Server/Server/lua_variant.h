#ifndef __LUA_VARIANT_H_
#define __LUA_VARIANT_H_
#include <cstdlib>

class LuaVariant final
{
public:
	/* 变量值类型 */
	typedef enum tagVarType : unsigned char
	{
		vNil = 0,		// 无值
		vNumber,			// 数字
		vString,			// 字符串
		vArray,			// 数组
	}VARTYPE;

public:
	LuaVariant();
	~LuaVariant();
	LuaVariant(const LuaVariant&);
	LuaVariant(LuaVariant&&);
	LuaVariant& operator= (const LuaVariant&);
	LuaVariant& operator= (LuaVariant&&);

public:
	void clear();
	LuaVariant& operator= (double val);
	LuaVariant& operator= (const char* val);

public:
	// 类型转换
	inline explicit operator double() const
	{
		if (_type == vNumber)
			return _data.n;
		else if (_type == vString)
		{
			char *e = nullptr;
			double d = strtod(_data.s.str, &e);
			if (!e)
			{
				return d;
			}
		}
		return 0.0f;
	}
	inline explicit operator const char* () const
	{
		if (_type == vString)
		{
			return _data.s.str;
		}
		return nullptr;
	}
	inline explicit operator const LuaVariant**() const
	{
		if (_type == vArray)
		{
			return const_cast<const LuaVariant**>(_data.a.list);
		}
		return nullptr;
	}

public:
	LuaVariant* Get(const char* sName);
	const LuaVariant& Set(const char* name, double val);
	const LuaVariant& Set(const char* name, const char* val);
	LuaVariant& Set(const char* sName);
	VARTYPE Type() const
	{
		return _type;
	}
	void SetType(VARTYPE type)
	{
		_type = type;
	}
private:
	LuaVariant* Get(size_t key);
	size_t hash(const char* str) const;

public:
	// 获取对象存储时需要多少字节
	size_t GetStoreSize() const;
	/*
	* Comments: 将变量数据打包到数据流
	* Param const char * ptr: 数据流指针
	* Param size_t size: 数据流长度
	* @Return size_t: 函数返回使用的字节数
	*/
	size_t Pack(char *dst, size_t size) const;
	/*
	* Comments: 从数据流中加载变量数据
	* Param const char * ptr: 数据流指针
	* Param size_t size: 数据流长度
	* @Return size_t: 函数返回使用的字节数
	*/
	size_t Load(const char* src, size_t size);
private:
	VARTYPE	_type;		// 数据类型
	size_t	_key;		// 哈希得到的索引
	union 
	{
		// 数值
		double n;
		// 字符串
		struct
		{
			unsigned int len;
			char *str;
		}s;
		// 数组
		struct  
		{
			LuaVariant** list;
			unsigned int len;
		}a;
	}_data;
};
#endif