#ifndef __MYSQL_RESULT_H_
#define __MYSQL_RESULT_H_

#include <memory>
#include <winsock2.h>

#ifdef _WIN64
#include <Mysql\x64\include\mysql.h>
#pragma comment(lib, "Mysql\\x64\\libmysql.lib")
#else
#include <Mysql\include\mysql.h>
#pragma comment(lib, "Mysql\\libmysql.lib")
#endif

class MysqlResult final : public std::enable_shared_from_this<MysqlResult>
{
public:
	MysqlResult(MYSQL_RES* result = nullptr);
	~MysqlResult();
	MysqlResult(const MysqlResult&) = delete;
	MysqlResult(MysqlResult&& obj);
	MysqlResult& operator= (const MysqlResult&) = delete;
	MysqlResult& operator= (MysqlResult&& obj);

public:
	explicit operator bool() const
	{
		if (_res != nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
public:
	std::shared_ptr<MysqlResult> get()
	{
		return shared_from_this();
	}

	MYSQL_ROW	GetCurrentRow()
	{
		return mysql_fetch_row(_res);
	}

	my_ulonglong GetCurrentNumRows() const
	{
		return mysql_num_rows(_res);
	}

	unsigned int GetCurrentNumFields() const
	{
		return mysql_num_fields(_res);
	}
private:
	MYSQL_RES* _res;
};

#endif