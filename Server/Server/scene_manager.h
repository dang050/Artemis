#ifndef __SCENE_MANAGER_H_
#define __SCENE_MANAGER_H_
#include "singleton.h"
#include "scene.h"
#include <unordered_map>
#include <future>

class SceneManager final : public Singleton<SceneManager>
{
public:
	SceneManager(std::chrono::milliseconds const& interval = std::chrono::milliseconds(1000));
	~SceneManager();
	SceneManager(const SceneManager&) = delete;
	SceneManager& operator= (const SceneManager&) = delete;

public:
	void Start();
	void Stop();
	std::tuple<unsigned int, int, int> GetInitScenePos() const
	{
		return std::make_tuple(m_nInitSceneId, m_nInitX, m_nInitY);
	}
	std::shared_ptr<Scene> EnterScene(std::shared_ptr<Entity>& entity, unsigned int sceneId, unsigned int subId = 0);
	bool CheckEnterScene(std::shared_ptr<Entity>& entity, unsigned int sceneId, unsigned int subId = 0);

public:
	void LogicRun();

private:
	std::string MakeKey(unsigned int sceneId, unsigned int subId)
	{
		return std::to_string(sceneId) + "_" + std::to_string(subId);
	}
private:
	std::unordered_map<std::string, std::shared_ptr<Scene>>	m_hMapScenes;
	std::chrono::milliseconds		m_logicLoopInterval;
	bool							m_bState;
	std::future<void>				m_hThread;

private:
	unsigned int					m_nInitSceneId;
	int								m_nInitX;
	int								m_nInitY;
};
#endif