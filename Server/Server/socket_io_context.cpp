#include "socket_io_context.h"

ObjectPool<PER_IO_CONTEXT> _PER_SOCKET_CONTEXT::m_poolIoContext;
std::mutex	_PER_SOCKET_CONTEXT::m_hPoolMutex;

_PER_IO_CONTEXT::_PER_IO_CONTEXT()
{
	ZeroMemory(&m_Overlapped, sizeof(m_Overlapped));
	ZeroMemory(m_szBuffer, BUFFER_LENGTH);
	m_sockAccept = INVALID_SOCKET;
	m_wsaBuf.buf = m_szBuffer;
	m_wsaBuf.len = BUFFER_LENGTH;
	m_opType = NULL_POSTED;
}

_PER_IO_CONTEXT::~_PER_IO_CONTEXT()
{
	//	RELEASE_SOCKET(m_sockAccept);
}

void _PER_IO_CONTEXT::ResetBuffer()
{
	ZeroMemory(m_szBuffer, BUFFER_LENGTH);
	m_wsaBuf.len = BUFFER_LENGTH;
}

_PER_SOCKET_CONTEXT::_PER_SOCKET_CONTEXT()
{
	m_Socket = INVALID_SOCKET;
	memset(&m_ClientAddr, 0, sizeof(m_ClientAddr));
}

_PER_SOCKET_CONTEXT::~_PER_SOCKET_CONTEXT()
{
	RELEASE_SOCKET(m_Socket);

	// 释放掉所有的IO上下文数据
	std::lock_guard<std::mutex> lck(m_hMutex);
	m_arrayIoContext.clear();

	m_connectCallback = nullptr;
	m_recvCallback = nullptr;
	m_sendCallback = nullptr;
	m_disconnectCallback = nullptr;
}

std::shared_ptr<PER_IO_CONTEXT> _PER_SOCKET_CONTEXT::GetNewIoContext()
{
	std::unique_lock<std::mutex> poolLck(_PER_SOCKET_CONTEXT::m_hPoolMutex);
	std::shared_ptr<PER_IO_CONTEXT> p(_PER_SOCKET_CONTEXT::m_poolIoContext.acquire(), [](PPER_IO_CONTEXT p){
		// 注意这里的m_poolIoContext是线程不安全的
		// 这里是不会造成死锁的
		std::lock_guard<std::mutex>	_poolLck(_PER_SOCKET_CONTEXT::m_hPoolMutex);
		_PER_SOCKET_CONTEXT::m_poolIoContext.release(p);
	});
	poolLck.unlock();
	std::lock_guard<std::mutex> lck(m_hMutex);
	m_arrayIoContext.push_back(p);
	return p;
}

void _PER_SOCKET_CONTEXT::RemoveContext(std::shared_ptr<PER_IO_CONTEXT>& pContext)
{
	assert(pContext != nullptr);
	std::lock_guard<std::mutex> lck(m_hMutex);
	auto pos = std::find(m_arrayIoContext.cbegin(), m_arrayIoContext.cend(), pContext);
	if (pos != m_arrayIoContext.cend())
	{
		m_arrayIoContext.erase(pos);
	}
}

void _PER_SOCKET_CONTEXT::RemoveContext(PPER_IO_CONTEXT pContext)
{
	assert(pContext != nullptr);
	std::lock_guard<std::mutex> lck(m_hMutex);
	auto pos = std::find_if(m_arrayIoContext.cbegin(), m_arrayIoContext.cend(), [&pContext](const std::shared_ptr<PER_IO_CONTEXT>& context)
	{
		if (context.get() == pContext)
		{
			return true;
		}
		return false;
	});
	if (pos != m_arrayIoContext.cend())
	{
		m_arrayIoContext.erase(pos);
	}
}

void _PER_SOCKET_CONTEXT::SetCallbackFunc(std::function<void()> const &connectCallback, std::function<void()> const &disconnectCallback, std::function<void(WSABUF*)> const &recvCallback, std::function<void(PPER_IO_CONTEXT)> const &sendCallback)
{
	m_connectCallback = connectCallback;
	m_disconnectCallback = disconnectCallback;
	m_recvCallback = recvCallback;
	m_sendCallback = sendCallback;
}