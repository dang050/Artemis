#include "timer_component_manager.h"

TimerComponentManager::TimerComponentManager()
{}

TimerComponentManager::~TimerComponentManager()
{}

void TimerComponentManager::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
	m_arrayInvalidIter.clear();
	std::unique_lock<std::mutex>	lock(m_hMutex);
	for (auto& it = m_mapTimerComponents.begin(); it != m_mapTimerComponents.end(); ++it)
	{
		it->second->Run(nCurrTick);
		if (!it->second->IsValid())
		{
			m_arrayInvalidIter.push_back(it);
		}
	}

	for (auto& it : m_arrayInvalidIter)
	{
		m_mapTimerComponents.erase(it);
	}
}

void TimerComponentManager::RegTimerComponent(const std::string& name, unsigned long nDelay, unsigned long nInterval, unsigned int nCount, const std::function<void(void)>& func)
{
	if (nCount > 0)
	{
		std::shared_ptr<TimerComponent> pTimerComponent = std::make_shared<TimerComponent>(TimerComponent::millsecond_type(nDelay), TimerComponent::millsecond_type(nInterval), nCount, func);
		std::lock_guard<std::mutex>		lock(m_hMutex);
		m_mapTimerComponents[name] = pTimerComponent;
	}
}

void TimerComponentManager::UnRegTimerComponent(const std::string& name)
{
	std::lock_guard<std::mutex>		lock(m_hMutex);
	auto& iter = m_mapTimerComponents.find(name);
	if (iter != m_mapTimerComponents.end())
	{
		m_mapTimerComponents.erase(iter);
	}
}