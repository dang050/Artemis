#include "scene_manager.h"
#include "tool.h"
#include "logger.h"
#include "parse_json.h"

SceneManager::SceneManager(std::chrono::milliseconds const& interval /* = std::chrono::milliseconds(10) */)
:m_logicLoopInterval(interval),
m_bState(false)
{
}

SceneManager::~SceneManager()
{
	if (m_bState)
	{
		Stop();
	}
}

void SceneManager::Start()
{
	std::string file = "scenes.json";
	std::ifstream ifs;
	ifs.open(file, std::ios::binary);
	if (!ifs)
	{
		error_log("Lose scenes Config File");
		return;
	}

	Json::Reader	reader;
	Json::Value		root;
	if (reader.parse(ifs, root))
	{
		for (unsigned int i = 0; i < root["MainScene"].size(); i++)
		{
			// 只加载主场景
			unsigned int sceneId = root["MainScene"][i].asUInt();
			m_hMapScenes[MakeKey(sceneId, 0)] = std::make_shared<Scene>(sceneId, 0);
		}

		// 获取初始场景
		m_nInitSceneId = root["InitScene"]["SceneId"].asUInt();
		m_nInitX = root["InitScene"]["X"].asInt();
		m_nInitY = root["InitScene"]["Y"].asInt();
	}
	else
	{
		error_log("Parse scenes Config File occurs error");
	}
	ifs.close();

	m_bState = true;
	m_hThread = std::async(std::launch::async, &SceneManager::LogicRun, this);

	debug_log("场景管理器启动.");
}

void SceneManager::Stop()
{
	m_bState = false;
	m_hThread.get();

	debug_log("场景管理器关闭.");
}

// 场景逻辑循环
void SceneManager::LogicRun()
{
	while (m_bState)
	{
		std::chrono::system_clock::duration	nCurrTick = iocp::GetCurrTickCount();
		for (auto& it : m_hMapScenes)
		{
			it.second->LogicRun(nCurrTick);
		}
		std::this_thread::sleep_for(m_logicLoopInterval);
	}
}

std::shared_ptr<Scene> SceneManager::EnterScene(std::shared_ptr<Entity>& entity, unsigned int sceneId, unsigned int subId)
{
	std::shared_ptr<Scene>	handler = nullptr;
	auto iter = m_hMapScenes.find(MakeKey(sceneId, subId));
	if (iter != m_hMapScenes.end())
	{
		iter->second->Enter(entity, entity->GetX(), entity->GetY());
		handler = iter->second;
	}
	return handler;
}


// 进入场景的条件检查
bool SceneManager::CheckEnterScene(std::shared_ptr<Entity>& entity, unsigned int sceneId, unsigned int subId /* = 0 */)
{
	auto iter = m_hMapScenes.find(MakeKey(sceneId, subId));
	if (iter != m_hMapScenes.end())
	{
		return true;
	}
	return false;
}