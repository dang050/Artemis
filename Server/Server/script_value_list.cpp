#include "script_value_list.h"

ScriptValueList::ScriptValueList()
{

}

ScriptValueList::~ScriptValueList()
{
	clear();
}

ScriptValueList::ScriptValueList(const ScriptValueList& obj)
:m_values(obj.m_values)
{
}

ScriptValueList::ScriptValueList(ScriptValueList&& obj)
: m_values(std::move(obj.m_values))
{
}

ScriptValueList& ScriptValueList::operator= (const ScriptValueList& obj)
{
	if (this == &obj)
		return *this;
	clear();
	m_values = obj.m_values;
	return *this;
}

ScriptValueList& ScriptValueList::operator= (ScriptValueList&& obj)
{
	if (this == &obj)
		return *this;
	clear();
	m_values = std::move(obj.m_values);
	return *this;
}

// PushArgs是不支持userdata的
void ScriptValueList::PushArgs(lua_State* L, size_t count)
{
	for (size_t index = 0; index != count; ++index)
	{
		ScriptValue& value = m_values[index];
		switch (value.GetType())
		{
		case ScriptValue::vBool:
			lua_pushboolean(L, static_cast<bool>(value));
			break;
		case ScriptValue::vLightData:
		{
			void* ptr = static_cast<void*>(value);
			if (ptr != nullptr)
			{
				lua_pushlightuserdata(L, ptr);
			}
			else
			{
				lua_pushnil(L);
			}
			break;
		}
		case ScriptValue::vNumber:
			lua_pushnumber(L, static_cast<double>(value));
			break;
		case ScriptValue::vString:
		{
			const char* str = static_cast<const char*>(value);
			if (str != nullptr)
			{
				//	lua_pushlstring(L, static_cast<const char*>(value), strlen(static_cast<const char*>(value)));
				lua_pushstring(L, static_cast<const char*>(value));
			}
			else
			{
				lua_pushnil(L);
			}
			break;
		}
		case ScriptValue::vCFunction:
		{
			lua_CFunction pfn = static_cast<lua_CFunction>(value);
			if (pfn != nullptr)
			{
				lua_pushcfunction(L, pfn);
			}
			else
			{
				lua_pushnil(L);
			}
			break;
		}
		default:
			lua_pushnil(L);
			break;
		}
	}
}

int ScriptValueList::GetResults(lua_State* L, int count)
{
	int result = 0;
	int type = 0;
	while (count != 0)
	{
		type = lua_type(L, -count);
		switch (type)
		{
		case LUA_TBOOLEAN:
			operator<<(lua_toboolean(L, -count));
			break;
		case LUA_TLIGHTUSERDATA:
			operator<<(lua_touserdata(L, -count));
			break;
		case LUA_TNUMBER:
			operator<<(lua_tonumber(L, -count));
			break;
		case LUA_TSTRING:
			operator<<(lua_tostring(L, -count));
			break;
		case LUA_TUSERDATA:
		{
			ScriptValue		tempVal;
			tempVal.SetUserData(lua_touserdata(L, -count), lua_objlen(L, -count));
			operator<<(std::move(tempVal));
			break;
		}
		default:
			// 其它不支持的类型放个Nil进去
			operator<<(ScriptValue());
			break;
		}
		count--;
		result++;
	}
	// 将返回值弹出栈
	lua_pop(L, result);
	return result;
}

int ScriptValueList::GetArguments(lua_State* L, int nStackIdx)
{
	clear();
	int nTop = lua_gettop(L);
	int type = 0;
	int result = 0;
	while (nStackIdx <= nTop)
	{
		type = lua_type(L, nStackIdx);
		switch (type)
		{
		case LUA_TBOOLEAN:
			operator<<(lua_toboolean(L, nStackIdx));
			break;
		case LUA_TLIGHTUSERDATA:
			operator<<(lua_touserdata(L, nStackIdx));
			break;
		case LUA_TNUMBER:
			operator<<(lua_tonumber(L, nStackIdx));
			break;
		case LUA_TSTRING:
			operator<<(lua_tostring(L, nStackIdx));
			break;
		case LUA_TUSERDATA:
		{
			ScriptValue		tempVal;
			tempVal.SetUserData(lua_touserdata(L, nStackIdx), lua_objlen(L, nStackIdx));
			operator<<(std::move(tempVal));
			break;
		}
		default:
			// 其它不支持的类型放个Nil进去
			operator<<(ScriptValue());
			break;
		}
		result++;
		nStackIdx++;
	}
	// 将取出来的内容弹出栈
	lua_pop(L, result);
	return result;
}