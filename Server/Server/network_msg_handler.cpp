#include "network_msg_handler.h"
#include "logger.h"

NetWorkMsgHandler::NetWorkMsgHandler(std::shared_ptr<PER_SOCKET_CONTEXT>& context, ConnectCallback const &connectCallback, DisconnectCallback const &disconnectCallback, RecvCallback const &recvCallback, SendCallback const &sendCallback)
:m_pContext(context),
m_connectCallback(connectCallback),
m_disconnectCallback(disconnectCallback),
m_recvCallback(recvCallback),
m_sendCallback(sendCallback),
m_recvPack(),
m_bFirst(true),
m_bAtom(false)
{
	// 注册回调函数
	context->SetCallbackFunc(
		std::bind(std::mem_fun(&NetWorkMsgHandler::connectCallback), this),
		std::bind(std::mem_fun(&NetWorkMsgHandler::disconnectCallback), this),
		std::bind(std::mem_fun(&NetWorkMsgHandler::recvCallback), this, std::placeholders::_1),
		std::bind(std::mem_fun(&NetWorkMsgHandler::sendCallback), this, std::placeholders::_1)
		);
}

NetWorkMsgHandler::~NetWorkMsgHandler()
{
	m_pContext.reset();
	m_connectCallback = nullptr;
	m_disconnectCallback = nullptr;
	m_recvCallback = nullptr;
	m_sendCallback = nullptr;
}

void NetWorkMsgHandler::connectCallback()
{
	debug_log("客户端 %s:%d 连入.", inet_ntoa(m_pContext->m_ClientAddr.sin_addr), ntohs(m_pContext->m_ClientAddr.sin_port));
	m_connectCallback();
}

void NetWorkMsgHandler::disconnectCallback()
{
	debug_log("客户端 %s:%d 下线.", inet_ntoa(m_pContext->m_ClientAddr.sin_addr), ntohs(m_pContext->m_ClientAddr.sin_port));
	m_disconnectCallback();
}

void NetWorkMsgHandler::recvCallback(WSABUF* buf)
{
	std::lock_guard<std::mutex>		lck(m_hMutex);
	// 在这里解决粘包问题
	size_t recvLen = buf->len;
	char *pos = buf->buf;
	while (recvLen > 0)
	{
		size_t packLen = 0;
		if (m_recvPack.size() == 0)
		{
			packLen = *(static_cast<size_t *>(static_cast<void*>(pos)));
		}
		else
		{
			packLen = static_cast<size_t>(m_recvPack);
		}

		size_t len = min(recvLen, packLen - m_recvPack.size());
		m_recvPack.WriteBuffer(pos, len);

		if (m_recvPack.size() == packLen)
		{
			// 一个完整的包接收完毕
			m_recvCallback(m_recvPack);
			m_recvPack.clear();
		}

		recvLen -= len;
		pos += len;
	}

}

void NetWorkMsgHandler::sendCallback(PPER_IO_CONTEXT pIoContext)
{
	std::lock_guard<std::mutex>	lck(m_hQueueMutex);

	// 如果发送队列不为空，则直接调用WSASend进行发送
	if (!m_sendPackQueue.empty())
	{
		auto buff = pIoContext->m_wsaBuf.buf;
		size_t len = BUFFER_LENGTH;
		auto pack = m_sendPackQueue.front();

		if (m_bFirst)
		{
			// 第一个包，把长度加进去
			auto pack_size = pack.size() + sizeof(unsigned int);
			memcpy(buff, &pack_size, sizeof(unsigned int));
			buff += sizeof(unsigned int);
			len -= sizeof(unsigned int);
		}

		if (pack.size() > len)
		{
			pack.ReadBuffer(buff, len);
			len -= len;
			m_bFirst = false;
		}
		else
		{
			len -= pack.size();
			pack.ReadBuffer(buff, pack.size());

			// 从队列中弹出
			m_sendPackQueue.pop();
			m_bFirst = true;
		}

		pIoContext->m_wsaBuf.len = static_cast<ULONG>(BUFFER_LENGTH - len);
		DWORD dwBytesSend = 0;
		int nBytesSend = WSASend(pIoContext->m_sockAccept, &pIoContext->m_wsaBuf, 1, &dwBytesSend, 0, &pIoContext->m_Overlapped, NULL);
		if ((SOCKET_ERROR == nBytesSend) && (WSA_IO_PENDING != WSAGetLastError()))
		{
			error_log("投递WSASend失败！错误代码：%d", WSAGetLastError());
			return;
		}
	}
	else
	{
		m_bAtom = false;
		m_pContext->RemoveContext(pIoContext);
	}
}

void NetWorkMsgHandler::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
	if (m_bAtom.load() == false)
		RealSend();
}

// 实际调用WSASend，只能在主线程中调用
void NetWorkMsgHandler::RealSend()
{
	std::lock_guard<std::mutex>	lck(m_hQueueMutex);

	// 如果发送队列不为空，则直接调用WSASend进行发送
	if (!m_sendPackQueue.empty())
	{
		m_bAtom = true;

		auto spIoContext = m_pContext->GetNewIoContext();
		spIoContext->m_sockAccept = m_pContext->m_Socket;

		spIoContext->m_opType = SEND_POSTED;
		auto buff = spIoContext->m_wsaBuf.buf;
		auto len = spIoContext->m_wsaBuf.len;
		auto pack = m_sendPackQueue.front();
		if (m_bFirst)
		{
			// 第一个包，把长度加进去
			auto pack_size = pack.size() + sizeof(unsigned int);	// 别忘了加上自己的长度unsigned int
			memcpy(buff, &pack_size, sizeof(unsigned int));
			buff += sizeof(unsigned int);
			len -= sizeof(unsigned int);
		}

		if (pack.size() > len)
		{
			pack.ReadBuffer(buff, len);
			len -= len;
			m_bFirst = false;
		}
		else
		{
			len = static_cast<ULONG>(len - pack.size());
			pack.ReadBuffer(buff, pack.size());

			// 从队列中弹出
			m_sendPackQueue.pop();
			m_bFirst = true;
		}

		spIoContext->m_wsaBuf.len -= len;
		DWORD dwBytesSend = 0;
		int nBytesSend = WSASend(spIoContext->m_sockAccept, &spIoContext->m_wsaBuf, 1, &dwBytesSend, 0, &spIoContext->m_Overlapped, NULL);
		if ((SOCKET_ERROR == nBytesSend) && (WSA_IO_PENDING != WSAGetLastError()))
		{
			error_log("投递WSASend失败！错误代码：%d", WSAGetLastError());
			return;
		}
	}
}

void NetWorkMsgHandler::Destroy()
{
	m_pContext->Destroy();
}