#ifndef __ACTOR_MANAGER_H_
#define __ACTOR_MANAGER_H_
#include <list>
#include <memory>
#include <unordered_map>
#include "socket_io_context.h"
#include "actor.h"
#include "singleton.h"
#include "ctimer.h"

class ActorManager final :public Singleton<ActorManager>
{
public:
	ActorManager();
	~ActorManager();
	ActorManager(const ActorManager&) = delete;
	ActorManager& operator= (const ActorManager&) = delete;

public:
	void AddActor(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext);
	void ClearActor();
	void RemoveLoginedActor(unsigned int actorId);		// 这里的删除是指从在线角色列表删除
	void AddLoginedActor(const std::shared_ptr<Actor>& actor);	// 加入在线角色列表
	bool CheckOnline(unsigned int actorId);		// 判断是否在线

public:
	void LogicRun(const std::chrono::system_clock::duration&);
	void Start();
	void Stop();

private:
	void ActorGC();
	
private:
	std::mutex													m_hMutex;
	std::list<std::shared_ptr<Actor>>							m_listActors;			// 所有连接的客户端
	std::mutex													m_hLoginMutex;
	std::unordered_map<unsigned int, std::shared_ptr<Actor>>	m_loginedActors;		// 在线的角色列表

private:
	CTimer<5000>				m_gcTimer;		// gc定时器，间隔5秒
};
#endif