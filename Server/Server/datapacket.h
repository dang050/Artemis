#ifndef __DATAPACKET_H_
#define __DATAPACKET_H_
#include <iostream>

// 此类线程不安全
class CDataPacket final
{
public:
	CDataPacket();
	~CDataPacket();
	CDataPacket(const CDataPacket&);
	CDataPacket& operator= (const CDataPacket&);
	CDataPacket(CDataPacket&&);
	CDataPacket& operator= (CDataPacket&&);

public:
	explicit operator size_t() const;
	size_t size() const;
	void clear();

public:
	template <
		typename T,
		typename = typename std::enable_if <
		!std::is_pointer<typename std::decay<T>::type >::value &&
		std::is_trivial<typename std::decay<T>::type>::value		// 限制只含有基本数据类型（不能有指针之类）
		>::type
	>
	inline CDataPacket& operator << (T&& val);
	/*
	#if _MSC_VER >= 1900	// for vs2015 and up
	template<typename T>
	inline CDataPacket& operator << (T&& val);
	#else
	template<typename T>
	inline CDataPacket& operator << (T& val);
	#endif
	template<typename T>
	inline CDataPacket& operator << (T const& val);
	*/
	template<typename T>
	inline CDataPacket& operator << (T* const val);
	template<typename T>
	inline CDataPacket& operator << (T const* const val);

public:
	CDataPacket& ReadBuffer(void* const, size_t);
	CDataPacket& WriteBuffer(const void* const, size_t);
public:
	template<
		typename T,
		typename = typename std::enable_if <
		!std::is_pointer<typename std::decay<T>::type >::value &&
		std::is_trivial<typename std::decay<T>::type>::value
		>::type
	>
	inline CDataPacket& operator >> (T& val);
	template<typename T>
	inline CDataPacket& operator >> (T* const val);
	CDataPacket& operator >> (std::string& val);
private:
	void chk_alloc_size(size_t size);
	void realloc_size(size_t size);
	bool check_value_size(size_t size) const;
private:
	static const size_t kDefaultSize = 0x00000080;  //128
	static const size_t kIncreaseSize = 0x00000400; //1024
	void*	_buffer;	// 分配内存的起始位置
	size_t	_all_size;	// 分配的内存总大小
	size_t	_used_size; // 使用到的大小（读的时候不会减小它，写的时候会增加它，重分配内存的时候会进行一个重置）
	void*	_read;		// 读取的位置
};

template<typename T, typename>
inline CDataPacket& CDataPacket::operator << (T&& val)
{
	using ValueType = typename std::decay<T>::type;	// 因为左值推导出来的是引用
	unsigned int size = sizeof(ValueType);
	chk_alloc_size(size);
	*reinterpret_cast<ValueType*>(static_cast<char*>(_buffer)+_used_size) = std::forward<T>(val);
	_used_size += size;

	return *this;
}
/*
#if _MSC_VER >= 1900	// vs2015及以上版本,待测试
template<typename T>
inline CDataPacket& CDataPacket::operator << (T&& val)
{
using ValueType = typename std::remove_reference<T>::type;	// 因为左值推导出来的是引用
unsigned int size = sizeof(ValueType);
chk_alloc_size(size);
*reinterpret_cast<ValueType*>(static_cast<char*>(_buffer)+_used_size) = std::forward<T>(val);
_used_size += size;

return *this;
}
#else
template<typename T>
inline CDataPacket& CDataPacket::operator << (T& val)
{
unsigned int size = sizeof(T);
chk_alloc_size(size);
*reinterpret_cast<T*>(static_cast<char*>(_buffer)+_used_size) = val;
_used_size += size;

return *this;
}
#endif

template<typename T>
inline CDataPacket& CDataPacket::operator << (T const& val)
{
unsigned int size = sizeof(T);
chk_alloc_size(size);
*reinterpret_cast<T*>(static_cast<char*>(_buffer)+_used_size) = val;
_used_size += size;

return *this;
}
*/

// 普通指针拷贝的是指针里面的内容
template<typename T>
inline CDataPacket& CDataPacket::operator << (T* const val)
{
	unsigned int size = sizeof(T);
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<void*>(val), size);
	_used_size += size;
	return *this;
}

template<typename T>
inline CDataPacket& CDataPacket::operator << (T const* const val)
{
	unsigned int size = sizeof(T);
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<const void*>(val), size);
	_used_size += size;
	return *this;
}

// 字符串指针特殊处理
// 在字符串前面写入两个字节，表示字符串用到的字节数(包括结束符)
template<>
inline CDataPacket& CDataPacket::operator << (const char* const val)
{
	size_t size = strlen(val);
	operator<<(static_cast<unsigned short>(size));		// 先写入字符串的字节数
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<const void*>(val), size);
	_used_size += size;

	return *this;
}

template<>
inline CDataPacket& CDataPacket::operator << (char* const val)
{
	size_t size = strlen(val);
	operator<<(static_cast<unsigned short>(size));		// 先写入字符串的字节数
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<void*>(val), size);
	_used_size += size;

	return *this;
}

template<>
inline CDataPacket& CDataPacket::operator<<(const wchar_t* const val)
{
	size_t size = wcslen(val) * sizeof(wchar_t);
	operator<<(static_cast<unsigned short>(size));		// 先写入字符串的字节数
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<const void*>(val), size);
	_used_size += size;

	return *this;
}

template<>
inline CDataPacket& CDataPacket::operator<<(wchar_t* const val)
{
	size_t size = wcslen(val) * sizeof(wchar_t);
	operator<<(static_cast<unsigned short>(size));		// 先写入字符串的字节数
	chk_alloc_size(size);
	memcpy(static_cast<void*>(static_cast<char*>(_buffer)+_used_size), static_cast<void*>(val), size);
	_used_size += size;

	return *this;
}

template<typename T, typename>
inline CDataPacket& CDataPacket::operator >> (T& val)
{
	unsigned int size = sizeof(T);
	if (check_value_size(size))
	{
		val = *reinterpret_cast<T*>(_read);
		_read = static_cast<void*>(static_cast<char*>(_read)+size);
	}
	return *this;
}

template<typename T>
inline CDataPacket& CDataPacket::operator >> (T* const val)
{
	unsigned int size = sizeof(T);
	if (check_value_size(size))
	{
		memcpy(static_cast<void*>(val), _read, size);
		_read = static_cast<void*>(static_cast<char*>(_read)+size);
	}
	return *this;
}

template<>
inline CDataPacket& CDataPacket::operator >> (char* const val)
{
	unsigned short size = 0;
	operator>>(size);			// 获取字符串长度
	if (check_value_size(size))
	{
		memcpy(static_cast<void*>(val), _read, size);
		_read = static_cast<void*>(static_cast<char*>(_read)+size);
	}
	return *this;
}

template<>
inline CDataPacket& CDataPacket::operator >> (wchar_t* const val)
{
	unsigned short size = 0;
	operator>>(size);			// 获取字符串长度
	if (check_value_size(size))
	{
		memcpy(static_cast<void*>(val), _read, size);
		_read = static_cast<void*>(static_cast<char*>(_read)+size);
	}
	return *this;
}

#endif