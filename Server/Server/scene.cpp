#include "scene.h"
#include "parse_json.h"
#include "logger.h"
#include "datapacket.h"
#include "SystemId.h"
#include "entity.h"
#include "actor.h"

Scene::Scene(unsigned int sceneId, unsigned int subId)
:m_nSceneId(sceneId),
m_nSubId(subId)
{
	LoadSceneDataFromConfig();
}

Scene::~Scene()
{
}

void Scene::LoadSceneDataFromConfig()
{
	std::string file = "scene_" + std::to_string(m_nSceneId) + ".json";
	std::ifstream ifs;
	ifs.open(file, std::ios::binary);
	if (!ifs)
	{
		error_log("Lose scene_%d Config File", m_nSceneId);
		return ;
	}

	Json::Reader	reader;
	Json::Value		root;
	if (reader.parse(ifs, root))
	{
		unsigned int aoiX = root["aoiX"].asUInt();
		unsigned int aoiY = root["aoiY"].asUInt();
		m_nDefaultX = root["defaultX"].asInt();
		m_nDefaultY = root["defaultY"].asInt();
		m_hAOI.SetAOIRegion(aoiX, aoiY);
	}
	else
	{
		error_log("Parse scene_%d Config File occurs errors", m_nSceneId);
	}
	ifs.close();
}


void Scene::PlayerMove(std::shared_ptr<Entity>& obj, const std::pair<int, int>& pos)
{
	// todo 速度暂定为1
	unsigned int speed = 1;

	std::vector<std::shared_ptr<Entity>> leaveNodes;
	std::vector<std::shared_ptr<Entity>> updateNodes;
	std::vector<std::shared_ptr<Entity>> enterNodes;

	std::unique_lock<std::mutex>	lck(m_hAOIMutex);
	m_hAOI.Move(obj, pos, leaveNodes, updateNodes, enterNodes);
	lck.unlock();

	// 发送移动成功的消息给玩家
	CDataPacket netPack;
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sMove);
	netPack << speed << obj->GetX() << obj->GetY();
	std::static_pointer_cast<Actor>(obj)->Send(netPack);

	netPack.clear();
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sMoveBro);
	netPack << obj->GetId() << speed << obj->GetX() << obj->GetY() << static_cast<BYTE>(obj->GetType());
	for (auto& it : updateNodes)
	{
		switch (it->GetType())
		{
		case Entity::EntityType::MONSTER:
			break;
		case Entity::EntityType::PLAYER:
			std::static_pointer_cast<Actor>(it)->Send(netPack);
			break;
		case Entity::EntityType::GOODS:
			break;
		case Entity::EntityType::SKILL:
			break;
		default:
			break;
		}
	}

	PlayerLeave(obj, leaveNodes, true);
	PlayerEnter(obj, enterNodes);
}
// 0(0,1) 1(1,1) 2(-1,1) 3(1,0) 4(-1,0) 5(1,-1) 6(-1,-1) 7(0,-1) 总共8个方向
bool Scene::Move(std::shared_ptr<Entity>& obj, int x, int y, int dir)
{
	std::pair<int, int> pos = obj->GetPos();
	if (pos.first != x || pos.second != y)
	{
		return false;
	}

	// todo 速度暂定为1
	unsigned int speed = 1;
	switch (dir)
	{
	case 0:
		pos.second += speed;
		break;
	case 1:
		pos.first += speed;
		pos.second += speed;
		break;
	case 2:
		pos.first -= speed;
		pos.second += speed;
		break;
	case 3:
		pos.first += speed;
		break;
	case 4:
		pos.first -= speed;
		break;
	case 5:
		pos.first += speed;
		pos.second -= speed;
		break;
	case 6:
		pos.first -= speed;
		pos.second -= speed;
		break;
	case 7:
		pos.second -= speed;
		break;
	default:
		return false;
	}

	switch (obj->GetType())
	{
	case Entity::EntityType::PLAYER:			// 玩家移动
		PlayerMove(obj, pos);
		break;
	case Entity::EntityType::MONSTER:			// 怪物移动
		break;
	case Entity::EntityType::SKILL:				// 技能移动
		break;
	default:
		break;
	}
	return true;
}

void Scene::PlayerEnter(std::shared_ptr<Entity>& entity, const std::vector<std::shared_ptr<Entity>>& nodes)
{
	std::shared_ptr<Actor> obj = std::static_pointer_cast<Actor>(entity);
	CDataPacket netPack;

	// 根据不同的实体类型去做不同的处理
	// 把自己的信息打包好
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sEnterSceneBro);
	netPack << static_cast<unsigned short>(0);
	netPack << static_cast<unsigned short>(1);
	netPack << obj->GetId();
	netPack << obj->GetName().c_str();
	netPack << obj->GetLevel();
	netPack << obj->GetModel();
	netPack << obj->GetX();
	netPack << obj->GetY();
	netPack << static_cast<unsigned short>(0);
	netPack << static_cast<unsigned short>(0);

	// 给nodes分类
	std::vector<std::shared_ptr<Actor>>	players;
	for (auto& it : nodes)
	{
		switch (it->GetType())
		{
		case Entity::EntityType::PLAYER:
		{
										   std::shared_ptr<Actor> role = std::static_pointer_cast<Actor>(it);
										   role->Send(netPack);
										   players.push_back(role);
										   break;
		}
		case Entity::EntityType::MONSTER:
			break;
		case Entity::EntityType::GOODS:
			break;
		case Entity::EntityType::SKILL:
			break;
		default:
			break;
		}
	}

	// 再把场景信息发给玩家
	netPack.clear();
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sEnterSceneBro);
	netPack << static_cast<unsigned short>(0);
	netPack << static_cast<unsigned short>(players.size());
	for (auto& it : players)
	{
		netPack << it->GetId();
		netPack << it->GetName().c_str();
		netPack << it->GetLevel();
		netPack << it->GetModel();
		netPack << it->GetX();
		netPack << it->GetY();
	}
	netPack << static_cast<unsigned short>(0);
	netPack << static_cast<unsigned short>(0);
	obj->Send(netPack);
}

void Scene::Enter(std::shared_ptr<Entity>& obj, int x, int y)
{
	if (x == 0)
	{
		x = m_nDefaultX;
	}
	if (y == 0)
	{
		y = m_nDefaultY;
	}
	// 坐标和场景信息
	obj->SetPos(x, y, m_nSceneId, m_nSubId);
	std::vector<std::shared_ptr<Entity>>	nodes;
	std::unique_lock<std::mutex>	lck(m_hAOIMutex);
	m_hAOI.Enter(obj, nodes);
	lck.unlock();

	switch (obj->GetType())
	{
	case Entity::EntityType::PLAYER:
		PlayerEnter(obj, nodes);
		break;
	default:
		break;
	}
}

// broadcast 用来标识要不要广播给自身
void Scene::PlayerLeave(std::shared_ptr<Entity>& obj, const std::vector<std::shared_ptr<Entity>>& nodes, bool broadcast)
{
	CDataPacket	netPack;
	netPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sLeaveSceneBro);
	netPack << static_cast<unsigned short>(1);
	netPack << obj->GetId();
	netPack << static_cast<BYTE>(obj->GetType());

	CDataPacket selfPack;
	selfPack << static_cast<unsigned int>(aeSceneSystem) << static_cast<unsigned int>(tagSceneSystemCmd::sLeaveSceneBro);
	selfPack << static_cast<unsigned short>(nodes.size());

	// 给nodes分类, 根据不同的实体类型去做不同的处理
	for (auto& it : nodes)
	{
		selfPack << it->GetId() << static_cast<BYTE>(it->GetType());
		switch (it->GetType())
		{
		case Entity::EntityType::PLAYER:
			std::static_pointer_cast<Actor>(it)->Send(netPack);
			break;
		default:
			break;
		}
	}

	if (broadcast)
	{
		std::static_pointer_cast<Actor>(obj)->Send(selfPack);
	}
}
bool Scene::Leave(std::shared_ptr<Entity>& obj)
{
	std::vector<std::shared_ptr<Entity>>	nodes;

	std::unique_lock<std::mutex>	lck(m_hAOIMutex);
	m_hAOI.Leave(obj, nodes);
	lck.unlock();
	// 修改坐标和场景信息
	obj->SetPos(0, 0, 0, 0);

	switch (obj->GetType())
	{
	case Entity::EntityType::PLAYER:
		PlayerLeave(obj, nodes);
		break;
	default:
		break;
	}

	return true;
}

void Scene::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
//	warning_log("scene1");
}