#ifndef __SQL_HELPER_H_
#define __SQL_HELPER_H_
#include <string>
#include "mysql_result.h"

namespace SqlHelper
{
	int SelectIdFromUser(const std::string& name, const std::string& passwd);

	bool CheckIdIsExistInUser(const std::string& name);

	void InsertIntoUser(const std::string& name, const std::string& passwd);

	std::shared_ptr<MysqlResult> GetUserDataFromPlayerData(unsigned int);

	bool CreatePlayerData(unsigned int id, const std::string& name, unsigned int model, int x, int y, unsigned int scene, unsigned int subScene = 0);

	void UpdatePlayerPosition(unsigned int id, int x, int y, unsigned int sceneId = 0, unsigned int subSceneId = 0);
}

#endif