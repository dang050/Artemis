#ifndef __SERVER_H_
#define __SERVER_H_

#include "iocp.h"
#include "singleton.h"
#include "timer_component_manager.h"
#include "lua_script.h"

class Server final :public Singleton<Server>
{
public:
	Server(std::chrono::milliseconds const& interval = std::chrono::milliseconds(5));
	~Server();
	Server(const Server&) = delete;
	Server& operator= (const Server&) = delete;

public:
	bool Start();
	void Stop();
	void RegisterTimer(const std::string&, unsigned long, unsigned long, unsigned int, const std::function<void(void)>&);
	void UnRegisterTimer(const std::string&);

public:
	void LogicRun();
public:
	void connected(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext);

private:
	CIOCP	m_iocp;
	bool	m_bState;
	std::chrono::milliseconds		m_logicLoopInterval;
	std::unique_ptr<std::thread>	m_pThread;
	TimerComponentManager			m_hTimerComponentMgr;		// 管理全局的定时器注册

private:
	LuaScript	m_hScript;
};

#endif