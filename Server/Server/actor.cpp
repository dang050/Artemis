#include "actor.h"
#include "logger.h"
#include "SystemId.h"
#include "actor_manager.h"
#include "sql_helper.h"

ObjectPool<CDataPacket>		Actor::m_packPool;
std::mutex			Actor::m_hPoolMutex;

Actor::Actor(std::shared_ptr<PER_SOCKET_CONTEXT>& pSocketContext)
:m_bGC(false),
m_bIsLogin(false),
m_subLoginSystem(this),
m_subSceneSystem(this),
_level(0),
Entity(EntityType::PLAYER),
std::enable_shared_from_this<Actor>()
{
	m_pNetWorkMsgHandler = std::make_shared<NetWorkMsgHandler>(
		pSocketContext,
		std::bind(&Actor::connectCallback, this),
		std::bind(&Actor::disconnectCallback, this),
		std::bind(&Actor::recvCallback, this, std::placeholders::_1),
		std::bind(&Actor::sendCallback, this)
	);
}

Actor::~Actor()
{
	m_pNetWorkMsgHandler.reset();

	// 消息队列会自动清空
}

void Actor::connectCallback()
{
	m_bGC = false;
}

void Actor::recvCallback(CDataPacket& pack)
{
	std::unique_lock<std::mutex>	poolLck(Actor::m_hPoolMutex);
	std::shared_ptr<CDataPacket> obj(Actor::m_packPool.acquire(pack), [](CDataPacket* p){
		std::unique_lock<std::mutex>	poolLck(Actor::m_hPoolMutex);
		Actor::m_packPool.release(p);
	});
	poolLck.unlock();

	// 入队列
	std::lock_guard<std::mutex>	lck(m_hMutex);
	m_packs.push(obj);
}

void Actor::sendCallback()
{

}

void Actor::disconnectCallback()
{
	// 执行登出逻辑
	Logout();
}

// 执行角色的登出逻辑，此函数由disconnectCallback()函数调用
void Actor::Logout()
{
	// 已登录
	if (m_bIsLogin)
	{
		// 离开场景
		m_subSceneSystem.LeaveScene();
		ActorManager::GetInstance()->RemoveLoginedActor(GetId());		// 从在线列表中删除玩家
	}

	m_bIsLogin = false;
	_id = 0;
	// 设置角色可以被回收
	m_bGC = true;
}

void Actor::LogicRun(const std::chrono::system_clock::duration& nCurrTick)
{
	Entity::LogicRun(nCurrTick);
	m_pNetWorkMsgHandler->LogicRun(nCurrTick);	// 处理数据发送队列
	ProcessNetworkMsg();	// 处理数据接收队列

	if (m_t50ms.CheckAndSet(nCurrTick))
	{
		m_subSceneSystem.LogicRun();
	}
}

void Actor::ProcessNetworkMsg()
{
	if (!m_bGC)
	{
		unsigned int len;

		std::unique_lock<std::mutex>	lck(m_hMutex, std::defer_lock);
		char buffer[8192];
		while (!m_packs.empty())
		{
			ZeroMemory(buffer, 8192);
			lck.lock();
			auto pack = m_packs.front();
			m_packs.pop();
			lck.unlock();
			(*pack) >> len;
			debug_log("接收到的数据包长度：%d", len);

			ProcessNetworkMsg(*pack);
		}
	}
}

void Actor::ProcessNetworkMsg(CDataPacket& pack)
{
	if (!m_bGC)
	{
		unsigned int system = 0;
		unsigned int cmd = 0;
		pack >> system >> cmd;
		switch (system)
		{
		case aeLoginSystem:
			m_subLoginSystem.ProcessNetwork(system, cmd, pack);
			break;
		case aeSceneSystem:
			m_subSceneSystem.ProcessNetwork(system, cmd, pack);
			break;
		default:
			break;
		}
	}
}

void Actor::Send(CDataPacket& pack)
{
	m_pNetWorkMsgHandler->Send(pack);
}

// 销毁玩家的唯一做法，把玩家的套接字关闭，然后通过IOCP回调执行登出逻辑
// 不能直接销毁，因为会触发完成端口的回调，导致使用已释放的内存
void Actor::Destroy()
{
	m_pNetWorkMsgHandler->Destroy();
}

// 执行角色的登陆逻辑，此函数由LoginSystem子系统里的LoginSuccess调用
void Actor::Login(unsigned int actorId)
{
	m_bIsLogin = true;
	_id = actorId;		// 赋值玩家ID
	ActorManager::GetInstance()->AddLoginedActor(GetSharedFromSelf());		// 添加进在线玩家列表
}

bool Actor::CheckAndLoadUserData()
{
	// 如果名字为空，则从数据库加载
	if (_name.empty())
	{
		return LoadUserDataFromDB();
	}
	else
	{
		return true;
	}
}

bool Actor::LoadUserDataFromDB()
{
	auto res = SqlHelper::GetUserDataFromPlayerData(_id);
	if (res == nullptr || res->GetCurrentNumRows() <= 0)
	{
		return false;
	}
	auto result = res->GetCurrentRow();
	_model = std::stoi(result[0]);
	_name = result[1];
	_level = std::stoi(result[2]);
	_sceneId = std::stoi(result[3]);
	_x = std::stoi(result[4]);
	_y = std::stoi(result[5]);
	_subSceneId = std::stoi(result[6]);
	return true;
}