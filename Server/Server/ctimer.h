#ifndef __CTIMER_H_
#define __CTIMER_H_
#include "tool.h"

template<unsigned long INTERVAL>
class CTimer final
{
	using millsecond_type = std::chrono::duration<unsigned long, std::ratio<1, 1000>>;
public:
	CTimer()
	{
		m_nextTick = iocp::GetCurrTickCount();
		m_nextTick += static_cast<millsecond_type>(INTERVAL);
	}
	~CTimer(){}
	CTimer(const CTimer<INTERVAL>&) = delete;
	CTimer<INTERVAL> operator=(const CTimer<INTERVAL>&) = delete;

public:
	inline bool CheckAndSet(const std::chrono::system_clock::duration& nCurrTick)
	{
		if (m_nextTick <= nCurrTick)
		{
			m_nextTick = nCurrTick + static_cast<millsecond_type>(INTERVAL);
			return true;
		}
		return false;
	}
private:
	std::chrono::system_clock::duration			m_nextTick;
};

#endif